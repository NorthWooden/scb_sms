<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
	request.setAttribute("basePath", basePath);
%>
<!DOCTYPE html>
<html>
<head>
<title>职员信息显示</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- 引入bootstrap -->
<link rel="stylesheet" type="text/css"
	href="${basePath}/css/bootstrap.min.css">
<!-- 引入JQuery  bootstrap.js-->
<script src="${basePath}/js/jquery-3.2.1.min.js"></script>
<script src="${basePath}/js/bootstrap.min.js"></script>
<%!int i = 1;%>
<%--<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">--%>
</head>
<body>
	<!-- 顶栏 -->
	<jsp:include page="top.jsp"></jsp:include>
	<!-- 中间主体 -->
	<div class="container" id="content">
		<div class="row">
			
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<div class="row">
							<h1 class="col-md-5">职员信息显示</h1>
							<button class="btn btn-default col-md-2" style="margin-top: 20px" onClick="location.href='${basePath}sm/positions'">
								返回
							</button>
						</div>
					</div>
					<div class="table-responsive">
						<table class="table">
							<thead>
								<tr>
									<th nowrap="nowrap">职员编号</th>
									<th nowrap="nowrap">职员姓名</th>
									<th nowrap="nowrap">部门编号</th>
									<th nowrap="nowrap">职位编号</th>
									<th nowrap="nowrap">职员英文名</th>
									<th nowrap="nowrap">职员邮箱</th>
									<th nowrap="nowrap">职员电话</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${requestScope.emps}" var="employee"
									varStatus="stauts">

									<tr>
										<td>${employee.employeeId }</td>
										<td>${employee.employeeName}</td>
										<td>${employee.employeeDepartmentId}</td>
										<td>${employee.employeePositionId}</td>
										<td>${employee.employeeEnglishname}</td>
										<td>${employee.employeeEmail}</td>
										<td>${employee.employeePhoneNumber}</td>
										
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>


				</div>

			</div>
		</div>
	</div>
	<div class="container" id="footer">
		<div class="row">
			<div class="col-md-12"></div>
		</div>
	</div>
</body>

</html>