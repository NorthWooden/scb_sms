<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
     <%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
request.setAttribute("basePath", basePath);
%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>

<link href="${basePath}css/bootstrap.min.css" rel="stylesheet">

<script src="${basePath}js/jquery-3.2.1.min.js"></script>
<script src="${basePath}js/bootstrap.min.js"></script>
</head>
<body>
	<form class="form-horizontal col-lg-offset-4"
	 action="${basePath}sm/department/edit"
		 method="post" onsubmit="return checkForm()">
		<h3>Department update</h3>
		<br><br><br><br>
		
		<div class="form-group">
			<label class="col-lg-2 control-label">部门序号：</label>
			<div class="col-lg-4">
				<input type="text" class="form-control" name="departmentId" id="departmentId"
					value=" ${department.departmentId}"  readonly="readonly" >
				
			</div>
		</div>
		<div class="form-group">
			<label class="col-lg-2 control-label">部门名称：</label>
			<div class="col-lg-4">
				<input type="text" class="form-control" name="departmentName" id="departmentName"
					value="${department.departmentName}" >	
					
			</div>
		</div>
		
		<div class="form-group">
			<label class="col-lg-2 control-label">部门主管：</label>
			<div class="col-lg-4">
				<input type="text" class="form-control" name="departmentLeader" id="departmentLeader" value="${department.departmentLeader}">	
				<span id="departmentLeader_span" style="display: none; color: red"></span>
			</div>
		</div>
		
		<div class="form-group">
			<label class="col-lg-2 control-label">上级部门：</label>
			<div class="col-lg-4">
				<select class="form-control" name="departmentPid" id="departmentPid" required="required">
					<option value="-1">没有上级</option>
					<c:forEach items="${departmentList }" var="department">
						<option value="${department.departmentId }">${department.departmentName }</option>
					</c:forEach>
				</select>
			</div>
		</div>
		
		<br>
		<div class="form-group">
			<div class="col-lg-4 col-lg-offset-2">
				<input type="submit" value="提交" name="submit" class="btn btn-danger">
				&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; 
				<input type="reset" value="重置"name="reset" class="btn btn-danger">
			</div>
		</div>
	</form>

    
</body>
<script type="text/javascript">
window.onload = function(){
	var departmentPid=${department.departmentPid}
	$('#departmentPid').val(departmentPid);
	var departmentLeader = document.getElementById("departmentLeader");
	var departmentLeader_span = document.getElementById("departmentLeader_span");
	departmentLeader.onblur=function(){
		var employeeId=departmentLeader.value;
		$.ajax({
			type:"get",
			url:"../../findEmployee",//注意url级别
			data:{
				employeeId:employeeId
			},
			success:function(data){
				departmentLeader_span.style.display = "inline-block";
				departmentLeader_span.innerHTML = "职员姓名："+data.employeeName;
				flag = true;
			},
			error:function(data){
				departmentLeader_span.style.display = "inline-block";
				departmentLeader_span.innerHTML = "职员不存在！";
				flag = false;
			}
		});
	}
}
	
</script>
</html>