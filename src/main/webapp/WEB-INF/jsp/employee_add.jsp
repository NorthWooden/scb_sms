<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";

request.setAttribute("basePath", basePath);
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>添加职员</title>
<link href="${basePath}css/bootstrap.min.css" rel="stylesheet">
<script type="text/javascript">
	window.onload = function() {
		var employeeName=document.getElementById("employeeName");
		var employeeName_span=document.getElementById("employeeName_span")
		
		var form=document.getElementById("form");
		var button=document.getElementById("button");
		var flag=false;
		
		employeeName.onblur = function() {
			if ("" == employeeName.value.trim() || null == employeeName.value) {
				employeeName_span.style.display = "inline-block";
				employeeName_span.innerHTML = "职员姓名不得为空！";
				flag = false;
			} else {
				employeeName_span.style.display = "none";
				flag = true;
			}
		
		button.onclick= function(){
			if(flag==true){
				employee.submit();
			}
			else{
				alert("添加失败！");
				}
			}
				
		}
			
	}
</script>	
</head>
<body>
<div class="container" id="content" style="text-align: center;">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<div class="row">
							<h2 style="text-align: center;">新增职员信息</h2>
						</div>
					</div> 
					<form class="form-horizontal col-lg-offset-4" 
					 action="${basePath}sm/employee/add" 
					 name="employee" method="post">
						
						<br><br><br><br>
						
					
						<div class="form-group">
							<label class="col-lg-2 control-label">职员姓名：</label>
							<div class="col-lg-4">
								<input type="text" class="form-control" name="employeeName" id="employeeName" 
									placeholder="请输入职员姓名"  required="required">
									<span id="employeeName_span"
									style="display: none; color: red"></span>
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">部门编号：</label>
							<div class="col-lg-4">
								<input type="text" class="form-control" name="employeeDepartmentId" id="employeeDepartmentId" 
									placeholder="请输入部门编号"  required="required">
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">职位编号：</label>
							<div class="col-lg-4">
								<input type="text" class="form-control" name="employeePositionId" id="employeePositionId" 
									placeholder="请输入职位编号"required="required" >
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">职员英文名：</label>
							<div class="col-lg-4">
								<input type="text" class="form-control" name="employeeEnglishname" id="employeeEnglishname" 
									placeholder="请输入职员英文名"required="required" >
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">职员邮箱：</label>
							<div class="col-lg-4">
								<input type="text" class="form-control" name="employeeEmail" id="employeeEmail" 
									placeholder="请输入职员邮箱"required="required" >
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">职员电话：</label>
							<div class="col-lg-4">
								<input type="text" class="form-control" name="employeePhoneNumber" id="employeePhoneNumber" 
									placeholder="请输入职员电话"required="required" >
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">职员状态：</label>
							<div class="col-lg-4">
								<input type="text" class="form-control" name="employeeState" id="employeeState" 
									placeholder="请输入职员状态"required="required" >
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">职员座机：</label>
							<div class="col-lg-4">
								<input type="text" class="form-control" name="employeeTel" id="employeeTel" 
									placeholder="请输入职员座机"required="required" >
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">职员位置：</label>
							<div class="col-lg-4">
								<input type="text" class="form-control" name="employeeAddress" id="employeeAddress" 
									placeholder="请输入职员位置"required="required" >
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">入职时间：</label>
							<div class="col-lg-4">
								<input type="text" class="form-control" name="employeeInductionTime" id="employeeInductionTime" 
									placeholder="输入入职时间如2018-11-11"required="required" >
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">创建人：</label>
							<div class="col-lg-4">
								<input type="text" class="form-control" name="employeeCreatorId" id="employeeCreatorId" 
									placeholder="请输入创建人"required="required" >
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">职员性别：</label>
							<div class="col-lg-4">
								<input type="text" class="form-control" name="employeeGender" id="employeeGender" 
									placeholder="请输入职员性别"required="required" >
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">职员出生日期：</label>
							<div class="col-lg-4">
								<input type="text" class="form-control" name="employeeBorn" id="employeeBorn" 
									placeholder="输入职员出生日期如1996-09-11"required="required" >
							</div>
						</div>
						
						<br>
						
						<br>
						<div class="form-group">
							<div class="col-lg-4 col-lg-offset-2">
								<input type="submit" value="提交"  class="btn btn-danger" id="submit">
								&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; 
								<input type="reset" value="重置"name="reset" class="btn btn-danger">
							</div>
						</div>
					</form>
					</div>
				</div>
			</div>
		</div>
</body>
</html>