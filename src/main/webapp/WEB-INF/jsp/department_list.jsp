<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
request.setAttribute("basePath", basePath);
%>
<!DOCTYPE html>
<html>
<head>
 
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<title>部门信息显示</title>

	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- 引入bootstrap -->
	<link rel="stylesheet" type="text/css" href="${basePath}css/bootstrap.min.css">
	<!-- 引入JQuery  bootstrap.js-->
	<script src="${basePath}js/jquery-3.2.1.min.js"></script>
	<script src="${basePath}js/bootstrap.min.js"></script>

	<%--<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">--%>

</head>
<body>
	<!-- 中间主体 -->
	<div class="container" id="content">
		<div class="row">
			<%-- <jsp:include page="menu.jsp"></jsp:include> --%>
			<div class="col-md-10">
				<div class="panel panel-default">
				    <div class="panel-heading">
						<div class="row">
					    	<h1 class="col-md-5">部门信息显示</h1>
							<form class="bs-example bs-example-form col-md-5" role="form" style="margin: 20px 0 10px 0;" action="searchDepartment" id="form1" method="post">
								<div class="input-group">
									<input type="text" class="form-control" placeholder="请输入部门名称" name="searchString">
									<span class="input-group-addon btn" id="sub">查询</span>
								</div>
							</form>
							<button class="btn btn-default col-md-2" style="margin-top: 20px" onClick="location.href='<%=basePath%>sm/department'">
								添加
								<!-- <sapn class="glyphicon glyphicon-plus"/> -->
							</button>

						</div>
				    </div>
				    <table class="table table-bordered" id="departmentTable">
				        <thead>
				            <tr>
				               	<th nowrap="nowrap">编号</th>
								<th nowrap="nowrap">部门名称</th>
								<th nowrap="nowrap">部门介绍</th>
								<th nowrap="nowrap">下级部门</th>
								<th nowrap="nowrap">部门领导</th>
								<th nowrap="nowrap">部门状态</th>
				            </tr>
				        </thead>
				        <tbody>
				        <!-- 
				        <c:set var="index" value="0" /> 
						<c:forEach items="${requestScope.department}" var="department">
							<tr>
								<td>${department.departmentId}</td>
								<td>${department.departmentName}</td>
								<td>${department.departmentDescription}</td>
								<td><a href="javascript:;" onclick="showChild(${department.departmentId})">查看下级部门</a></td>
								<td>
									<c:if test="${department.departmentLeader==null }">
										<a href="javascrip:;">暂无，请到更改设置</a>
									</c:if>
									<c:if test="${department.departmentLeader=='' }">
										<a href="javascrip:;">暂无，请到更改设置</a>
									</c:if>
									<c:if test="${department.departmentLeader!=null }">
										${department.leaderEmployee.employeeName}
									</c:if>
								</td>
								<td>
									<c:if test="${department.departmentIsvisible==0 }">
										可用
									</c:if>
									<c:if test="${department.departmentIsvisible==1 }">
										禁用
									</c:if>
								</td>
								<td><a href="department/edit/${department.departmentId}">
									<button class="btn btn-info" onclick="return confirmUpdate()">
										<span class="glyphicon glyphicon-pencil" aria-hidden="true">
										</span> 
										编辑
									</button></a></td>
								<td><c:if test="${department.departmentIsvisible==0 }"><a href="department/delete/${department.departmentId}&false">
								<button class="btn btn-danger" onclick="return confirmDelete()" >
									<span class="glyphicon glyphicon-trash" aria-hidden="true">
									</span> 
									禁用
								</button></a></c:if>
								<c:if test="${department.departmentIsvisible==1 }"><a href="department/reset/${department.departmentId}">
								<button class="btn btn-danger" onclick="return confirmDelete()" >
									<span class="glyphicon glyphicon-trash" aria-hidden="true">
									</span> 
									重用
								</button></a></c:if></td>
								<td><a href="deleteDepartment?departmentId=${department.departmentId}&boolean=true">
								<button class="btn btn-danger" onclick="return confirmDelete()" >
									<span class="glyphicon glyphicon-trash" aria-hidden="true">
									</span> 
									级联禁用
								</button></a></td>
							</tr>
						</c:forEach> -->
				        </tbody>
				    </table>
				</div>
			</div>
		</div>
	</div>
	<div class="container" id="footer">
		<div class="row">
			<div class="col-md-12"></div>
		</div>
	</div>
</body>
	<script type="text/javascript">
		$("#nav li:nth-child(2)").addClass("active");
		function confirmDelete() {
			return confirm("确定删除该部门吗");
		}
		function confirmUpdate() {
			return confirm("确定修改该部门吗");
		}
		
        $("#sub").click(function () {
            $("#form1").submit();
        });
        function setInfo(data){
        	var leaderName='';
			var isvisible=0;
			if(data.leaderEmployee==null){
				leaderName='暂无，请到更改设置';
			}else{
				leaderName=data.leaderEmployee.employeeName
			}
			if(data.departmentIsvisible==1){
				isvisible='禁用';
			}else{
				isvisible='可用';
			}
        	var x='<tr><td>'+data.departmentId+'</td><td>'+data.departmentName+'</td><td>'
			+data.departmentDescription+'</td><td><a href="javascript:;" onclick="showChild('
			+data.departmentId+')">查看下级部门</a></td><td>'+leaderName+
			'</td><td>'+isvisible+'</td><td><a href="department/edit/'+data.departmentId+'">'+
			'<button class="btn btn-info" onclick="return confirmUpdate()">'+
			'<span class="glyphicon glyphicon-pencil" aria-hidden="true">'+
			'</span> 编辑</button></a></td>'+
			'<td><a href="department/delete/'+data.departmentId+'&false">'+
			'<button class="btn btn-danger" onclick="return confirmDelete()" >'+
			'<span class="glyphicon glyphicon-trash" aria-hidden="true">'+
			'</span> 禁用</button></a></td>'+
			'<td><a href="department/delete/'+data.departmentId+'&true">'+
			'<button class="btn btn-danger" onclick="return confirmDelete()" >'+
			'<span class="glyphicon glyphicon-trash" aria-hidden="true">'+
			'</span> 级联禁用</button></a></td></tr>';
			return x;
        }
        var array=[];
        var obj={};
        $.ajax({
			type:"post",
			url:"departments/json/tree",
			dataType: "json",
			data:{},
			async: false,
			success:function(data){
				obj=data;
				$.each(data,function(i,itme){
					array.push(itme);
				})
			},
			error:function(){
				alert("获取数据时发生错误");
			}
		});
        $(function(){
        	$.each(obj,function(i,itme){
        		var info=setInfo(itme);
        		$('#departmentTable tbody:last').append(info);
        	})
        })
        function showChild(id){
        	var trlist=$('#departmentTable').find('tr');
        	var line=0;
        	$.each(trlist,function(){
        		if($(this).find('td:eq(0)').text()==id){
        			line=$(this).prevAll().length
        		}
        	})
        	$('#departmentTable tbody tr:eq('+line+') td:eq(3)').html('<a href="javascript:;" onclick="hideChild('+id+')">收起下级部门</a>')
        	let copyArray=[];
        	copyArray=array.slice();
        	let size=copyArray.length;
        	let index=0
        	while(index<size){
        		if(copyArray[index].departmentId==id){
        			$.each(copyArray[index].childDepartment,function(i,itme){
        				var info=setInfo(itme)
        				$('#departmentTable tbody tr:eq('+line+')').after(info)
        			})
        			break;
        		}else{
        			$.each(copyArray[index].childDepartment,function(i,itme){
        				copyArray.push(itme)
        			})
        		}
        		size=copyArray.length;
        		index++;
        	}
        	
        	/*
        	$.each(copyArray,function(){
        		if(this.departmentId==id){
        			$.each(this.childDepartment,function(i,itme){
        				var info=setInfo(itme)
        				$('#departmentTable tbody tr:eq('+line+')').after(info)
        			})
        			return false;
        		}else{
        			$.each(this.childDepartment,function(i,itme){
        				alert("hello")
        				copyArray.push(itme)
        			})
        		}
        	})*/
        }
        function hideChild(id){
        	var trlist=$('#departmentTable').find('tr');
        	$.each(trlist,function(){
        		if($(this).find('td:eq(0)').text()==id){
        			$(this).find('td:eq(3)').html('<a href="javascript:;" onclick="showChild('+id+')">查看下级部门</a>')
        		}
        	})
        	var copyArray=array.slice();
        	var parentDepartment={};
        	let size=copyArray.length;
        	let index=0
        	while(index<size){
        		if(copyArray[index].departmentId==id){
        			parentDepartment=copyArray[index];
        			break;
        		}else{
        			$.each(copyArray[index].childDepartment,function(i,itme){
        				copyArray.push(itme)
        			})
        		}
        		size=copyArray.length;
        		index++;
        	}
        	/*
        	$.each(copyArray,function(){
        		if(this.departmentId==id){
        			parentDepartment=this;
        			return false;
        		}else{
        			$.each(this.childDepartment,function(i,itme){
        				copyArray.push(itme)
        			})
        		}
        	})*/
        	
        	$.each(parentDepartment.childDepartment,function(i,itme){
        		$.each(trlist,function(){
            		if($(this).find('td:eq(0)').text()==itme.departmentId){
            			$(this).remove();
            		}
            	})
        	})
        	$.each(parentDepartment.childDepartment,function(){
        		hideChild(this.departmentId);
        	})
        }
	</script>
</html>