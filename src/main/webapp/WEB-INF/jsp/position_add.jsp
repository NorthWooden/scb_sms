<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";

request.setAttribute("basePath", basePath);
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>职位添加</title>
<link href="${basePath}css/bootstrap.min.css" rel="stylesheet">
<script src="${basePath}js/jquery-3.2.1.min.js"></script>
<script type="text/javascript">


	$(document).ready(function(){
		$("#positionId").blur(function(){
			if("" == $("#positionId").val || null ==$("#positionId").val ){
				$("#positionId_span").attr("style.display","inline-block");
				 $("#positionId_span").text("职位ID不能为空");
			} else if(null!=positionId){
				$.ajax({
					type:"post",
					url:"../position/judge/"+positionId.value,		
					dataType:"json",
					success:function(msg){
						if(msg.result=='exist'){
							
							$("#positionId_span").css("display","inline-block");
							 $("#positionId_span").text("该职位ID已存在");
							flag1=false;	
						}else if(msg.result=='notexist'){
							$("#positionId_span").css("display","none");
							flag1=true;
						}
					},
					error:function(){
						alert("读取失败");
					}
				})
			}else {
				$("#positionId_span").css("display","none");
				flag1 = true;
			}
		});
		
		 $("#positionName").blur(function(){
			if ("" == $("#positionName").val() || null == $("#positionName").val()) {
				$("#positionName_span").css("display","inline-block");
				$("#positionName_span").text("职位名称不得为空！");
				flag = false;
			} else {
				$("#positionName_span").css("display","none");
				flag = true;
			}
		})
		
		$("#button").click(function(){
			if(flag1==true&&flag==true){
				pos.submit();
			}
			else{
				alert("添加失败！");
				}
		}) 
	})
	
</script>	
</head>
<body>
<div class="container" id="content" style="text-align: center;">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<div class="row">
							<h2 style="text-align: center;">新增职位信息</h2>
							<button class="btn btn-default col-md-2" style="margin-top: 20px" onClick="location.href='${basePath}sm/positions'">
								返回
							</button>
						</div>
					</div> 
					<form class="form-horizontal col-lg-offset-4" 
					 action="${basePath}sm/position/add" name="pos" method="post">
						
						<br><br><br><br>
						
						<div class="form-group">
							<label class="col-lg-2 control-label">职位ID：</label>
							<div class="col-lg-4">
								<input type="text" class="form-control" name="positionId" id="positionId" 
									placeholder="请输入职位ID"  required="required">
									 <span id="positionId_span"
									style="display: none; color: red"></span>
							</div>
						</div>
					
						<div class="form-group">
							<label class="col-lg-2 control-label">职位名称：</label>
							<div class="col-lg-4">
								<input type="text" class="form-control" name="positionName" id="positionName" 
									placeholder="请输入职位名称"  required="required">
									 <span id="positionName_span"
									style="display: none; color: red"></span>
							</div>
						</div>
						<!-- <div class="form-group">
							<label class="col-lg-2 control-label">职位创建人</label>
							<div class="col-lg-4">
								<input type="text" class="form-control" name="positionCreatorId" id="positionCreatorId" 
									placeholder="请输入职位创建人姓名"required="required" >
									 <span id="positionCreatorId_span"
									style="display: none; color: red"></span>
							</div>
						</div> -->
						<br>
					<!-- 	<div class="form-group">
							<label class="col-lg-2 control-label">职位创建时间</label>
							<div class="col-lg-4">
								<input type="datetime-local" class="form-control" name="positionCreatedTime" id="positionCreatedTime" required="required">
									
							</div>
						</div> -->
						
						<br>
						<div class="form-group">
							<div class="col-lg-4 col-lg-offset-2">
								<input type="button" value="提交"  class="btn btn-danger" id="button">
								&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; 
								<input type="reset" value="重置"name="reset" class="btn btn-danger">
							</div>
						</div>
					</form>
					</div>
				</div>
			</div>
		</div>
</body>
</html>