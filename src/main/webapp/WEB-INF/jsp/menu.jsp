<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path;
%>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- 引入bootstrap -->
<link rel="stylesheet" type="text/css"
	href="<%=basePath%>/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css"
	href="<%=basePath%>/css/zTreeStyle/zTreeStyle.css">

<!-- 引入JQuery  bootstrap.js-->
<script src="<%=basePath%>/js/jquery-3.2.1.min.js"></script>
<script src="<%=basePath%>/js/jquery.ztree.all.min.js"></script>
<script src="<%=basePath%>/js/bootstrap.min.js"></script>

<style type="text/css">
body {
	background: url(<%=basePath%>/images/groudback.jpg) repeat;
}

#login-box {
	/*border:1px solid #F00;*/
	padding: 35px;
	border-radius: 15px;
	background: #56666B;
	color: #fff;
}
</style>
</head>
<div class="col-md-2">

	<ul id="main-nav" class="nav nav-tabs nav-stacked">
		<li class="active"><a href="http://localhost:8092/com.scb.dev.sms/menu/display"
			target="main"> <i class="glyphicon glyphicon-th-large"></i>首页
		</a></li>
		<c:forEach items="${menuJson }" var="menu">
			<!-- 遍历菜单 -->
			<c:if test="${menu.menuId == menu.menuPid }">
				<!-- 是父菜单 -->
				<li><c:set var="loop" value="1"></c:set> <c:forEach
						items="${menuJson }" var="ifChildExist">
						<!-- 遍历菜单判断是否存在子菜单 -->
						<c:if test="${loop == 1 }">
							<c:set var="any_child" value="0"></c:set>
							<!-- 判断是否存在子菜单 any_child 0false 1true  -->
							<c:forEach items="${menuJson }" var="check">
								<c:if
									test="${check.menuId != menu.menuId && check.menuPid == menu.menuId && any_child == 0 }">
									<c:set var="any_child" value="1"></c:set>
								</c:if>
							</c:forEach>
							<c:if test="${any_child == 1 }">
								<!-- 存在子菜单 -->
								<a href="${menu.menuUrl }" target="view_frame" class="nav-header collapsed" 
									data-toggle="collapse"> <i class="glyphicon glyphicon-cog"></i>
									${menu.menuName } <span
									class="pull-right glyphicon glyphicon-chevron-down"></span>
								</a>
								<ul id="${menu.menuUrl }" class="nav nav-list collapse in">
									<c:forEach items="${menuJson }" var="child">
										<c:if
											test="${child.menuPid == menu.menuId && child.menuId != menu.menuId }">
											<li><a href="${child.menuUrl }" target="view_frame">${child.menuName }</a></li>
										</c:if>
									</c:forEach>
								</ul>
							</c:if>
							<c:if test="${any_child == 0 }">
								<c:set var="loop" value="1"></c:set>
								<a href="${menu.menuUrl }" target="view_frame"> <i
									class="glyphicon glyphicon-cog"></i> ${menu.menuName }
								</a>
							</c:if>
							<c:set var="loop" value="0"></c:set>
						</c:if>
					</c:forEach></li>
			</c:if>
		</c:forEach>
	</ul>
</div>

</html>
