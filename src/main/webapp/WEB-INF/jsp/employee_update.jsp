<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";

request.setAttribute("basePath", basePath);
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>职员信息修改</title>
<link href="${basePath}css/bootstrap.min.css" rel="stylesheet">
<script type="text/javascript">
window.onload = function() {
	var employeeName=document.getElementById("employeeName");
	var employeeName_span=document.getElementById("employeeName_span")
	
	var form=document.getElementById("form");
	var button=document.getElementById("button");
	var flag=false;
	
	employeeName.onblur = function() {
		if ("" == employeeName.value.trim() || null == employeeName.value) {
			employeeName_span.style.display = "inline-block";
			employeeName_span.innerHTML = "职员姓名不得为空！";
			flag = false;
		} else {
			employeeName_span.style.display = "none";
			flag = true;
		}
	
	button.onclick= function(){
		if(flag==true){
			employee.submit();
		}
		else{
			alert("添加失败！");
			}
		}
			
	}
		
}
</script>	
</head>
<body>
 <div class="container" id="content" style="text-align: center;">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<div class="row">
							<h2 style="text-align: center;">修改职员信息</h2>
						</div>
					</div> 
						<form class="form-horizontal col-lg-offset-4"
						 action="${basePath}sm/employee/edit"
						 name="employee" method="post">
							
							<br><br><br><br>
						<div class="form-group">
							<label class="col-lg-2 control-label">职员编号：</label>
							<div class="col-lg-4">
								<input type="text" class="form-control" name="employeeId" id="employeeId" 
								value="${employee.employeeId }" >
								<span id="employeeId_span"
										style="display: none; color: red"></span>
							</div>
						</div>
							
						<div class="form-group">
							<label class="col-lg-2 control-label">职员姓名：</label>
							<div class="col-lg-4">
								<input type="text" class="form-control" name="employeeName" id="employeeName" 
								value="${employee.employeeName }" >
								<span id="employeeName_span"
										style="display: none; color: red"></span>
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">部门编号：</label>
							<div class="col-lg-4">
								<input type="text" class="form-control" name="employeeDepartmentId" id="employeeDepartmentId" 
									value="${employee.employeeDepartmentId }" >
							</div>
									
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">职位编号：</label>
							<div class="col-lg-4">
								<input type="text" class="form-control" name="employeePositionId" id="employeePositionId" 
									value="${employee.employeePositionId }" >
									
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">职员英文名：</label>
							<div class="col-lg-4">
								<input type="text" class="form-control" name="employeeEnglishname" id="employeeEnglishname" 
									value="${employee.employeeEnglishname }" >
			
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">职员邮箱：</label>
							<div class="col-lg-4">
								<input type="text" class="form-control" name="employeeEmail" id="employeeEmail" 
									value="${employee.employeeEmail }" >
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">职员电话：</label>
							<div class="col-lg-4">
								<input type="text" class="form-control" name="employeePhoneNumber" id="employeePhoneNumber" 
									value="${employee.employeePhoneNumber }" >
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">职员状态：</label>
							<div class="col-lg-4">
								<input type="text" class="form-control" name="employeeState" id="employeeState" 
									 value="${employee.employeeState }">
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">职员座机：</label>
							<div class="col-lg-4">
								<input type="text" class="form-control" name="employeeTel" id="employeeTel" 
									value="${employee.employeeTel }" >
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">职员位置：</label>
							<div class="col-lg-4">
								<input type="text" class="form-control" name="employeeAddress" id="employeeAddress" 
									value="${employee.employeeAddress }" >
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">入职时间：</label>
							<div class="col-lg-4">
								<input type="text" class="form-control" name="employeeInductionTime" id="employeeInductionTime" 
									value="<fmt:formatDate value="${employee.employeeInductionTime}" pattern="yyyy-MM-dd"/>" >
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">更改人：</label>
							<div class="col-lg-4">
								<input type="text" class="form-control" name="employeeChangerId" id="employeeChangerId" 
									value="${employee.employeeChangerId}" >
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">职员性别：</label>
							<div class="col-lg-4">
								<input type="text" class="form-control" name="employeeGender" id="employeeGender" 
									value="${employee.employeeGender }" >
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">职员出生日期：</label>
							<div class="col-lg-4">
								<input type="text" class="form-control" name="employeeBorn" id="employeeBorn" 
									value="<fmt:formatDate value="${employee.employeeBorn}" pattern="yyyy-MM-dd"/>" >
							</div>
						</div>
							
							<br>
							<div class="form-group">
								<div class="col-lg-4 col-lg-offset-2">
									<input type="button" value="提交"  class="btn btn-danger" id="button">
									&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; 
									<input type="reset" value="重置"name="reset" class="btn btn-danger">
								</div>
							</div>
						</form>
						 </div>
					</div>
				</div>
			</div> 
						
</body>
</html>