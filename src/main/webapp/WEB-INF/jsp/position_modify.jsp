<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";

request.setAttribute("basePath", basePath);
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>职位修改</title>
<link href="${basePath}css/bootstrap.min.css" rel="stylesheet">
<script src="${basePath}js/jquery-3.2.1.min.js"></script>
<script type="text/javascript">

	$(document).ready(function(){
		$("#positionName").blur(function(){
			if(""==$("#positionName").val()||null==$("#positionName").val()){
				$("#positionName_span").css("display","inline-block");
				$("#positionName_span").text("职位名称不能为空！");
				flag=false;
			}else{
				$("#positionName_span").css("display","none");
				flag=true;
			}
		});
		
		$("#button").click(function(){
			if(flag==true){
				position.submit();
			}
			else
				alert("修改失败！");
		});
	});
	
	
</script>	
</head>
<body>
 <div class="container" id="content" style="text-align: center;">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<div class="row">
							<h2 style="text-align: center;">修改职位信息</h2>
							<button class="btn btn-default col-md-2" style="margin-top: 20px" onClick="location.href='${basePath}sm/positions'">
								返回	
							</button>
						</div>
					</div> 
						<form class="form-horizontal col-lg-offset-4"
						 action="${basePath}sm/position/edit"
						 name="position" method="post">
							
							<br><br><br><br>
							
							<div class="form-group">
								<label class="col-lg-2 control-label">职位ID：</label>
								<div class="col-lg-4">
									<input type="text" class="form-control" name="positionId" id="positionId" value="${position.positionId }" >
										
								</div>
							</div>
							<div class="form-group">
								<label class="col-lg-2 control-label">职位名称：</label>
								<div class="col-lg-4">
									<input type="text" class="form-control" name="positionName" id="positionName" value="${position.positionName }" >	
										<span id="positionName_span"
										style="display: none; color: red"></span>	
								</div>
							</div>
							
							<!-- <div class="form-group">
								<label class="col-lg-2 control-label">修改人名称：</label>
								<div class="col-lg-4">
									<input class="form-control" name="positionChangerId" id="positionChangerId"  value="${position.positionChangerId }">	
								</div>
								<span id="positionChangerId_span"
										style="display: none; color: red"></span>	
							</div> --!>
							<!-- <div class="form-group">
								<label class="col-lg-2 control-label">修改人名：</label>
								<div class="col-lg-4">
									<input type="text" class="form-control" name="positionUpdatedName" id="updateName"
									placeholder="请输入修改人名" required="required" >
									 <span id="updateName_span"
										style="display: none; color: red"></span>
								</div>
							</div> -->
							<!-- <div class="form-group">
								<label class="col-lg-2 control-label">修改时间：</label>
								<div class="col-lg-4">
									<input type="datetime-local" class="form-control" name="positionChangedTime" id="positionChangedTime" 
										 >	
											
								</div>
							</div> -->
							
							<br>
							<div class="form-group">
								<div class="col-lg-4 col-lg-offset-2">
									<input type="button" value="提交"  class="btn btn-danger" id="button">
									&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; 
									<input type="reset" value="重置"name="reset" class="btn btn-danger">
								</div>
							</div>
						</form>
						 </div>
					</div>
				</div>
			</div> 
						
</body>
</html>