<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
	request.setAttribute("basePath", basePath);
%>
<!DOCTYPE html>
<html>
<head>
<title>用户信息显示</title>

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- 引入bootstrap -->
<link rel="stylesheet" type="text/css"
	href="${basePath}/css/bootstrap.min.css">
<!-- 引入JQuery  bootstrap.js-->
<script src="${basePath}/js/jquery-3.2.1.min.js"></script>
<script src="${basePath}/js/bootstrap.min.js"></script>
<%! int i=1;%>
<%--<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">--%>

</head>
<body>
	<div class="container" id="content">
		<div class="row">
			<div class="col-md-10">
				<div class="panel panel-default">
					<div class="panel-heading">
						<div class="row">
							<h1 class="col-md-5">用户管理</h1>
							<form class="bs-example bs-example-form col-md-5" role="form"
								style="margin: 20px 0 10px 0;" action="selectById"
								id="form1" method="get">
								<div class="input-group">
									<input type="text" class="form-control" placeholder="请输入用户名" name="accountId">
							        <div class="form-group" style="text-align: right">
							        	<button class="btn btn-default" type="submit">提交</button>
							        </div>
								</div>
							</form>
							<button class="btn btn-default col-md-2" style="margin-top: 20px"
								onClick="location.href='toAdd'">
								新增用户信息
							</button>

						</div>
					</div>
					<table class="table table-bordered">
						<thead>
							<tr>
								<th>编号</th>
								<th>用户名</th>
								<th>密码</th>
							</tr>
						</thead>
						<tbody>
		 					<c:forEach items="${requestScope.accounts}" var="account"
								varStatus="stauts">
								<tr>

									<td><%=i++ %></td>
									<td>${account.accountName}</td>
									<td>${account.accountPwd}</td>
									<td>
										<button class="btn btn-default btn-xs btn-info"
											onClick="location.href='toUpdate?accountId=${account.accountId}'">编辑</button>
										<button class="btn btn-default btn-xs btn-danger btn-primary"
											onClick="location.href='del?accountId=${account.accountId}'">删除</button>
									</td>
								</tr>
							</c:forEach>  
						</tbody>
					</table>
				</div>

			</div>
		</div>
	</div>
	<div class="container" id="footer">
		<div class="row">
			<div class="col-md-12"></div>
		</div>
	</div>
</body>
</html>