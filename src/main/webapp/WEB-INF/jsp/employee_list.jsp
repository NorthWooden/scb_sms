<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
	request.setAttribute("basePath", basePath);
%>
<!DOCTYPE html>
<html>
<head>
<title>职员信息显示</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- 引入bootstrap -->
<link rel="stylesheet" type="text/css"
	href="${basePath}/css/bootstrap.min.css">
<!-- 引入JQuery  bootstrap.js-->
<script src="${basePath}/js/jquery-3.2.1.min.js"></script>
<script src="${basePath}/js/bootstrap.min.js"></script>
<%!int i = 1;%>
<%--<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">--%>
</head>
<body>
	<!-- 顶栏 -->
	<jsp:include page="top.jsp"></jsp:include>
	<!-- 中间主体 -->
	<div class="container" id="content">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<div class="row">
							<h1 class="col-md-5">职员信息显示</h1>
							<form class="bs-example bs-example-form col-md-5" role="form"
								style="margin: 20px 0 10px 0;" action="employee"
								id="form1" method="post">
								<div class="input-group">
									<input type="text" class="form-control" placeholder="请输入职员编号"
										name="employeeId"> <span class="input-group-addon btn"
										id="sub">查询</span>
								</div>
							</form>

							<button class="btn btn-default col-md-2" style="margin-top: 20px"
								onClick="location.href='${basePath}sm/employee/add'">
								新增职员 <span class="glyphicon glyphicon-plus"></span>
							</button>
						</div>
					</div>
					<div class="table-responsive">
						<table class="table">
							<thead>
								<tr>
									<th nowrap="nowrap">职员编号</th>
									<th nowrap="nowrap">职员姓名</th>
									<th nowrap="nowrap">部门编号</th>
									<th nowrap="nowrap">职位编号</th>
									<th nowrap="nowrap">职员英文名</th>
									<th nowrap="nowrap">职员邮箱</th>
									<th nowrap="nowrap">职员电话</th>
									<th nowrap="nowrap">职员状态</th>
									<th nowrap="nowrap">职员座机</th>
									<th nowrap="nowrap">职员位置</th>
									<th nowrap="nowrap">入职时间</th>
									<th nowrap="nowrap">创建人</th>
									<th nowrap="nowrap">创建时间</th>
									<th nowrap="nowrap">更改人</th>
									<th nowrap="nowrap">更改时间</th>
									<th nowrap="nowrap">性别</th>
									<th nowrap="nowrap">出生日期</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${requestScope.emps}" var="employee"
									varStatus="stauts">

									<tr>
										<td>${employee.employeeId }</td>
										<td>${employee.employeeName}</td>
										<td>${employee.employeeDepartmentId}</td>
										<td>${employee.employeePositionId}</td>
										<td>${employee.employeeEnglishname}</td>
										<td>${employee.employeeEmail}</td>
										<td>${employee.employeePhoneNumber}</td>
										<td>${employee.employeeState}</td>
										<td>${employee.employeeTel}</td>
										<td>${employee.employeeAddress}</td>
										<td><fmt:formatDate
												value="${employee.employeeInductionTime}"
												pattern="yyyy-MM-dd" /></td>
										<td>${employee.employeeCreatorId}</td>
										<td><fmt:formatDate
												value="${employee.employeeCreatedTime}"
												pattern="yyyy-MM-dd HH:mm:ss" /></td>
										<td>${employee.employeeChangerId}</td>
										<td><fmt:formatDate
												value="${employee.employeeChangedTime}"
												pattern="yyyy-MM-dd HH:mm:ss" /></td>
										<td>${employee.employeeGender}</td>
										<td><fmt:formatDate value="${employee.employeeBorn}"
												pattern="yyyy-MM-dd" /></td>

										<td><a
											href="${basePath}sm/employee/edit/${employee.employeeId}">
												<button class="btn btn-info"
													onclick="return confirmUpdate()">
													<span class="glyphicon glyphicon-pencil" aria-hidden="true">
													</span> 编辑
												</button>
										</a></td>
										<td><a
											href="${basePath}sm/employee/delete/${employee.employeeId}">
												<button class="btn btn-danger"
													onclick="return confirmDelete()">
													<span class="glyphicon glyphicon-trash" aria-hidden="true">
													</span> 删除
												</button>
										</a></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>


				</div>

			</div>
		</div>
	</div>
	<div class="container" id="footer">
		<div class="row">
			<div class="col-md-12"></div>
		</div>
	</div>
</body>
<script type="text/javascript">
	$("#nav li:nth-child(2)").addClass("active");

	function confirmDelete() {
		return confirm("确定删除该职员吗");
	}

	function confirmUpdate() {
		return confirm("确定修改该职员吗");
	}

	$("#sub").click(function() {
		$("#form1").submit();
	});
</script>
</html>