<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
 <%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
request.setAttribute("basePath", basePath);
%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>

<link href="${basePath}css/bootstrap.min.css" rel="stylesheet">
<!-- 引入JQuery  bootstrap.js-->
<script src="${basePath}js/jquery-3.2.1.min.js"></script>
<script src="${basePath}js/bootstrap.min.js"></script>
<script type="text/javascript">
	window.onload = function() {
		var departmentName = document.getElementById("departmentName");
		var departmentName_span = document.getElementById("departmentName_span");
		var departmentLeader = document.getElementById("departmentLeader");
		var departmentLeader_span = document.getElementById("departmentLeader_span");

		var flag=false;
		departmentName.onblur = function() {
			if ("" == departmentName.value.trim() || null == departmentName.value) {
				departmentName_span.style.display = "inline-block";
				departmentName_span.innerHTML = "名称不得为空！";
				flag = false;
			} else {
				departmentName_span.style.display = "none";
				flag = true;
			}
		}
		departmentLeader.onblur=function(){
			var employeeId=departmentLeader.value;
			$.ajax({
				type:"get",
				url:"findEmployee",
				dataType: "json",
				data:{
					employeeId:employeeId
				},
				async: false,
				success:function(data){
					//departmentLeader_span.style.display = "none";
					departmentLeader_span.style.display = "inline-block";
					departmentLeader_span.innerHTML = "职员姓名："+data.employeeName;
					flag = true;
				},
				error:function(data){
					departmentLeader_span.style.display = "inline-block";
					departmentLeader_span.innerHTML = "职员不存在！";
					flag = false;
				}
			})
		}
	
		function checkForm() {
			return flag;
		}
	}
</script>	
</head>
<body>
	
	<form class="form-horizontal col-lg-offset-4"
	 action="${basePath}sm/depart"
		 method="post" onsubmit="return checkForm()">
		<h3>Department Add</h3>
		<br><br><br><br>

		
		<div class="form-group">
			<label class="col-lg-2 control-label">部门名称：</label>
			<div class="col-lg-4">
				<input type="text" class="form-control" name="departmentName" id="departmentName"
					placeholder="请输入部门名称" required="required" >
					 <span id="departmentName_span"
					style="display: none; color: red"></span>
			</div>
		</div>
		<div class="form-group">
			<label class="col-lg-2 control-label">部门领导：</label>
			<div class="col-lg-4">
				<input type="text" class="form-control" name="departmentLeader" id="departmentLeader"
				placeholder="请输入创建人名" required="required" >
				<span id="departmentLeader_span"
					style="display: none; color: red"></span>
				 
			</div>
		</div>
		
		<div class="form-group">
			<label class="col-lg-2 control-label">上级部门：</label>
			<div class="col-lg-4">
				<select class="form-control" name="departmentPid" id="departmentPid" required="required">
					<option value="-1">没有上级</option>
					<c:forEach items="${departmentList }" var="department">
						<option value="${department.departmentId }">${department.departmentName }</option>
					</c:forEach>
				</select>
			</div>
		</div>
		
		<div class="form-group">
			<label class="col-lg-2 control-label">部门描述：</label>
			<div class="col-lg-4">
				<textarea class="form-control" name="departmentDescription"></textarea>
			</div>
		</div>
		<br>
		<div class="form-group">
			<div class="col-lg-4 col-lg-offset-2">
				<input type="submit" value="提交" name="submit" class="btn btn-danger">
				&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; 
				<input type="reset" value="重置" name="reset" class="btn btn-danger">
			</div>
		</div>
	</form>
		
	
</body>
</html>