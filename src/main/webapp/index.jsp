<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<!DOCTYPE html>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path;
%>
<html>
<head>
<title></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- 引入bootstrap -->
<link rel="stylesheet" type="text/css"
	href="<%=basePath%>/css/bootstrap.min.css">
<!-- 引入JQuery  bootstrap.js-->
<script src="<%=basePath%>/js/jquery-3.2.1.min.js"></script>
<script src="<%=basePath%>/js/bootstrap.min.js"></script>
<style type="text/css">
body {
	background: url(<%=basePath%>/images/a.jpg) repeat;
}

#login-box {
	/*border:1px solid #F00;*/
	padding: 35px;
	border-radius: 15px;
	background: #56666B;
	color: #FAEBD7;
}

.test {
	font-size: 60px;
	color: #FF0000;
	font-style: italic;
}
</style>
</head>
<body>
	<center>
		<a class="test"
			href="http://localhost:8092/com.scb.dev.sms/sm/showLogon">Login</a>
	</center>
</body>
</html>