/*
Navicat MySQL Data Transfer

Source Server         : 144.202.55.191_3306
Source Server Version : 50173
Source Host           : 144.202.55.191:3306
Source Database       : sms

Target Server Type    : MYSQL
Target Server Version : 50173
File Encoding         : 65001

Date: 2018-11-22 11:36:05
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `sm_account`
-- ----------------------------
DROP TABLE IF EXISTS `sm_account`;
CREATE TABLE `sm_account` (
  `ACCOUNT_ID` varchar(32) NOT NULL COMMENT '账户编号 使用uuid',
  `ACCOUNT_NAME` varchar(25) NOT NULL COMMENT '账户名称',
  `ACCOUNT_PWD` varchar(32) NOT NULL COMMENT '账户密码 生成规则：随机生成',
  `ACCOUNT_CHANGER_ID` varchar(32) NOT NULL COMMENT '账户修改人',
  `ACCOUNT_CHANGED_TIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '账户修改时间',
  `ACCOUNT_INVISIBLE` int(11) NOT NULL DEFAULT '0' COMMENT '是否删除 1：不可用 0：可用',
  PRIMARY KEY (`ACCOUNT_ID`),
  UNIQUE KEY `ACCOUNT_NAME` (`ACCOUNT_NAME`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sm_account
-- ----------------------------
INSERT INTO `sm_account` VALUES ('12345678123456781234567812345678', 'mybatis', 'luodawda', '12345678123456781234567812345678', '2018-11-22 11:06:27', '0');
INSERT INTO `sm_account` VALUES ('508E3C94F8F113DA10BFE90FBE6B22E4', '154876564', '926E411B674E9FD712D488D7297AA7CA', '56156156', '2018-11-22 11:08:18', '0');

-- ----------------------------
-- Table structure for `sm_account_log`
-- ----------------------------
DROP TABLE IF EXISTS `sm_account_log`;
CREATE TABLE `sm_account_log` (
  `ACCOUNT_LOG_ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '账户日志编号 自增',
  `ACCOUNT_LOG_OPERATION` varchar(20) NOT NULL COMMENT '账号日志操作',
  `ACCOUNT_LOG_OPERATION_TIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '账户日志操作时间',
  `ACCOUNT_LOG_OPERATOR` varchar(32) NOT NULL COMMENT '账户操作人',
  `ACCOUNT_LOG_OPERATION_CONTENT` varchar(300) NOT NULL COMMENT '账户操作内容',
  `ACCOUNT_UPDATE_PWD` varchar(20) NOT NULL COMMENT '账户密码修改',
  PRIMARY KEY (`ACCOUNT_LOG_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sm_account_log
-- ----------------------------

-- ----------------------------
-- Table structure for `sm_department`
-- ----------------------------
DROP TABLE IF EXISTS `sm_department`;
CREATE TABLE `sm_department` (
  `DEPARTMENT_ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '部门编号',
  `DEPARTMENT_NAME` varchar(15) NOT NULL COMMENT '部门名称',
  `DEPARTMENT_ISVISIBLE` int(11) DEFAULT '0' COMMENT '是否可用 1：不可用 0：可用',
  `DEPARTMENT_DESCRIPTION` varchar(100) DEFAULT NULL COMMENT '部门介绍',
  `DEPARTMENT_LEADER` varchar(32) NOT NULL COMMENT '部门领导',
  `DEPARTMENT_PID` int(11) NOT NULL COMMENT '上级部门',
  `DEPARTMENT_CREATOR_ID` varchar(32) NOT NULL,
  `DEPARTMENT_CREATED_TIME` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `DEPARTMENT_CHANGER_ID` varchar(32) DEFAULT NULL,
  `DEPARTMENT_CHANGED_TIME` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`DEPARTMENT_ID`),
  UNIQUE KEY `DEPARTMENT_NAME` (`DEPARTMENT_NAME`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sm_department
-- ----------------------------
INSERT INTO `sm_department` VALUES ('1', 'RB', '0', 'best Department', '885bb5b002474df9868a491b5d5333bb', '1', '1', '2018-11-19 17:17:34', null, '2018-11-22 09:22:01');
INSERT INTO `sm_department` VALUES ('2', 'TS', '0', 'also best Department', '885bb5b002474df9868a491b5d5333bb', '2', '2', '2018-11-14 11:56:02', null, '2018-11-22 09:22:03');
INSERT INTO `sm_department` VALUES ('27', 'WW', '0', 'very good', '885bb5b002474df9868a491b5d5333bb', '1', '1', '0000-00-00 00:00:00', null, '2018-11-22 09:22:05');
INSERT INTO `sm_department` VALUES ('28', 'DZD', '0', 'gogogo', '885bb5b002474df9868a491b5d5333bb', '1', '1024', '2018-11-19 20:33:06', null, '2018-11-22 09:22:09');
INSERT INTO `sm_department` VALUES ('32', 'DD', '0', 'dddddd', '885bb5b002474df9868a491b5d5333bb', '28', '1024', '0000-00-00 00:00:00', null, null);

-- ----------------------------
-- Table structure for `sm_department_log`
-- ----------------------------
DROP TABLE IF EXISTS `sm_department_log`;
CREATE TABLE `sm_department_log` (
  `DEPARTMENT_LOG_ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '日志编号',
  `DEPARTMENT_LOG_CREATE_TIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '生成时间',
  `DEPARTMENT_LOG_CONTENT` varchar(300) NOT NULL COMMENT '日志内容',
  `DEPARTMENT_LOG_OPERATION_TYPE` varchar(20) NOT NULL COMMENT '操作类别',
  `DEPARTMENT_LOG_OPERATION_ID` varchar(32) NOT NULL COMMENT '操作账号',
  PRIMARY KEY (`DEPARTMENT_LOG_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sm_department_log
-- ----------------------------

-- ----------------------------
-- Table structure for `sm_employee`
-- ----------------------------
DROP TABLE IF EXISTS `sm_employee`;
CREATE TABLE `sm_employee` (
  `EMPLOYEE_ID` varchar(32) NOT NULL COMMENT '职员编号 uuid生成',
  `EMPLOYEE_NAME` varchar(25) NOT NULL COMMENT '职员姓名',
  `EMPLOYEE_GENDER` varchar(1) NOT NULL COMMENT '职员性别',
  `EMPLOYEE_BORN` timestamp NULL DEFAULT NULL COMMENT '职员生日 yyyy-mm-dd',
  `EMPLOYEE_DEPARTMENT_ID` int(11) NOT NULL COMMENT '部门编号',
  `EMPLOYEE_POSITION_ID` int(11) NOT NULL COMMENT '职位编号',
  `EMPLOYEE_ENGLISHNAME` varchar(25) NOT NULL COMMENT '职员英文名',
  `EMPLOYEE_EMAIL` varchar(30) NOT NULL COMMENT '职员邮箱',
  `EMPLOYEE_PHONE_NUMBER` varchar(25) NOT NULL COMMENT '职员电话',
  `EMPLOYEE_STATE` varchar(4) NOT NULL COMMENT '职员状态',
  `EMPLOYEE_TEL` varchar(12) DEFAULT NULL COMMENT '职员座机',
  `EMPLOYEE_ADDRESS` varchar(100) NOT NULL COMMENT '职员位置',
  `EMPLOYEE_INDUCTION_TIME` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '入职时间',
  `EMPLOYEE_CREATOR_ID` varchar(32) NOT NULL COMMENT '创建人',
  `EMPLOYEE_CREATED_TIME` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `EMPLOYEE_CHANGER_ID` varchar(32) DEFAULT NULL COMMENT '更改人',
  `EMPLOYEE_CHANGED_TIME` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP COMMENT '更改时间',
  PRIMARY KEY (`EMPLOYEE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sm_employee
-- ----------------------------
INSERT INTO `sm_employee` VALUES ('885bb5b002474df9868a491b5d5333bb', '小明', '男', '1997-02-23 00:00:00', '1', '1001', 'zack', 'zack@clpsglobal.com', '11111', '在职', null, '北京', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', null, '0000-00-00 00:00:00');
INSERT INTO `sm_employee` VALUES ('a09d39d0137a4517914bf66cc80d71e1', '李四', '男', '1995-09-06 00:00:00', '12', '1003', 'lisi', 'Si.Li@clpsglobal.com', '15075465455', '在职', null, '北京市', '2008-09-06 00:00:00', 'admin', '2018-11-21 15:41:19', null, '0000-00-00 00:00:00');
INSERT INTO `sm_employee` VALUES ('a9c0d7d328ec42a5a7472773ddfb62e6', '张三', '男', '1995-09-06 00:00:00', '11', '1001', 'zhangsan', 'San.Zhang@clpsglobal.com', '15070123698', '在职', '8645789', '深圳市', '2018-09-06 00:00:00', 'admin', '2018-11-21 15:48:36', 'admin', '2018-11-21 15:48:36');

-- ----------------------------
-- Table structure for `sm_employee_log`
-- ----------------------------
DROP TABLE IF EXISTS `sm_employee_log`;
CREATE TABLE `sm_employee_log` (
  `EMPLOYEE_LOG_ID` int(8) NOT NULL AUTO_INCREMENT COMMENT '职员日志编号',
  `EMPLOYEE_LOG_OPERATION` varchar(8) NOT NULL COMMENT '职员日志操作',
  `EMPLOYEE_LOG_OPERATION_TIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '职员日志操作时间',
  `EMPLOYEE_LOG_OPERATION_CONTENT` varchar(300) NOT NULL COMMENT '志愿日志操作内容',
  `EMPLOYEE_LOG_OPERATOR` varchar(32) NOT NULL COMMENT '职员日志操作人',
  PRIMARY KEY (`EMPLOYEE_LOG_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sm_employee_log
-- ----------------------------

-- ----------------------------
-- Table structure for `sm_logon_log`
-- ----------------------------
DROP TABLE IF EXISTS `sm_logon_log`;
CREATE TABLE `sm_logon_log` (
  `LOGON_LOG_ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '登录日志编号',
  `LOGON_LOG_OPERATION` varchar(20) NOT NULL COMMENT '登录日志操作',
  `LOGON_LOG_OPERATION_TIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '登录日志操作时间',
  `LOGON_LOG_OPERATOR` varchar(32) NOT NULL COMMENT '登录日志操作人',
  `LOGON_LOG_OPERATION_CONTENT` varchar(200) NOT NULL COMMENT '登录操作内容',
  PRIMARY KEY (`LOGON_LOG_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sm_logon_log
-- ----------------------------

-- ----------------------------
-- Table structure for `sm_menu`
-- ----------------------------
DROP TABLE IF EXISTS `sm_menu`;
CREATE TABLE `sm_menu` (
  `MENU_ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '菜单编号',
  `MENU_PID` int(11) NOT NULL COMMENT '菜单父级编号',
  `MENU_NAME` varchar(25) CHARACTER SET utf8 NOT NULL COMMENT '菜单名称',
  `MENU_DESCRIPITION` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '菜单描述',
  `MENU_CREATOR_ID` varchar(30) CHARACTER SET utf8 NOT NULL COMMENT '创建人',
  `MENU_CREATED_TIME` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `MENU_CHANGER_ID` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `MENU_CHANGED_TIME` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `MENU_URL` varchar(500) CHARACTER SET utf8 NOT NULL COMMENT '菜单url',
  `MENU_LEVEL` varchar(5) CHARACTER SET utf8 NOT NULL COMMENT '菜单等级',
  `MENU_ORDER` varchar(10) CHARACTER SET utf8 NOT NULL COMMENT '菜单排序',
  `MENU_ISVISIBLE` int(11) NOT NULL DEFAULT '1' COMMENT '是否可用 1：不可用 0：可用',
  PRIMARY KEY (`MENU_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sm_menu
-- ----------------------------
INSERT INTO `sm_menu` VALUES ('1', '1', '用户信息', '用户信息操作', '1512480230', '2018-11-16 15:51:03', '1512480230', '2018-11-21 10:56:15', 'www.baidu.com', '1', '1', '1');
INSERT INTO `sm_menu` VALUES ('2', '2', '部门管理', '部门管理操作', '1512480233', '2018-11-20 09:17:35', '1512480233', '2018-11-15 09:46:09', 'www.sougou.com', '1', '2', '1');
INSERT INTO `sm_menu` VALUES ('3', '1', '修改密码', '修改密码操作', '1512480654', '2018-11-20 09:17:21', '1512482065', '2018-11-15 10:20:55', 'www.google.com', '2', '1', '1');
INSERT INTO `sm_menu` VALUES ('4', '2', '添加部门', '部门管理操作', '1512480230', '2018-11-20 09:18:49', '1512480230', '2018-11-15 11:04:06', 'www.qq.com', '2', '1', '1');
INSERT INTO `sm_menu` VALUES ('5', '1', '修改信息', '修改信息操作', '1512480233', '2018-11-20 09:19:29', '1512480222', '2018-11-15 17:22:24', 'www.4399.com', '2', '2', '1');
INSERT INTO `sm_menu` VALUES ('7', '2', '删除部门', '删除部门操作', '1512480230', '2018-11-20 09:19:43', '1512480230', '2018-11-16 09:24:42', 'www.Alibaba.com', '2', '2', '1');
INSERT INTO `sm_menu` VALUES ('8', '8', '菜单管理', '菜单管理操作', '1512480222', '2018-11-20 11:50:09', '1548987654', '2018-11-21 15:55:12', 'http://localhost:8092/com.scb.dev.sms/menu/show', '1', '3', '1');
INSERT INTO `sm_menu` VALUES ('9', '8', '菜单添加', '菜单添加操作', '1512480230', '2018-11-20 10:57:43', '1512480230', '2018-11-21 15:55:42', 'http://localhost:8092/com.scb.dev.sms/menu/toAdd', '2', '1', '1');
INSERT INTO `sm_menu` VALUES ('10', '8', '菜单删除', '菜单删除操作', '1512480222', '2018-11-20 10:57:43', '1512480222', '2018-11-21 15:55:26', 'http://localhost:8092/com.scb.dev.sms/menu/show', '2', '2', '1');
INSERT INTO `sm_menu` VALUES ('11', '8', '菜单更新', '菜单更新操作', '1512480222', '2018-11-20 10:57:43', '1512482222', '2018-11-21 15:55:46', 'http://localhost:8092/com.scb.dev.sms/menu/show', '2', '3', '1');

-- ----------------------------
-- Table structure for `sm_menu_log`
-- ----------------------------
DROP TABLE IF EXISTS `sm_menu_log`;
CREATE TABLE `sm_menu_log` (
  `MENU_LOG_ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '菜单日志编号',
  `MENU_LOG_OPERATION` varchar(20) CHARACTER SET utf8 NOT NULL COMMENT '菜单日志操作',
  `MENU_LOG_OPERATOR` varchar(32) CHARACTER SET utf8 NOT NULL COMMENT '菜单日志操作人',
  `MENU_LOG_OPERATION_TIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '菜单日志操作时间',
  `MENU_LOG_OPERATION_CONTENT` varchar(300) CHARACTER SET utf8 NOT NULL COMMENT '菜单日志操作内容',
  PRIMARY KEY (`MENU_LOG_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sm_menu_log
-- ----------------------------

-- ----------------------------
-- Table structure for `sm_permission`
-- ----------------------------
DROP TABLE IF EXISTS `sm_permission`;
CREATE TABLE `sm_permission` (
  `PERMISSION_ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '权限编号',
  `PERMISSION_NAME` varchar(25) NOT NULL COMMENT '权限名称',
  `PERMISSION_POSITION_ID` int(11) NOT NULL COMMENT '职位编号',
  `PERMISSION_MENU_ID` int(11) NOT NULL COMMENT '菜单编号',
  PRIMARY KEY (`PERMISSION_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=1004 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sm_permission
-- ----------------------------
INSERT INTO `sm_permission` VALUES ('1', '删除业务', '10010', '1');
INSERT INTO `sm_permission` VALUES ('2', '查看业务', '10050', '2');
INSERT INTO `sm_permission` VALUES ('3', '添加业务', '10080', '1');
INSERT INTO `sm_permission` VALUES ('4', '删除业务', '10080', '2');
INSERT INTO `sm_permission` VALUES ('5', '修改业务', '10080', '8');
INSERT INTO `sm_permission` VALUES ('6', '修改业务', '10080', '4');

-- ----------------------------
-- Table structure for `sm_permission_log`
-- ----------------------------
DROP TABLE IF EXISTS `sm_permission_log`;
CREATE TABLE `sm_permission_log` (
  `PERMISSION_LOG_ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '权限日志编号',
  `PERMISSION_LOG_OPERATION` varchar(20) NOT NULL COMMENT '权限日志操作',
  `PERMISSION_LOG_OPERATOR` varchar(32) NOT NULL COMMENT '权限日志操作人',
  `PERMISSION_LOG_OPERATION_TIME` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP COMMENT '权限日志操作时间',
  `PERMISSION_LOG_OPERATION_CONTENT` varchar(300) NOT NULL COMMENT '权限操作内容',
  PRIMARY KEY (`PERMISSION_LOG_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sm_permission_log
-- ----------------------------

-- ----------------------------
-- Table structure for `sm_position`
-- ----------------------------
DROP TABLE IF EXISTS `sm_position`;
CREATE TABLE `sm_position` (
  `POSITION_ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '职员编号',
  `POSITION_NAME` varchar(25) NOT NULL COMMENT '职员姓名',
  `POSITION_CREATOR_ID` varchar(32) NOT NULL COMMENT '创建人',
  `POSITION_CREATED_TIME` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `POSITION_CHANGER_ID` varchar(32) DEFAULT NULL COMMENT '更改人',
  `POSITION_CHANGED_TIME` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更改时间',
  PRIMARY KEY (`POSITION_ID`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10081 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sm_position
-- ----------------------------
INSERT INTO `sm_position` VALUES ('10010', '项目经理', 'admin', '2018-11-15 11:44:22', 'admin', '2018-11-21 14:30:11');
INSERT INTO `sm_position` VALUES ('10030', '测试职位2', 'admin', '2018-11-17 13:56:24', 'admin', '2018-11-21 17:35:33');
INSERT INTO `sm_position` VALUES ('10050', '业务员', 'admin', '2018-11-20 11:54:51', 'admin', '2018-11-21 15:26:52');
INSERT INTO `sm_position` VALUES ('10060', '测试职位3', 'admin', '2018-11-20 00:00:00', null, null);
INSERT INTO `sm_position` VALUES ('10080', '系统管理员', 'admin', '2018-11-16 14:19:57', null, null);

-- ----------------------------
-- Table structure for `sm_position_log`
-- ----------------------------
DROP TABLE IF EXISTS `sm_position_log`;
CREATE TABLE `sm_position_log` (
  `POSITION_LOG_ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '职位日志标号',
  `POSITION_LOG_OPERATION` varchar(20) NOT NULL COMMENT '职员日志操作',
  `POSITION_LOG_OPERATION_TIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '职员日志操作时间',
  `POSITION_LOG_OPERATOR` varchar(32) NOT NULL COMMENT '职员日志操作人',
  `POSITION_LOG_OPERATION_CONTENT` varchar(300) NOT NULL COMMENT '职员日志操作内容',
  PRIMARY KEY (`POSITION_LOG_ID`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sm_position_log
-- ----------------------------
