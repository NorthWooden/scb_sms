/**
 * Project Name:clps.sms
 * File Name:CommonDate.java
 * Package Name:com.clps.dev.sms.common
 * Date:2018年10月31日上午11:39:32
 * Copyright (c) 2018, gael.zhu@qq.com All Rights Reserved.
 *
 */
package com.scb.dev.sms.common;

/**
 * ClassName: CommonDate <br/>
 * Description: Common Data. <br/><br/>
 * date: 2018年10月31日 上午11:39:32 <br/>
 *
 * @author gael.zhu
 * @version V1.0
 * @since JDK 1.8
 */
public interface CommonData {
	
	//数据库操作状态码
	public static final String SUCCESS="001";
	public static final String FAILURES="002";
	
	public static final String SAVE_SUCCESS="001-001";
	public static final String UPDATE_SUCCESS="002-001";
	public static final String DELETE_SUCCESS="003-001";
	public static final String QUERY_SUCCESS="004-001";
	
	public static final String SAVE_FAILURE="001-002";
	public static final String UPDATE_FAILURE="002-002";
	public static final String DELETE_FAILURE="003-002";
	public static final String QUERY_FAILURE="004-002";

	
	//固定方法编码
	public static final String ADDACCOUNT="14413356";          //添加用户的方法编码
	
	
	//邮箱格式
	public static final String CLPS="clpsglobal.com";
	public static final String QQ="qq.com";
	public static final String WANGYI="163.com";
	
	
	
	//时间(时区，格式)编码
	public static final String YYYY_MM_DD_HH_MM_12="yyyy-MM-dd hh:mm aa";    //12小时制度
	public static final String YYYY_MM_DD_HH_MM_24="yyyy-MM-dd HH:mm";    //24小时制度
	
	
	public static final String TIMEZONE_0800="Z";                          //'Z'大写+0800
	public static final String TIMEZONE_CST="z";                          //‘z’小写CST

	public static final String MMDDHHMM="MMddHHmm";                  //tokenId 需求格式
	
	public static final int[] TOKENIDRANDOM= {68,719,476,735};      //tokenId随机数
	
	
	
	//格式化编码
	public static final String EMAIL_NOEMAIL="Not A Email";                 //不是邮箱的标志
	public static final String EMAIL_AEMAIL="A Email";                      //是邮箱的标志
	public static final String EMAIL_WHETHERLEAGAL="^[\\u4E00-\\u9FA5A-Za-z0-9_]+$"; //非特殊字符
	public static final String EMAIL_SIGN = "@";                           //邮箱标志
	public static final int EMAIL_MIN=6;                                   //邮箱最小长度
	public static final int EMAIL_MAX=16;                                  //邮箱最大长度
	
	//菜单级别
	public static final String MENU_TOP="1";                   //菜单最上级节点标志
	
	//格式
	public static final String TOSTRING="";                   //To String
	
	//操作码 
	public static final String INSERTACCOUNT="01597469";         //插入用户的操作码
	
}
