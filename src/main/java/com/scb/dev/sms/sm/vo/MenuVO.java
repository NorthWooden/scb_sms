/**
 * Project Name:scb_sms
 * File Name:MenuVO.java
 * Package Name:com.scb.dev.sms.sm.vo
 * Date:2018年11月8日上午11:51:48
 * Copyright (c) 2018, gael.zhu@qq.com All Rights Reserved.
 *
 */
package com.scb.dev.sms.sm.vo;

import java.io.Serializable;

import org.springframework.stereotype.Component;

/**
 * ClassName: MenuVO <br/>
 * Description: MenuVO. <br/>
 * <br/>
 * date: 2018年11月8日 上午11:51:48 <br/>
 *
 * @author gael.zhu
 * @version V1.0
 * @since JDK 1.8
 */
@Component
public class MenuVO implements Serializable {

	private static final long serialVersionUID = -2219771266918620638L;
	
	private int menuId;                  //菜单ID
	
	private int menuPid;                 //菜单父菜单
	
	private String menuName;             //菜单名称
	
	private String menuUrl;              //菜单URL
	
	private String menuDescripition;     //菜单描述
	
	private String menuLevel;            //菜单级别
	
	private String menuOrder;            //菜单顺序

	/**
	 * menuId.
	 *
	 * @return the menuId
	 */
	public final int getMenuId() {
		return menuId;
	}

	/**
	 * menuId.
	 *
	 * @param menuId the menuId to set
	 */
	public final void setMenuId(int menuId) {
		this.menuId = menuId;
	}

	/**
	 * menuPid.
	 *
	 * @return the menuPid
	 */
	public final int getMenuPid() {
		return menuPid;
	}

	/**
	 * menuPid.
	 *
	 * @param menuPid the menuPid to set
	 */
	public final void setMenuPid(int menuPid) {
		this.menuPid = menuPid;
	}

	/**
	 * menuName.
	 *
	 * @return the menuName
	 */
	public final String getMenuName() {
		return menuName;
	}

	/**
	 * menuName.
	 *
	 * @param menuName the menuName to set
	 */
	public final void setMenuName(String menuName) {
		this.menuName = menuName;
	}

	/**
	 * menuURL.
	 *
	 * @return the menuURL
	 */
	public final String getMenuUrl() {
		return menuUrl;
	}

	/**
	 * menuURL.
	 *
	 * @param menuURL the menuURL to set
	 */
	public final void setMenuUrl(String menuURL) {
		this.menuUrl = menuURL;
	}

	/**
	 * menuDescripition.
	 *
	 * @return the menuDescripition
	 */
	public final String getMenuDescripition() {
		return menuDescripition;
	}

	/**
	 * menuDescripition.
	 *
	 * @param menuDescripition the menuDescripition to set
	 */
	public final void setMenuDescripition(String menuDescripition) {
		this.menuDescripition = menuDescripition;
	}

	/**
	 * menuLevel.
	 *
	 * @return the menuLevel
	 */
	public final String getMenuLevel() {
		return menuLevel;
	}

	/**
	 * menuLevel.
	 *
	 * @param menuLevel the menuLevel to set
	 */
	public final void setMenuLevel(String menuLevel) {
		this.menuLevel = menuLevel;
	}

	/**
	 * menuOrder.
	 *
	 * @return the menuOrder
	 */
	public final String getMenuOrder() {
		return menuOrder;
	}

	/**
	 * menuOrder.
	 *
	 * @param menuOrder the menuOrder to set
	 */
	public final void setMenuOrder(String menuOrder) {
		this.menuOrder = menuOrder;
	}
}
