/**
 * Project Name:scb_sms
 * File Name:MenuServiceImpl.java
 * Package Name:com.scb.dev.sms.sm.service.impl
 * Date:2018年11月8日上午9:28:36
 * Copyright (c) 2018, gael.zhu@qq.com All Rights Reserved.
 *
 */
package com.scb.dev.sms.sm.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.scb.dev.sms.common.CommonData;
import com.scb.dev.sms.sm.dao.MenuMapper;
import com.scb.dev.sms.sm.pojo.Menu;
import com.scb.dev.sms.sm.service.IMenuService;
import com.scb.dev.sms.sm.vo.MenuVO;
import com.scb.dev.sms.util.factory.ToolFactory;

/**
 * ClassName: MenuServiceImpl <br/>
 * Description: MenuService Impl. <br/><br/>
 * date: 2018年11月8日 上午9:28:36 <br/>
 *
 * @author gael.zhu
 * @version V1.0
 * @since JDK 1.8
 */
@Service
public class MenuServiceImpl implements IMenuService{

	@Autowired
	private MenuMapper menuMapper;
	
	@Autowired
	private List<MenuVO> menuVoGroup;
	
	@Autowired
	private MenuVO menuVo;
	
	private int flag=0;
	
	/**
	 * @see com.scb.dev.sms.sm.service.IMenuService#getAllMenu()
	 */
	@Override
	public List<MenuVO> getAllMenu() {
		this.menuVoGroup=this.menuMapper.selectAllMenu();
		if(null!=menuVoGroup) 
			return menuVoGroup;
		else
			return null;
	}
	

	/**
	 * @see com.scb.dev.sms.sm.service.IMenuService#insertMenu(com.scb.dev.sms.sm.pojo.Menu)
	 */
	@Override
	public String insertMenu(Menu menu) {
		if(0==menu.getMenuPid()) {
	       menu.setMenuLevel(CommonData.MENU_TOP);
		   menu.setMenuPid(this.menuMapper.selectAllMenu().size()+1);
		   menu.setMenuOrder(this.menuMapper.selectMenuByLevel(CommonData.MENU_TOP).size()+1+CommonData.TOSTRING);
		}
		else {
		   menu.setMenuLevel((Integer.parseInt((this.menuMapper.selectById(menu.getMenuPid())).getMenuLevel())+1)+CommonData.TOSTRING);
		   menu.setMenuOrder(this.menuMapper.selectMenuByPid(menu.getMenuPid()).size()+1+CommonData.TOSTRING);
		}
		menu.setMenuCreatedTime(ToolFactory.getInstanceOfDateTool().getTime());
		menu.setMenuChangedTime(ToolFactory.getInstanceOfDateTool().getTime());
		flag=this.menuMapper.insertSelective(menu);
		if(flag>=1) {
			return CommonData.SAVE_SUCCESS;
		}
		else
			return CommonData.SAVE_FAILURE;
	}

	/**
	 * @see com.scb.dev.sms.sm.service.IMenuService#updateMenu(com.scb.dev.sms.sm.pojo.Menu)
	 */
	@Override
	public String updateMenu(Menu menu) {
		if(0==menu.getMenuPid()) {
			menu.setMenuLevel(CommonData.MENU_TOP);
			menu.setMenuPid(menu.getMenuId());
			menu.setMenuOrder(this.menuMapper.selectMenuByLevel(CommonData.MENU_TOP).size()+1+CommonData.TOSTRING);
			
		}
		else {
			menu.setMenuLevel((Integer.parseInt((this.menuMapper.selectById(menu.getMenuPid())).getMenuLevel())+1)+CommonData.TOSTRING);
			
			
			menu.setMenuOrder(this.menuMapper.selectMenuByPid(menu.getMenuPid()).size()+1+CommonData.TOSTRING);
		}
		menu.setMenuChangedTime(ToolFactory.getInstanceOfDateTool().getTime());
		flag=this.menuMapper.updateByPrimaryKeySelective(menu);
		if(flag>=1) {
			return CommonData.UPDATE_SUCCESS;
		}
		else
			return CommonData.UPDATE_FAILURE;
	}

	/**
	 * @see com.scb.dev.sms.sm.service.IMenuService#deleteMenuById(int)
	 */
	@Override
	public String deleteMenuById(int menuId) {
		flag=this.menuMapper.deleteByPrimaryKey(menuId);
		if(flag>=1) {
			return CommonData.DELETE_SUCCESS;
		}
		else
			return CommonData.DELETE_FAILURE;
	}

	/**
	 * @see com.scb.dev.sms.sm.service.IMenuService#getMenuByName(int)
	 */
	@Override
	public MenuVO getMenuById(Integer menuId) {
		this.menuVo=this.menuMapper.selectById(menuId);
		if(null!=menuVo) 
			return menuVo;
		else
			return null;
	}


	/**
	 * @see com.scb.dev.sms.sm.service.IMenuService#getMenuByLevel(java.lang.String)
	 */
	@Override
	public List<MenuVO> getMenuByLevel(String menuLevel) {
		this.menuVoGroup=this.menuMapper.selectMenuByLevel(menuLevel);
		if(null!=menuVoGroup) 
			return menuVoGroup;
		else
			return null;
	}

}
