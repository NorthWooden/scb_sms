/**
 * Project Name:scb_sms
 * File Name:IAccountService.java
 * Package Name:com.scb.dev.sms.sm.service
 * Date:2018年11月22日上午10:06:50
 * Copyright (c) 2018, gael.zhu@qq.com All Rights Reserved.
 *
 */
package com.scb.dev.sms.sm.service;

import java.util.List;

import com.scb.dev.sms.sm.pojo.Account;
import com.scb.dev.sms.sm.vo.AccountVO;

/**
 * ClassName: IAccountService <br/>
 * Description: AccountService. <br/><br/>
 * date: 2018年11月22日 上午10:06:50 <br/>
 *
 * @author gael.zhu
 * @version V1.0
 * @since JDK 1.8
 */
public interface IAccountService {
	
	/**
	 * 
		 * @desciption 用户注册. <br/>     
		 * @param account Account:用户信息
		 * @return String:详见CommonData
	 */
	public String registerAccount(Account account);
	
	/**
	 * 
		 * @desciption 根据ID删除用户. <br/>     
		 * @param account String:TokenId
		 * @return String:详见CommonData
	 */
	public String deleteAccount(String accountId);
	
	/**
	 * 
		 * @desciption 更新用户. <br/>     
		 * @param account Account:用户信息
		 * @return String:详见CommonData
	 */
	public String updateAccount(Account account);
	
	/**
	 * 
		 * @desciption 根据Id删除用户. <br/>     
		 * @param accountId String:TokenID
		 * @return AccountVO:符合要求的account
	 */
	public AccountVO selectAccountById(String accountId);
	
	/**
	 * 
		 * @desciption 获取所有用户. <br/>     
		 * @return List<AccountVO>:所有用户
	 */
	public List<AccountVO> selectAllAccount();
	
	/**
	 * 
		 * @desciption 用户登录. <br/>     
		 * @param account Account:登录用户信息
		 * @return int :（1表示成功，0表示失败）
	 */
	int login(Account account);
}
