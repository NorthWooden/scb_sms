package com.scb.dev.sms.sm.vo;

import java.io.Serializable;

import org.springframework.stereotype.Component;

/**
 * 
 * ClassName: AccountVO <br/>
 * Description:  AccountVO <br/>
 * date: 2018年11月18日 上午6:48:07 <br/>
 *
 * @author Cristal.Xu
 * @version V1.0
 * @since JDK 1.8
 */
@Component
public class AccountVO implements Serializable{

	private static final long serialVersionUID = -4939117968767633010L;
	
    private String accountId;         //用户TokenId

    private String accountName;       //用户密码

    private String accountPwd;        //用户密码

    /** 
     * @return the value of sm_account.ACCOUNT_ID
     *
     * @mbg.generated Thu Nov 15 12:38:17 CST 2018
     */
    public String getAccountId() {
        return accountId;
    }

    /** 
     * @param accountId the value for sm_account.ACCOUNT_ID
     *
     * @mbg.generated Thu Nov 15 12:38:17 CST 2018
     */
    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    /** 
     * @return the value of sm_account.ACCOUNT_NAME
     *
     * @mbg.generated Thu Nov 15 12:38:17 CST 2018
     */
    public String getAccountName() {
        return accountName;
    }

    /**
     *  
     * @param accountName the value for sm_account.ACCOUNT_NAME
     *
     * @mbg.generated Thu Nov 15 12:38:17 CST 2018
     */
    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    /** 
     * @return the value of sm_account.ACCOUNT_PWD
     *
     * @mbg.generated Thu Nov 15 12:38:17 CST 2018
     */
    public String getAccountPwd() {
        return accountPwd;
    }

    /** 
     * @param accountPwd the value for sm_account.ACCOUNT_PWD
     *
     * @mbg.generated Thu Nov 15 12:38:17 CST 2018
     */
    public void setAccountPwd(String accountPwd) {
        this.accountPwd = accountPwd;
    }

}
