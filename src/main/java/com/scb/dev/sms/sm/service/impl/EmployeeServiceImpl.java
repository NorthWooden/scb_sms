/**
 * Project Name: scb.sms
 * File Name: EmployeeServiceImpl
 * Package Name: com.scb.dev.sms.service.sm.impl
 * Date: 2018/11/15 1:57 PM
 * Copyright (c) 2018, Wang, Haoyue All Rights Reserved.
 */
package com.scb.dev.sms.sm.service.impl;

import java.util.List;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.scb.dev.sms.common.CommonData;
import com.scb.dev.sms.sm.dao.EmployeeMapper;
import com.scb.dev.sms.sm.pojo.Employee;
import com.scb.dev.sms.sm.service.IEmployeeService;
import com.scb.dev.sms.util.pagination.Page;

/**
 * 
 * ClassName: EmployeeServiceImpl <br/>
 * Description:职员Service实现类
 * date: 2018年11月19日 下午3:47:00 <br/>
 *
 * @author Vanhom.Peng
 * @version V1.0
 * @since JDK 1.8
 */
@Service
@Transactional
public class EmployeeServiceImpl implements IEmployeeService {

	private Logger logger=Logger.getLogger(this.getClass());
	@Autowired
	private EmployeeMapper mapper;
	/**
	 * 
	 *  查询所有职员的信息
	 * @see com.scb.dev.sms.sm.service.IEmployeeService#queryAllEmployee()
	 */
	@Override
	public List<Employee> queryAllEmployee() {
		List<Employee> allEmployee=mapper.selectAllEmployeeInfo();
		if(allEmployee.size()>0) {
			  logger.info(CommonData.SUCCESS);
			  return allEmployee;
		}else {
			logger.info(CommonData.FAILURES);
			return null;
		}
	}

	/**
	 * 
	 *通过职员ID查询职员信息 .
	 * @see com.scb.dev.sms.sm.service.IEmployeeService#queryEmployeeById(java.lang.String)
	 */
	@Override
	public Employee queryEmployeeById(String employeeId) {
		Employee employee=mapper.selectByPrimaryKey(employeeId);
		if(employee!=null) {
			logger.info(CommonData.SUCCESS);
			return employee;
		}else {
			logger.info(CommonData.FAILURES);
			return null;
		}
	}

	/**
	 * 
	 * 添加职员信息
	 * @see com.scb.dev.sms.sm.service.IEmployeeService#addNewEmployee(com.scb.dev.sms.sm.pojo.Employee)
	 */
	@Override
	public String addNewEmployee(Employee employee) {
		if(employee!=null) {
			employee.setEmployeeId(UUID.randomUUID().toString().replace("-",""));
			if(mapper.insert(employee)>0) {
				return CommonData.SAVE_SUCCESS;
			}else
				return CommonData.SAVE_FAILURE;
		}else {
			
			return CommonData.SAVE_FAILURE;
		}
		
		
	}

	/**
	 * 
	 * 修改职员信息
	 * @return 
	 * @see com.scb.dev.sms.sm.service.IEmployeeService#modifyEmployeeInfo(com.scb.dev.sms.sm.pojo.Employee)
	 */
	@Override
	public String modifyEmployeeInfo(Employee employee) {
		Employee emp=employee;
		Employee e=mapper.selectByPrimaryKey(emp.getEmployeeId());
		if(e!=null) {
			if(mapper.updateByPrimaryKey(emp)>0) {
				return CommonData.UPDATE_SUCCESS;
			}else
				return CommonData.UPDATE_FAILURE;
		}else {
			return CommonData.QUERY_FAILURE;
		}
	}

	/**
	 * 
	 * 删除职员信息
	 * @see com.scb.dev.sms.sm.service.IEmployeeService#removeEmployee(com.scb.dev.sms.sm.pojo.Employee)
	 */
	@Override
	public String removeEmployee(Employee employee) {
		Employee emp=employee;
		if(mapper.selectByPrimaryKey(emp.getEmployeeId())!=null) {
			int result=mapper.deleteByPrimaryKey(emp.getEmployeeId());
			if(result>0) {
				return CommonData.DELETE_SUCCESS;
			}else
				return CommonData.DELETE_FAILURE;
		}else {
			return CommonData.QUERY_FAILURE;
		}
		
	}
	/**
	 * 分页查询职位信息
	 * @see com.scb.dev.sms.sm.service.IEmployeeService#findByPage(com.scb.dev.sms.util.pagination.Page)
	 */
	@Override
	public List<Employee> findByPage(Page page) {
		List<Employee> employees=mapper.selectByPage(page);
		if(employees.size()>0) {
			logger.info(CommonData.SAVE_SUCCESS);
			return employees;
		}else {
			logger.info(CommonData.SAVE_FAILURE);
			return null;
		}
	}
	/**
	 * 
	 * 通过部门ID查询职员信息 
	 * @see com.scb.dev.sms.sm.service.IEmployeeService#queryEmployeeByDepartmentId(java.lang.Integer)
	 */

	@Override
	public List<Employee> queryEmployeeByDepartmentId(Integer employeeDepartmentId) {
		List<Employee> employee=mapper.selectByDepartmentId(employeeDepartmentId);
		if(employee!=null) {
			logger.info(CommonData.SUCCESS);
			return employee;
		}else {
			logger.info(CommonData.FAILURES);
			return null;
		}
	}
	/**
	 * 
	 * 通过职位ID查询职员信息 
	 * @see com.scb.dev.sms.sm.service.IEmployeeService#queryEmployeeByPositionId(java.lang.Integer)
	 */
	@Override
	public List<Employee> queryEmployeeByPositionId(Integer employeePositionId) {
		List<Employee> employee=mapper.selectByPositionId(employeePositionId);
		if(employee!=null) {
			logger.info(CommonData.SUCCESS);
			return employee;
		}else {
			logger.info(CommonData.FAILURES);
			return null;
		}
	}

   
}
