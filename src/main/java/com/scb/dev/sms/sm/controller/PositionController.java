/**
 * Project Name:scb_sms
 * File Name:PositionController.java
 * Package Name:com.scb.dev.sms.sm.controller
 * Date:2018年11月17日下午6:31:14
 * Copyright (c) 2018,Grace.Lee@clps.com All Rights Reserved.
 *
 */
package com.scb.dev.sms.sm.controller;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import com.scb.dev.sms.sm.pojo.Employee;
import com.scb.dev.sms.sm.pojo.Menu;
import com.scb.dev.sms.sm.pojo.Position;
import com.scb.dev.sms.sm.service.IEmployeeService;
import com.scb.dev.sms.sm.service.IPositionService;

/**
 * ClassName: PositionController <br/>
 * Description: 
 * date: 2018年11月17日 下午6:31:14 <br/>
 *
 * @author Grace.Lee
 * @version V1.0
 * @since JDK 1.8
 */

@Controller
@RequestMapping(value="/sm")
public class PositionController {

	
	
	@Resource
	private IPositionService positionService;
	@Resource
	private IEmployeeService employeeService;
	private Position position;
	private List<Position> positionlist;
	Logger logger=Logger.getLogger(PositionController.class);
	
	/**
	 * 
	 * showPositionList 显示所有职位信息
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/positions",method=RequestMethod.GET)
	public String showPositionList(Model model ,HttpServletRequest request){
		positionlist=this.positionService.queryAllPosititon();
		model.addAttribute("position", positionlist);
		request.setAttribute("log", positionlist);
		return "position_list";
		
	}
	/**
	 * 
	 * select  根据职位ID进行查询，跳转到positionlist界面
	 * @param model
	 * @param positionId
	 * @return
	 */
	@RequestMapping(value="/position",method=RequestMethod.POST)
	public String showPosition(Integer positionId,Model model,HttpServletRequest request) {
		  position=this.positionService.queryPositionById(positionId);
		  List<Position> poslist=new ArrayList<Position>();
		  poslist.add(position);
		  model.addAttribute("position", poslist);
		  request.setAttribute("log", poslist);
		  return "position_list";
		
		
		 
	}
	
	@RequestMapping(value="/position/judge/{positionId}",method=RequestMethod.POST)
	@ResponseBody
	public Map<String,Object> judgePositionId(@PathVariable("positionId") Integer positionId) {
		Map<String,Object> resultMap = new HashMap<String, Object>();
		if(positionService.queryPositionById(positionId)!=null) {
			resultMap.put("result", "exist");
		}else
			resultMap.put("result", "notexist");
		return resultMap;
	}
	
	/**
	 * 
	 * showUpdatePosition:修改之前,跳转到修改页面
	 * @param positionid
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/position/{positionId}", method=RequestMethod.GET)
	public String showUpdatePosition(@PathVariable("positionId") Integer positionid,HttpServletRequest request) {
		Position position=positionService.queryPositionById(positionid);
		request.setAttribute("position",position);
		return "position_modify";
	}
	/**
	 * 
	 * updatePositionInfo 修改职位信息，跳转到positionlist界面
	 * @param model
	 * @param position
	 * @return
	 */
	@RequestMapping(value="/position/edit",method=RequestMethod.POST)
	public String updatePositionInfo(Position position, HttpServletRequest request,HttpSession session) {
		String positionChangerId=(String) session.getAttribute("operator");
		position.setPositionChangerId(positionChangerId);
		String commond=positionService.modifyPositionInfo(position);
		request.setAttribute("log", commond);
		return "redirect: ../positions";
		
	}

	@RequestMapping(value="/position/delete",method=RequestMethod.GET)
	public String dePosition(Model model ,HttpServletRequest request) {
		positionlist=this.positionService.queryAllPosititon();
		model.addAttribute("position", positionlist);
		request.setAttribute("log", positionlist);
		return "position_delete";
	}
	/**
	 * 
	 * deletePosition 根据职位Id删除职位信息
	 * @param model
	 * @param positionId
	 * @return
	 */
	@RequestMapping(value="/position/delete/{positionId}",method=RequestMethod.GET)
	public String deletePosition(@PathVariable("positionId") Integer positionId,HttpServletRequest request) {
			this.position=positionService.queryPositionById(positionId);
		  String commond=positionService.removePosition(this.position);
		  request.setAttribute("log", commond);
		  return "redirect: ../../position/delete";
		  
	  }
		
	/**
	 * 	
	 * queryMenuByPosition  通过职位查找菜单
	 * @param model
	 * @param positionName
	 * @return
	 */
	@RequestMapping(value="/position/menu/{positionId}",method=RequestMethod.GET)
	public String showMenuByPosition(@PathVariable("positionId") Integer positionId,Model model,HttpServletRequest request) {
		
		this.position=positionService.queryPositionById(positionId);
		List<Menu> list=positionService.qeuryMenuByPosition(this.position.getPositionId());
		model.addAttribute("menus",list);
		request.setAttribute("log", list);
		return "menu_list";
	}
	
	/**
	 * 
	 * addPosition 新增Position
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/position/add",method=RequestMethod.GET)
	public String createPosition() {
		return "position_add";
	}
	
	@RequestMapping(value="/position/add",method=RequestMethod.POST)
	public String addPosition(Position position,HttpServletRequest request,HttpSession session) {
			String positionCreatorId=(String) session.getAttribute("operator");
			position.setPositionCreatorId(positionCreatorId);
			String commond=positionService.insertPosition(position);
			request.setAttribute("log", commond);
			return "redirect: ../positions";
	}
	
	@RequestMapping(value="/position/employee/{positionId}",method=RequestMethod.GET)
	public String showEmployeeByPosition(@PathVariable("positionId") Integer positionId,Model model,HttpServletRequest request) {
		
		List<Employee> employees=employeeService.queryEmployeeByPositionId(positionId);
		model.addAttribute("emps", employees);
		request.setAttribute("log", employees);
		return "position_employee";
	}
}
