/**
 * Project Name:scb_sms
 * File Name:MenuController.java
 * Package Name:com.scb.dev.sms.sm.controller
 * Date:2018年11月8日上午11:51:48
 * Copyright (c) 2018, gael.zhu@qq.com All Rights Reserved.
 *
 */
package com.scb.dev.sms.sm.controller;

import java.io.IOException;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.scb.dev.sms.common.CommonData;
import com.scb.dev.sms.sm.pojo.Menu;
import com.scb.dev.sms.sm.service.IMenuService;
import com.scb.dev.sms.sm.vo.MenuVO;

/**
 * ClassName: MenuController <br/>
 * Description: MenuController. <br/>
 * <br/>
 * date: 2018年11月8日 上午11:51:48 <br/>
 *
 * @author gael.zhu
 * @version V1.00.*
 * @since JDK 1.8
 */
@Controller
@RequestMapping(value = "/sm")
public class MenuController {

	
	@Resource
	private IMenuService menuService;
	
	/**
	 * 
	 * @desciption 显示菜单信息. <br/>
	 * @param model Model
	 * @return "main" String:主菜单
	 */
	@RequestMapping(value = "/main",method=RequestMethod.GET)
	public String showMain(Model model) throws IOException {
		return "main";
	}
	
	/**
	 * 
		 * @desciption 展示菜单信息. <br/>     
		 * @param model Model
		 * @return menu_list:菜单信息界面
	 */
	@RequestMapping(value = "/show",method=RequestMethod.GET)
	public String showMenu(Model model) throws Exception {
		List<MenuVO> menus = menuService.getAllMenu();
		model.addAttribute("menus", menus);
		return "menu_list";
	}
	
	/**
	 * 
	 * @desciption 显示菜单信息. <br/>
	 * @param model Model
	 * @return "menu" String:菜单界面
	 */
	@RequestMapping(value = "/display",method=RequestMethod.GET)
	public String getJson(Model model) throws IOException {
		List<MenuVO> menus = menuService.getAllMenu();
		model.addAttribute("menuJson", menus);
		return "menu";
	}
	
	/**
	 * 
	 * @desciption 添加菜单 <br/>
	 * @param record Menu
	 * @param model  Model
	 * @return String:菜单添加界面以及结果
	 */
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public String addMenu(Menu record, Model model, HttpServletRequest request,HttpSession session) throws Exception {
		record.setMenuPid(Integer.parseInt(request.getParameter("brand")));
		session=request.getSession();
		record.setMenuChangerId(session.getAttribute("operator")+CommonData.TOSTRING);
		record.setMenuCreatorId(session.getAttribute("operator")+CommonData.TOSTRING);
		this.menuService.insertMenu(record);
		List<MenuVO> menus = menuService.getAllMenu();
		model.addAttribute("menus", menus);
		return "menu_list";

	}

	@RequestMapping(value = "/toAdd", method = RequestMethod.GET)
	public String toAddMenu(Model model) throws Exception {
		List<MenuVO> menus = menuService.getAllMenu();
		model.addAttribute("menus", menus);
		return "menu_add";
	}

	/**
	 * 
	 * @desciption 删除菜单信息. <br/>
	 * @param menuId int
	 * @return menu_delresult
	 */
	@RequestMapping(value = "/del", method = RequestMethod.GET)
	public String delMenu(int menuId,Model model) throws Exception {
		menuService.deleteMenuById(menuId);
		List<MenuVO> menus = menuService.getAllMenu();
		model.addAttribute("menus", menus);
		return "menu_list";
	}
	
	/**
	 * 
	 * @history:
	 * @param
	 * @return 通过菜单id获取该菜单的信息，并跳转到修改界面
	 */
	@RequestMapping(value = "/toUpdate",method=RequestMethod.GET)
	public String getMenu(Integer menuId, Model model, HttpSession session) {
		session.setAttribute("menuId", menuId);
		model.addAttribute("menu", menuService.getMenuById(menuId));
		List<MenuVO> menus = menuService.getAllMenu();
		model.addAttribute("menus", menus);
		return "menu_update";
	}

	
	/**
	 * 
	 * @history:
	 * @param
	 * @return 修改菜单信息
	 */
	@RequestMapping(value = "/update",method=RequestMethod.POST)
	public String updateMenu(Menu record, Model model, HttpSession session, HttpServletRequest request) throws Exception{
		record.setMenuId((int)session.getAttribute("menuId"));
		record.setMenuPid(Integer.parseInt(request.getParameter("brand")));
		record.setMenuChangerId(session.getAttribute("operator")+CommonData.TOSTRING);
		this.menuService.updateMenu(record);
		List<MenuVO> menus = menuService.getAllMenu();
		model.addAttribute("menus", menus);
		return "menu_list";
	}
	
	/**
	 * 
	 * @desciption 根据级别查询菜单. <br/>
	 * @param model Model
	 * @return "menu" String:菜单界面
	 */
	@RequestMapping(value = "/queryByLevel",method=RequestMethod.GET)
	public String queryMenuByLevel(Model model,String menuLevel) throws IOException {
		List<MenuVO> menus = menuService.getMenuByLevel(menuLevel);
		model.addAttribute("menus", menus);
		return "menu_list";
	}
}


