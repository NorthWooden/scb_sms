/**
 * Project Name:scb_sms
 * File Name:LogonController.java
 * Package Name:com.scb.dev.sms.sm.controller
 * Date:2018年11月22日下午12:05:57
 * Copyright (c) 2018, gael.zhu@qq.com All Rights Reserved.
 *
 */
package com.scb.dev.sms.sm.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.scb.dev.sms.sm.pojo.Account;
import com.scb.dev.sms.sm.service.IAccountService;

/**
 * ClassName: LogonController <br/>
 * Description: Logon Controller. <br/><br/>
 * date: 2018年11月22日 下午12:05:57 <br/>
 *
 * @author gael.zhu
 * @version V1.0
 * @since JDK 1.8
 */
@Controller
@RequestMapping("/sm")
public class LogonController {
	
	@Autowired
	private IAccountService accountService;
	@Autowired
	private MenuController menuController;
	
	/**
	 * 
		 * @desciption 用户登录. <br/>     
		 * @param model Model
		 * @return account_list:用户信息界面
	 */
	@RequestMapping(value = "/doLogon",method=RequestMethod.POST)
	public String logon(Model model,Account account,HttpSession session) throws Exception {
		if(this.accountService.login(account)==1) {
			session.setAttribute("operator", account.getAccountName());
			return this.menuController.showMain(model);
		}
		return "login";
	}
	
	@RequestMapping(value = "/showLogon",method=RequestMethod.GET)
	public String showLogon(Model model) throws Exception {
		return "login";
	}
}
