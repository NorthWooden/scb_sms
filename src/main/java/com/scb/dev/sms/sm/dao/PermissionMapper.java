package com.scb.dev.sms.sm.dao;

import java.util.List;

import com.scb.dev.sms.sm.pojo.Permission;

public interface PermissionMapper {
    /**
     * @mbg.generated Wed Nov 14 21:18:59 CST 2018
     */
    int deleteByPrimaryKey(Integer permissionId);

    /**
     * @mbg.generated Wed Nov 14 21:18:59 CST 2018
     */
    int insert(Permission record);

    /**
     * @mbg.generated Wed Nov 14 21:18:59 CST 2018
     */
    int insertSelective(Permission record);
    
    /**
     * @mbg.generated Wed Nov 14 21:18:59 CST 2018
     */
    List<Permission> selsctAllPermission();
    
    /**
     * @mbg.generated Wed Nov 14 21:18:59 CST 2018
     */
    Permission selectByPrimaryKey(Integer permissionId);

    /**
     * @mbg.generated Wed Nov 14 21:18:59 CST 2018
     */
    int updateByPrimaryKeySelective(Permission record);

    /**
     * @mbg.generated Wed Nov 14 21:18:59 CST 2018
     */
    int updateByPrimaryKey(Permission record);
}