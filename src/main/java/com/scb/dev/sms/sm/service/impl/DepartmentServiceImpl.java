/**
 * Project Name:scb_sms
 * File Name:DepartmentServiceImpl.java
 * Package Name:com.scb.dev.sms.sm.service.impl
 * Date:2018年11月15日上午10:48:24
 * Copyright (c) 2018, clpsglobal All Rights Reserved.
 *
 */
package com.scb.dev.sms.sm.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.scb.dev.sms.common.CommonData;
import com.scb.dev.sms.sm.dao.DepartmentMapper;
import com.scb.dev.sms.sm.dao.EmployeeMapper;
import com.scb.dev.sms.sm.pojo.Department;
import com.scb.dev.sms.sm.pojo.Employee;
import com.scb.dev.sms.sm.service.IDepartmentService;

/**
 * ClassName: DepartmentServiceImpl
 * Description: Department service implements
 * date: 2018年11月15日 上午10:48:24 
 *
 * @author zack.guo
 * @version V1.0
 * @since JDK 1.8
 */

@Service
@Transactional(transactionManager = "transactionManager",propagation = Propagation.REQUIRES_NEW)
public class DepartmentServiceImpl implements IDepartmentService {
	
	public Department department=null;
	public List<Department> departmentList = null;
	@Autowired
	public DepartmentMapper departmentMapper;
	@Autowired
	public EmployeeMapper employeeMapper;
	public Logger logger = Logger.getLogger(this.getClass());
	//public LinkedList<Department> 
	/**
	 * 
	 * @description:迭代建树方法
	 * @param Integer departmentPid
	 * @return List<Department>
	 */
//	public List<Department> findChildDepartment(Integer departmentPid){
//		List<Department>childList=departmentMapper.selectByPid(departmentPid);
//		for (Department childDepartment : childList) {
//			childDepartment.setLeaderEmployee(employeeMapper.selectByPrimaryKey(childDepartment.getDepartmentLeader()));
//			List<Department> neoChildList=findChildDepartment(childDepartment.getDepartmentId());
//			childDepartment.setChildDepartment(neoChildList);
//		}
//		return childList;
//	}
	/**
	 * 
	 * @see com.scb.dev.sms.sm.service.IDepartmentService#findAllDepartment()
	 */
	@Override
	public List<Department> findAllDepartment() {
		departmentList=departmentMapper.selectAll();
//		for (Department department : departmentList) {
//			department.setLeaderEmployee(employeeMapper.selectByPrimaryKey(department.getDepartmentLeader()));
//		}
		if(departmentList.size()>0) {
			logger.info(CommonData.QUERY_SUCCESS);
			return departmentList;
		}else {
			logger.info(CommonData.QUERY_FAILURE);
			return null;
		}
	}
	/**
	 * 
	 * @see com.scb.dev.sms.sm.service.IDepartmentService#addNewDepartment()
	 */
	@Override
	public String addNewDepartment(Department department) {
		if(department==null) {
			return CommonData.SAVE_FAILURE;
		}else {
			int i=0,j=0;
			Department departmentInfo=new Department();
			departmentInfo.setDepartmentLeader(department.getDepartmentLeader());
			departmentList=departmentMapper.selectByInfo(departmentInfo);
			for (Department department2 : departmentList) {
				department2.setDepartmentLeader("");
				departmentMapper.updateByPrimaryKeySelective(department2);
			}
			i=departmentMapper.insertSelective(department);
			Employee employee=employeeMapper.selectByPrimaryKey(department.getDepartmentLeader());
			employee.setEmployeeDepartmentId(department.getDepartmentId());
			j=employeeMapper.updateByPrimaryKey(employee);
//			try {
//				
//			} catch (Exception e) {
//				TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
//			}
			if(i==1&&j==1) {
				return CommonData.SAVE_SUCCESS;
			}else {
				return CommonData.SAVE_FAILURE;
			}
		}
	}
	/**
	 * 
	 * @see com.scb.dev.sms.sm.service.IDepartmentService#deleteDepartment(java.lang.Integer)
	 */
	
	@Override
	public String deleteDepartment(Integer departmentId,boolean single) {
		department=departmentMapper.selectByPrimaryKey(departmentId);
		Integer departmentPid=department.getDepartmentPid();
		department=new Department();
		department.setDepartmentPid(departmentId);
		departmentList=departmentMapper.selectByInfo(department);
		if(single==true) {
			for (Department department : departmentList) {
				department.setDepartmentPid(departmentPid);
				int mark=departmentMapper.updateByPrimaryKeySelective(department);
				if(mark==0) {
					return CommonData.DELETE_FAILURE;
				}
			}
		}else {
			for (Department department : departmentList) {
				String s=deleteDepartment(department.getDepartmentId(), false);
				if(s.equals(CommonData.DELETE_FAILURE)) {
					return CommonData.DELETE_FAILURE;
				}
			}
		}
		int mark = departmentMapper.deleteByPrimaryKey(departmentId);
		if(mark==1) {
			return CommonData.DELETE_SUCCESS;
		}else {
			return CommonData.DELETE_FAILURE;
		}
	}
	/**
	 * 
	 * @see com.scb.dev.sms.sm.service.IDepartmentService#findOneDepartmentByName(java.lang.String)
	 */
	@Override
	public Department findOneDepartmentByName(String departmentName) {
		department=new Department();
		department.setDepartmentName(departmentName);
		departmentList=departmentMapper.selectByInfo(department);
		if(departmentList.size()==1) {
			logger.info(CommonData.QUERY_SUCCESS);
			department=departmentList.get(0);
			return department;
		}else {
			logger.info(CommonData.QUERY_FAILURE);
			return null;
		}
		
	}
	/**
	 * 
	 * @see com.scb.dev.sms.sm.service.IDepartmentService#updateDepartment(com.scb.dev.sms.sm.pojo.Department)
	 */
	@Override
	public String updateDepartment(Department department) {
		if(department==null) {
			return CommonData.UPDATE_FAILURE;
		}else {
			int mark=departmentMapper.updateByPrimaryKeySelective(department);
			if(mark==1) {
				return CommonData.UPDATE_SUCCESS;
			}else {
				return CommonData.UPDATE_FAILURE;
			}
		}
		
	}
	/**
	 * 
	 * @see com.scb.dev.sms.sm.service.IDepartmentService#findDepartmentByPartName(java.lang.String)
	 */
	@Override
	public List<Department> findDepartmentByPartName(String departmentName) {
		departmentList=departmentMapper.selectByPartName(departmentName);
		if(departmentList!=null) {
			logger.info(CommonData.QUERY_SUCCESS);
			return departmentList;
		}else {
			logger.info(CommonData.QUERY_FAILURE);
			return null;
		}
		
	}
	/**
	 * 
	 * @see com.scb.dev.sms.sm.service.IDepartmentService#findDepartmentById(java.lang.Integer)
	 */
	@Override
	public Department findDepartmentById(Integer departmentId) {
		if(departmentId==null) {
			logger.info(CommonData.QUERY_FAILURE);
			return null;
		}else {
			department=departmentMapper.selectByPrimaryKey(departmentId);
			if(department!=null) {
				logger.info(CommonData.QUERY_SUCCESS);
				return department;
			}else {
				logger.info(CommonData.QUERY_FAILURE);
				return null;
			}
		}
	}
	/**
	 * 
	 * @see com.scb.dev.sms.sm.service.IDepartmentService#findAllDepartmentByTree()
	 */
	@Override
	public List<Department> findAllDepartmentByTree() {
		System.out.println(new Date());
		departmentList=departmentMapper.selectAll();
//		List<Department>treeList=new ArrayList<>();
		List<Department>sortList=new ArrayList<>();
//		int size = 0;
		System.out.println(new Date());
		if(departmentList.size()>0) {
			for (Department department : departmentList) {
				if(department.getDepartmentPid()==-1) {
					sortList.add(department);
				}
			}
//			size=sortList.size();
//			int sortSize=size;
//			int i=0;
//			while(i<sortSize) {
//				department=sortList.get(i);
//				for (Department childDepartment : departmentList) {
//					if(childDepartment.getDepartmentPid()==department.getDepartmentId()) {
//						if(department.getChildDepartment()==null) {
//							department.setChildDepartment(new ArrayList<>());
//						}
//						department.getChildDepartment().add(childDepartment);
//						sortList.add(childDepartment);
//						sortSize++;
//					}
//				}
//				department.setLeaderEmployee(employeeMapper.selectByPrimaryKey(department.getDepartmentLeader()));
//				i++;
//			}
//			for (i = 0; i < size; i++) {
//				treeList.add(sortList.get(i));
//			}
			logger.info(CommonData.QUERY_SUCCESS);
//			return treeList;
			return sortList;
		}else {
			logger.info(CommonData.QUERY_FAILURE);
			return null;
		}
	}
	
	
}
