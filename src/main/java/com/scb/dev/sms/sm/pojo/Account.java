/**
 * Project Name:scb_sms
 * File Name:Menu.java
 * Package Name:com.scb.dev.sms.sm.pojo
 * Date:2018年11月8日上午11:51:48
 * Copyright (c) 2018, gael.zhu@qq.com All Rights Reserved.
 *
 */
package com.scb.dev.sms.sm.pojo;

import java.io.Serializable;
import java.util.Date;

import org.springframework.stereotype.Component;
/**
 * ClassName: Account <br/>
 * Description: Account. <br/>
 * <br/>
 * date: 2018年11月8日 上午11:51:48 <br/>
 *
 * @author gael.zhu
 * @version V1.0
 * @since JDK 1.8
 */
@Component
public class Account implements Serializable{

	private static final long serialVersionUID = 7807896260860594128L;

	private String accountId;                //用户TokenId

    private String accountName;              //用户名

    private String accountPwd;               //用户密码

    private String accountChangerId;         //用户信息修改人（ID）

    private Date accountChangedTime;         //用户信息修改时间

    private Integer accountInvisible;        //用户是否激活

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column sm_account.ACCOUNT_ID
     *
     * @return the value of sm_account.ACCOUNT_ID
     *
     * @mbg.generated Thu Nov 22 09:17:17 CST 2018
     */
    public String getAccountId() {
        return accountId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column sm_account.ACCOUNT_ID
     *
     * @param accountId the value for sm_account.ACCOUNT_ID
     *
     * @mbg.generated Thu Nov 22 09:17:17 CST 2018
     */
    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column sm_account.ACCOUNT_NAME
     *
     * @return the value of sm_account.ACCOUNT_NAME
     *
     * @mbg.generated Thu Nov 22 09:17:17 CST 2018
     */
    public String getAccountName() {
        return accountName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column sm_account.ACCOUNT_NAME
     *
     * @param accountName the value for sm_account.ACCOUNT_NAME
     *
     * @mbg.generated Thu Nov 22 09:17:17 CST 2018
     */
    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column sm_account.ACCOUNT_PWD
     *
     * @return the value of sm_account.ACCOUNT_PWD
     *
     * @mbg.generated Thu Nov 22 09:17:17 CST 2018
     */
    public String getAccountPwd() {
        return accountPwd;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column sm_account.ACCOUNT_PWD
     *
     * @param accountPwd the value for sm_account.ACCOUNT_PWD
     *
     * @mbg.generated Thu Nov 22 09:17:17 CST 2018
     */
    public void setAccountPwd(String accountPwd) {
        this.accountPwd = accountPwd;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column sm_account.ACCOUNT_CHANGER_ID
     *
     * @return the value of sm_account.ACCOUNT_CHANGER_ID
     *
     * @mbg.generated Thu Nov 22 09:17:17 CST 2018
     */
    public String getAccountChangerId() {
        return accountChangerId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column sm_account.ACCOUNT_CHANGER_ID
     *
     * @param accountChangerId the value for sm_account.ACCOUNT_CHANGER_ID
     *
     * @mbg.generated Thu Nov 22 09:17:17 CST 2018
     */
    public void setAccountChangerId(String accountChangerId) {
        this.accountChangerId = accountChangerId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column sm_account.ACCOUNT_CHANGED_TIME
     *
     * @return the value of sm_account.ACCOUNT_CHANGED_TIME
     *
     * @mbg.generated Thu Nov 22 09:17:17 CST 2018
     */
    public Date getAccountChangedTime() {
        return accountChangedTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column sm_account.ACCOUNT_CHANGED_TIME
     *
     * @param accountChangedTime the value for sm_account.ACCOUNT_CHANGED_TIME
     *
     * @mbg.generated Thu Nov 22 09:17:17 CST 2018
     */
    public void setAccountChangedTime(Date accountChangedTime) {
        this.accountChangedTime = accountChangedTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column sm_account.ACCOUNT_INVISIBLE
     *
     * @return the value of sm_account.ACCOUNT_INVISIBLE
     *
     * @mbg.generated Thu Nov 22 09:17:17 CST 2018
     */
    public Integer getAccountInvisible() {
        return accountInvisible;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column sm_account.ACCOUNT_INVISIBLE
     *
     * @param accountInvisible the value for sm_account.ACCOUNT_INVISIBLE
     *
     * @mbg.generated Thu Nov 22 09:17:17 CST 2018
     */
    public void setAccountInvisible(Integer accountInvisible) {
        this.accountInvisible = accountInvisible;
    }
}