/**
 * Project Name:scb_sms
 * File Name:AccountController.java
 * Package Name:com.scb.dev.sms.sm.controller
 * Date:2018年11月22日上午11:10:50
 * Copyright (c) 2018, gael.zhu@qq.com All Rights Reserved.
 *
 */
package com.scb.dev.sms.sm.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.scb.dev.sms.common.CommonData;
import com.scb.dev.sms.sm.pojo.Account;
import com.scb.dev.sms.sm.service.IAccountService;
import com.scb.dev.sms.sm.vo.AccountVO;

/**
 * ClassName: AccountController <br/>
 * Description: Account Controller. <br/><br/>
 * date: 2018年11月22日 上午11:10:50 <br/>
 *
 * @author gael.zhu
 * @version V1.0
 * @since JDK 1.8
 */
@Controller
@RequestMapping(value = "/account")
public class AccountController {

	@Resource
	private IAccountService accountService;

	/**
	 * 
		 * @desciption 展示用户信息. <br/>     
		 * @param model Model
		 * @return account_list:用户信息界面
	 */
	@RequestMapping(value = "/show",method=RequestMethod.GET)
	public String showMenu(Model model) throws Exception {
		List<AccountVO> accounts = accountService.selectAllAccount();
		model.addAttribute("accounts", accounts);
		return "account_list";
	}
	
	/**
	 * 
	 * @desciption 注册用户 <br/>
	 * @param record Account
	 * @param model  Model
	 * @return String:用户添加界面以及结果
	 */
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public String addMenu(Account record, Model model,HttpSession session) throws Exception {
		record.setAccountChangerId(session.getAttribute("operator")+CommonData.TOSTRING);
		this.accountService.registerAccount(record);
		List<AccountVO> accounts = this.accountService.selectAllAccount();
		model.addAttribute("accounts", accounts);
		return "account_list";

	}

	@RequestMapping(value = "/toAdd", method = RequestMethod.GET)
	public String toAddMenu(Model model) throws Exception {
		return "account_add";
	}

	/**
	 * 
	 * @desciption 删除用户信息. <br/>
	 * @param account String
	 * @return menu_list:用户信息界面
	 */
	@RequestMapping(value = "/del", method = RequestMethod.GET)
	public String delMenu(String accountId,Model model) throws Exception {
		this.accountService.deleteAccount(accountId);
		List<AccountVO> accounts = this.accountService.selectAllAccount();
		model.addAttribute("accounts", accounts);
		return "account_list";
	}
	
	/**
	 * 
	 * @author: victor.gu
	 * @createTime: 2018年5月14日 下午1:47:56
	 * @history:
	 * @param
	 * @return 通过菜单id获取该菜单的信息，并跳转到修改界面
	 */
	@RequestMapping(value = "/toUpdate",method=RequestMethod.GET)
	public String getMenu(String accountId, Model model, HttpSession session) {
		session.setAttribute("accountId", accountId);
		model.addAttribute("account", this.accountService.selectAccountById(accountId));
		return "account_update";
	}

	
	/**
	 * 
	 * @author: victor.gu
	 * @createTime: 2018年5月14日 下午1:47:56
	 * @history:
	 * @param
	 * @return 修改菜单信息
	 */
	@RequestMapping(value = "/update",method=RequestMethod.POST)
	public String updateMenu(Account record, Model model, HttpSession session) throws Exception{
		record.setAccountId(session.getAttribute("accountId")+CommonData.TOSTRING);
		record.setAccountChangerId(session.getAttribute("operator")+CommonData.TOSTRING);
		this.accountService.updateAccount(record);
		List<AccountVO> accounts = this.accountService.selectAllAccount();
		model.addAttribute("accounts", accounts);
		return "account_list";
	}

	/**
	 * 
	 * @desciption 根据ID查询Account. <br/>
	 * @param model Model
	 * @return account_list String:用户界面
	 */
	@RequestMapping(value = "/selectById",method=RequestMethod.GET)
	public String queryAccountById(Model model,String accountId) throws IOException {
		List<AccountVO> accounts = new ArrayList<AccountVO>();
		accounts.add(this.accountService.selectAccountById(accountId));
		model.addAttribute("accounts", accounts);
		return "account_list";
	}
}
