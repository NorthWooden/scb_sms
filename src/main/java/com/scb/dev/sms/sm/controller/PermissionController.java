/**
 * Project Name:scb_sms
 * File Name:PermissionController.java
 * Package Name:com.scb.dev.sms.sm.controller
 * Date:2018年11月18日下午7:28:24
 * Copyright (c) 2018, Annie.Jiang@clps.com All Rights Reserved.
 *
 */
package com.scb.dev.sms.sm.controller;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.scb.dev.sms.sm.pojo.Permission;
import com.scb.dev.sms.sm.service.IPermissionService;

/**
 * ClassName: PermissionController <br/>
 * Description: 
 * date: 2018年11月18日 下午7:28:24 <br/>
 *
 * @author Annie.Jiang
 * @version V1.0
 * @since JDK 1.8
 */
@Controller
@RequestMapping(value="/sm")
public class PermissionController {

	@Resource
	private IPermissionService permissionService;
	
	/**
	 * showAllPermissions:显示所有权限信息. <br/>
	 * @return permission_list String 权限列表
	 * @throws Exception
	 */
	@RequestMapping(value="/showPermission",method=RequestMethod.GET)
	public String showAllPermissions(Model model) throws Exception{
		List<Permission> pers=permissionService.showAllPermissions();
		model.addAttribute("pers", pers);
		return "permission_list";
	}
	
	/** 
	 * selectPermission:查询权限详细信息. <br/>
	 * @param model
	 * @param perId
	 * @return permission_info Stirng 权限信息
	 * @throws Exception
	 */
	@RequestMapping(value="/selectPermission",method=RequestMethod.GET)
	public String selectPermission(Model model,Integer perId) throws Exception {
		Permission per=permissionService.showPermission(perId);
		model.addAttribute("per", per);
		return "permission_info";
	}
	
	/**
	 * 
	 * addPermission:增加权限. <br/>
	 * @param per
	 * @return permission_add  
	 * @throws Exception
	 */
	@RequestMapping(value="/addPermission",method=RequestMethod.POST)
	public String addPermission(Permission per) throws Exception {
		permissionService.addPermission(per);
		return "permission_add";
	}
	
	/**
	 * 
	 * deletePermission:根据权限Id号删除权限. <br/>
	 * @param perId
	 * @return permission_delete String
	 * @throws Exception
	 */
	@RequestMapping(value="/deletePermission",method=RequestMethod.POST)
	public String deletePermission(Integer perId) throws Exception {
		permissionService.deletePermission(perId);
		return "permission_delete";
	}
	
	
	
	
	/**
	 * updatePermission:更新权限信息. <br/>
	 * @param per
	 * @return permission_update String
	 * @throws Exception
	 */
	@RequestMapping(value="/updatePermission",method=RequestMethod.POST)
	public String updatePermission(Permission per) throws Exception {
		permissionService.changePermission(per);
		return "permission_update";
	}
}
