/**
 * Project Name:scb_sms
 * File Name:IEmployeeService.java
 * Package Name:com.scb.dev.sms.sm.service.impl
 * Date:2018年11月19日下午3:24:00
 * Copyright (c) 2018, Vanhom.Peng@clpsglobal.com All Rights Reserved.
 *
 */
package com.scb.dev.sms.sm.service;

import java.util.List;
import com.scb.dev.sms.sm.pojo.Employee;
import com.scb.dev.sms.util.pagination.Page;
/**
 * ClassName: IEmployeeService <br/>
 * date: 2018年11月19日 下午3:24:00 <br/>
 * @author Vanhom.Peng
 * @version V1.0
 * @since JDK 1.8
 */
public interface IEmployeeService {
	/** 
	 * queryAllEmployee:(查询所有的职员信息). <br/>
	 * @author Vanhom.Peng
	 * @return
	 * @since JDK 1.8
	 */
	public List<Employee> queryAllEmployee();
	/**
	 * queryEmployeeById:(根据职员ID查询指定职员信息). <br/>
	 * @author Vanhom.Peng
	 * @param employeeId
	 * @return
	 * @since JDK 1.8
	 */
	public Employee queryEmployeeById(String employeeId);
	/**
	 * 
	 * queryEmployeeByDepartmentId:(根据部门ID查询职员信息). <br/>
	 * @author Vanhom_Peng
	 * @param employeeDepartmentId
	 * @return
	 * @since JDK 1.8
	 */
	public List<Employee> queryEmployeeByDepartmentId(Integer employeeDepartmentId);
	/**
	 * 
	 * queryEmployeeByPositionId:(根据职位ID查询职员信息). <br/>
	 * @author Vanhom_Peng
	 * @param employeePositionId
	 * @return
	 * @since JDK 1.8
	 */
	public List<Employee> queryEmployeeByPositionId(Integer employeePositionId);
	/**
	 * 
	 * addNewEmployee:(添加职员信息). <br/>
	 * @author Vanhom.Peng
	 * @param employee
	 * @return
	 * @since JDK 1.8
	 */
	public String addNewEmployee(Employee employee);
	/**
	 * modifyEmployeeInfo:(修改职员信息). <br/>
	 * @author Vanhom.Peng
	 * @param employee
	 * @return 
	 * @since JDK 1.8
	 */
	public  String modifyEmployeeInfo(Employee employee) ;
	/**
	 * removeEmployee:(删除职员信息). <br/>
	 * @author Vanhom.Peng
	 * @param emp
	 * @since JDK 1.8
	 */
	public  String removeEmployee(Employee employee);
	/**
	 * 
	 * findByPage 通过页面查找职位信息
	 * @param page
	 * @return
	 */
	 public List<Employee> findByPage(Page page);
	

}
