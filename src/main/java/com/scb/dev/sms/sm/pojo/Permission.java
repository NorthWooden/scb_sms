package com.scb.dev.sms.sm.pojo;

import java.io.Serializable;

import org.springframework.stereotype.Component;

@Component
public class Permission implements Serializable {
    /**
     * @mbg.generated Wed Nov 14 21:18:58 CST 2018
     */
    private Integer permissionId;

    /**
     * @mbg.generated Wed Nov 14 21:18:58 CST 2018
     */
    private String permissionName;

    /**
     * @mbg.generated Wed Nov 14 21:18:58 CST 2018
     */
    private String permissionPositionId;

    /**
     * @mbg.generated Wed Nov 14 21:18:58 CST 2018
     */
    private String permissionMenuId;

    /**
     * @mbg.generated Wed Nov 14 21:18:58 CST 2018
     */
    private static final long serialVersionUID = 1L;

    /**
     * @return the value of sm_permission.PERMISSION_ID
     *
     * @mbg.generated Wed Nov 14 21:18:58 CST 2018
     */
    public Integer getPermissionId() {
        return permissionId;
    }

    /**
     * @param permissionId the value for sm_permission.PERMISSION_ID
     *
     * @mbg.generated Wed Nov 14 21:18:58 CST 2018
     */
    public void setPermissionId(Integer permissionId) {
        this.permissionId = permissionId;
    }

    /**
     * @return the value of sm_permission.PERMISSION_NAME
     *
     * @mbg.generated Wed Nov 14 21:18:58 CST 2018
     */
    public String getPermissionName() {
        return permissionName;
    }

    /**
     * @param permissionName the value for sm_permission.PERMISSION_NAME
     *
     * @mbg.generated Wed Nov 14 21:18:58 CST 2018
     */
    public void setPermissionName(String permissionName) {
        this.permissionName = permissionName;
    }

    /**
     * @return the value of sm_permission.PERMISSION_POSITION_ID
     *
     * @mbg.generated Wed Nov 14 21:18:58 CST 2018
     */
    public String getPermissionPositionId() {
        return permissionPositionId;
    }

    /**
     * @param permissionPositionId the value for sm_permission.PERMISSION_POSITION_ID
     *
     * @mbg.generated Wed Nov 14 21:18:58 CST 2018
     */
    public void setPermissionPositionId(String permissionPositionId) {
        this.permissionPositionId = permissionPositionId;
    }

    /**
     * @return the value of sm_permission.PERMISSION_MENU_ID
     *
     * @mbg.generated Wed Nov 14 21:18:58 CST 2018
     */
    public String getPermissionMenuId() {
        return permissionMenuId;
    }

    /**
     * @param permissionMenuId the value for sm_permission.PERMISSION_MENU_ID
     *
     * @mbg.generated Wed Nov 14 21:18:58 CST 2018
     */
    public void setPermissionMenuId(String permissionMenuId) {
        this.permissionMenuId = permissionMenuId;
    }

	/**
	 *
	 */
	
	public Permission() {
		super();
	}

	/**
	 *
	 * @param permissionId
	 * @param permissionName
	 * @param permissionPositionId
	 * @param permissionMenuId
	 */
	
	public Permission(Integer permissionId, String permissionName, String permissionPositionId,
			String permissionMenuId) {
		super();
		this.permissionId = permissionId;
		this.permissionName = permissionName;
		this.permissionPositionId = permissionPositionId;
		this.permissionMenuId = permissionMenuId;
	}

	/**
	 *.
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Permission [permissionId=" + permissionId + ", permissionName=" + permissionName
				+ ", permissionPositionId=" + permissionPositionId + ", permissionMenuId=" + permissionMenuId + "]";
	}

    
}