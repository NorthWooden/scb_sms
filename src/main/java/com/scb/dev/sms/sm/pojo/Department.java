/**
 * Project Name:scb_sms
 * File Name:DepartmentDAO.java
 * Package Name:com.scb.dev.sms.sm.dao
 * Date:2018年11月15日 上午9:21:45
 * Copyright (c) 2018, clpsglobal All Rights Reserved.
 *
 */

package com.scb.dev.sms.sm.pojo;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonBackReference;

/**
 * 
 * ClassName: Department
 * Description: pojo
 * date: 2018年11月15日 上午9:21:45
 *
 * @author zack.guo
 * @version V1.0
 * @since JDK 1.8
 */

public class Department {

	/**
	 * @mbggenerated  Wed Nov 14 14:34:13 GMT+08:00 2018
	 */
	private Integer departmentId;
	/**
	 * @mbggenerated  Wed Nov 14 14:34:13 GMT+08:00 2018
	 */
	private String departmentName;
	/**
	 * @mbggenerated  Wed Nov 14 14:34:13 GMT+08:00 2018
	 */
	private Integer departmentIsvisible;
	/**
	 * @mbggenerated  Wed Nov 14 14:34:13 GMT+08:00 2018
	 */
	private String departmentDescription;
	/**
	 * @mbggenerated  Wed Nov 14 14:34:13 GMT+08:00 2018
	 */
	private String departmentLeader;
	/**
	 * @mbggenerated  Wed Nov 14 14:34:13 GMT+08:00 2018
	 */
	private Integer departmentPid;
	/**
	 * @mbggenerated  Wed Nov 14 14:34:13 GMT+08:00 2018
	 */
	private String departmentCreatorId;
	/**
	 * @mbggenerated  Wed Nov 14 14:34:13 GMT+08:00 2018
	 */
	private Date departmentCreatedTime;
	/**
	 * @mbggenerated  Wed Nov 14 14:34:13 GMT+08:00 2018
	 */
	private String departmentChangerId;
	/**
	 * @mbggenerated  Wed Nov 14 14:34:13 GMT+08:00 2018
	 */
	private Date departmentChangedTime;
	
	@JsonBackReference
	private Department parentDepartment;
	private List<Department> childDepartment;
	private Employee leaderEmployee;

	/**
	 * @return  the value of sm_department.DEPARTMENT_ID
	 * @mbggenerated  Wed Nov 14 14:34:13 GMT+08:00 2018
	 */
	public Integer getDepartmentId() {
		return departmentId;
	}

	/**
	 * @param departmentId  the value for sm_department.DEPARTMENT_ID
	 * @mbggenerated  Wed Nov 14 14:34:13 GMT+08:00 2018
	 */
	public void setDepartmentId(Integer departmentId) {
		this.departmentId = departmentId;
	}

	/**
	 * @return  the value of sm_department.DEPARTMENT_NAME
	 * @mbggenerated  Wed Nov 14 14:34:13 GMT+08:00 2018
	 */
	public String getDepartmentName() {
		return departmentName;
	}

	/**
	 * @param departmentName  the value for sm_department.DEPARTMENT_NAME
	 * @mbggenerated  Wed Nov 14 14:34:13 GMT+08:00 2018
	 */
	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	/**
	 * @return  the value of sm_department.DEPARTMENT_ISVISIBLE
	 * @mbggenerated  Wed Nov 14 14:34:13 GMT+08:00 2018
	 */
	public Integer getDepartmentIsvisible() {
		return departmentIsvisible;
	}

	/**
	 * @param departmentIsvisible  the value for sm_department.DEPARTMENT_ISVISIBLE
	 * @mbggenerated  Wed Nov 14 14:34:13 GMT+08:00 2018
	 */
	public void setDepartmentIsvisible(Integer departmentIsvisible) {
		this.departmentIsvisible = departmentIsvisible;
	}

	/**
	 * @return  the value of sm_department.DEPARTMENT_DESCRIPTION
	 * @mbggenerated  Wed Nov 14 14:34:13 GMT+08:00 2018
	 */
	public String getDepartmentDescription() {
		return departmentDescription;
	}

	/**
	 * @param departmentDescription  the value for sm_department.DEPARTMENT_DESCRIPTION
	 * @mbggenerated  Wed Nov 14 14:34:13 GMT+08:00 2018
	 */
	public void setDepartmentDescription(String departmentDescription) {
		this.departmentDescription = departmentDescription;
	}

	/**
	 * @return  the value of sm_department.DEPARTMENT_LEADER
	 * @mbggenerated  Wed Nov 14 14:34:13 GMT+08:00 2018
	 */
	public String getDepartmentLeader() {
		return departmentLeader;
	}

	/**
	 * @param departmentLeader  the value for sm_department.DEPARTMENT_LEADER
	 * @mbggenerated  Wed Nov 14 14:34:13 GMT+08:00 2018
	 */
	public void setDepartmentLeader(String departmentLeader) {
		this.departmentLeader = departmentLeader;
	}

	/**
	 * @return  the value of sm_department.DEPARTMENT_PID
	 * @mbggenerated  Wed Nov 14 14:34:13 GMT+08:00 2018
	 */
	public Integer getDepartmentPid() {
		return departmentPid;
	}

	/**
	 * @param departmentPid  the value for sm_department.DEPARTMENT_PID
	 * @mbggenerated  Wed Nov 14 14:34:13 GMT+08:00 2018
	 */
	public void setDepartmentPid(Integer departmentPid) {
		this.departmentPid = departmentPid;
	}

	/**
	 * @return  the value of sm_department.DEPARTMENT_CREATOR_ID
	 * @mbggenerated  Wed Nov 14 14:34:13 GMT+08:00 2018
	 */
	public String getDepartmentCreatorId() {
		return departmentCreatorId;
	}

	/**
	 * @param departmentCreatorId  the value for sm_department.DEPARTMENT_CREATOR_ID
	 * @mbggenerated  Wed Nov 14 14:34:13 GMT+08:00 2018
	 */
	public void setDepartmentCreatorId(String departmentCreatorId) {
		this.departmentCreatorId = departmentCreatorId;
	}

	/**
	 * @return  the value of sm_department.DEPARTMENT_CREATED_TIME
	 * @mbggenerated  Wed Nov 14 14:34:13 GMT+08:00 2018
	 */
	public Date getDepartmentCreatedTime() {
		return departmentCreatedTime;
	}

	/**
	 * @param departmentCreatedTime  the value for sm_department.DEPARTMENT_CREATED_TIME
	 * @mbggenerated  Wed Nov 14 14:34:13 GMT+08:00 2018
	 */
	public void setDepartmentCreatedTime(Date departmentCreatedTime) {
		this.departmentCreatedTime = departmentCreatedTime;
	}

	/**
	 * @return  the value of sm_department.DEPARTMENT_CHANGER_ID
	 * @mbggenerated  Wed Nov 14 14:34:13 GMT+08:00 2018
	 */
	public String getDepartmentChangerId() {
		return departmentChangerId;
	}

	/**
	 * @param departmentChangerId  the value for sm_department.DEPARTMENT_CHANGER_ID
	 * @mbggenerated  Wed Nov 14 14:34:13 GMT+08:00 2018
	 */
	public void setDepartmentChangerId(String departmentChangerId) {
		this.departmentChangerId = departmentChangerId;
	}

	/**
	 * @return  the value of sm_department.DEPARTMENT_CHANGED_TIME
	 * @mbggenerated  Wed Nov 14 14:34:13 GMT+08:00 2018
	 */
	public Date getDepartmentChangedTime() {
		return departmentChangedTime;
	}

	/**
	 * @param departmentChangedTime  the value for sm_department.DEPARTMENT_CHANGED_TIME
	 * @mbggenerated  Wed Nov 14 14:34:13 GMT+08:00 2018
	 */
	public void setDepartmentChangedTime(Date departmentChangedTime) {
		this.departmentChangedTime = departmentChangedTime;
	}

	/**
	 * parentDepartment.
	 *
	 * @return  the parentDepartment
	 */
	public Department getParentDepartment() {
		return parentDepartment;
	}

	/**
	 * parentDepartment.
	 *
	 * @param   parentDepartment    the parentDepartment to set
	 */
	public void setParentDepartment(Department parentDepartment) {
		this.parentDepartment = parentDepartment;
	}

	/**
	 * childDepartment.
	 *
	 * @return  the childDepartment
	 */
	public List<Department> getChildDepartment() {
		return childDepartment;
	}

	/**
	 * childDepartment.
	 *
	 * @param   childDepartment    the childDepartment to set
	 */
	public void setChildDepartment(List<Department> childDepartment) {
		this.childDepartment = childDepartment;
	}

	/**
	 * leaderEmployee.
	 *
	 * @return  the leaderEmployee
	 */
	public Employee getLeaderEmployee() {
		return leaderEmployee;
	}

	/**
	 * leaderEmployee.
	 *
	 * @param   leaderEmployee    the leaderEmployee to set
	 */
	public void setLeaderEmployee(Employee leaderEmployee) {
		this.leaderEmployee = leaderEmployee;
	}
	
}
