/**
 * Project Name:scb_sms
 * File Name:IPermissionService.java
 * Package Name:com.scb.dev.sms.sm.service
 * Date:2018年11月15日上午11:53:38
 * Copyright (c) 2018, Annie.Jiang@clps.com All Rights Reserved.
 *
 */
package com.scb.dev.sms.sm.service;

import java.util.List;

import com.scb.dev.sms.sm.pojo.Permission;

/**
 * ClassName: IPermissionService <br/>
 * Description: 
 * date: 2018年11月15日 上午11:53:38 <br/>
 *
 * @author Annie.Jiang
 * @version V1.0
 * @since JDK 1.8
 */
public interface IPermissionService {

	/**
	 * addPermission:新增权限. <br/>
	 * @param per
	 * @return int
	 * @throws Exception
	 */
	public int addPermission(Permission per) throws Exception;
	
	/**

	 * showPermission:显示权限信息. <br/>
	 * @param perId
	 * @return
	 * @throws Exception
	 */
	public Permission showPermission(int perId) throws Exception;
	
	/**
	 * showAllPermission:显示系统内的所有权限. <br/>
	 * @return
	 */
	public List<Permission> showAllPermissions() throws Exception;

	/**
	 * changePermission:修改权限内容. <br/>
	 * @param perId
	 * @return
	 * @throws Exception
	 */
	public int changePermission(Permission per) throws Exception;
	
	/**
	 * deletePermission:删除权限. <br/>
	 * @param perId
	 * @return
	 * @throws Exception
	 */
	public int deletePermission(int perId) throws Exception;
}
