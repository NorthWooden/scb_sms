/**
 * Project Name:scb_sms
 * File Name:DepartmentController.java
 * Package Name:com.scb.dev.sms.sm.controller
 * Date:2018年11月16日下午3:49:21
 * Copyright (c) 2018, clpsglobal All Rights Reserved.
 *
 */
package com.scb.dev.sms.sm.controller;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.scb.dev.sms.sm.pojo.Department;
import com.scb.dev.sms.sm.pojo.Employee;
import com.scb.dev.sms.sm.service.IDepartmentService;
import com.scb.dev.sms.sm.service.IEmployeeService;

/**
 * ClassName: DepartmentController
 * Description: 
 * date: 2018年11月16日 下午3:49:21 
 *
 * @author zack.guo
 * @version V1.0
 * @since JDK 1.8
 */
@Controller
@RequestMapping("/sm")
public class DepartmentController {
	@Resource
	IDepartmentService departmentService;
	@Resource
	IEmployeeService employeeService;
	/**
	 * @description 返回json类型的department数据
	 * @return List<Department>
	 */
	@RequestMapping("/departments/json/tree")
	@ResponseBody
	@GetMapping
	public List<Department> findDepartmentBytree(HttpSession session) {
		List<Department>dList = departmentService.findAllDepartmentByTree();
		session.setAttribute("log", dList);
		return dList;
	}
	/**
	 * @description 返回json类型的department数据
	 * @return List<Department>
	 */
	@RequestMapping("/departments/json")
	@ResponseBody
	@GetMapping
	public List<Department> findDepartment(HttpSession session) {
		List<Department>dList = departmentService.findAllDepartment();
		session.setAttribute("log", dList);
		return dList;
	}
	/**
	 * 
	 * @description 跳转到添加页面
	 * @return String
	 */
	@RequestMapping("/department")
	@GetMapping
	public String showAddDepartment(HttpServletRequest request) {
		List<Department>dlist=departmentService.findAllDepartment();
		request.setAttribute("departmentList", dlist);
		return "department_add";
	}
	/**
	 * 
	 * @description 跳转到更新页面
	 * @param Integer departmentId
	 * @param HttpServletRequest request
	 * @return String
	 */
	@RequestMapping("/department/edit/{departmentId}")
	@GetMapping
	public String showUpdateDepartment(@PathVariable("departmentId") Integer departmentId,HttpServletRequest request) {
		Department department=new Department();
		department=departmentService.findDepartmentById(departmentId);
		List<Department>dlist=departmentService.findAllDepartment();
		request.setAttribute("department", department);
		request.setAttribute("departmentList", dlist);
		return "department_update";
	}
	
	/**
	 * 
	 * @description 添加部门信息
	 * @param Department department
	 * @return String
	 */
	@RequestMapping("/depart")
	@PostMapping
	public String addDepartment(Department department,HttpSession session) {
		department.setDepartmentCreatorId("1024");//临时，之后使用session获取
		String mark=departmentService.addNewDepartment(department);
		session.setAttribute("log", mark);
		return "redirect:departments";
	}
	/**
	 * 
	 * @description 显示部门信息
	 * @param HttpServletRequest request
	 * @return String
	 */
	@RequestMapping("/departments")
	@GetMapping
	public String showQueryDepartment(HttpServletRequest request) {
		//List<Department> dlist=departmentService.findAllDepartmentByTree();
		//request.setAttribute("department", dlist);
		return "department_list";
	}
	/**
	 * 
	 * @description 删除部门
	 * @param Integer departmentId
	 * @param boolean single
	 * @return String
	 */
	@RequestMapping("/department/delete/{departmentId}&{single}")
	@GetMapping
	public String deleteDepartment(@PathVariable("departmentId") Integer departmentId,
			@PathVariable("single") boolean single,HttpSession session) {
		String mark=departmentService.deleteDepartment(departmentId, single);
		session.setAttribute("log", mark);
		return "redirect:../../departments";
	}
	/**
	 * 
	 * @description 更新部门
	 * @param Department department
	 * @return String
	 */
	@RequestMapping("/department/edit")
	@PostMapping
	public String updateDepartment(Department department,HttpSession session) {
		String mark=departmentService.updateDepartment(department);
		session.setAttribute("log", mark);
		return "redirect:../departments";
	}
	/**
	 * 
	 * @description:模糊搜索
	 * @param String searchString
	 * @param HttpServletRequest request
	 * @return String
	 */
	@RequestMapping("/department/searchString")
	@GetMapping
	public String searchDepartment(@PathVariable("searchString") String searchString,HttpServletRequest request) {
		List<Department>dlist=departmentService.findDepartmentByPartName(searchString);
		request.setAttribute("department", dlist);
		return "department_list";
	}
	/**
	 * 
	 * @description:查找职员信息，确认是否存在
	 * @param String employeeId
	 * @return Employee
	 */
	@RequestMapping("/findEmployee")
	@ResponseBody
	@GetMapping
	public Employee findEmployee(String employeeId,HttpSession session) {
		Employee employee=employeeService.queryEmployeeById(employeeId);
		session.setAttribute("log", employee);
		return employee;
	}
}
