/**
 * Project Name:scb_sms
 * File Name:PermissionServiceImpl.java
 * Package Name:com.scb.dev.sms.sm.service.impl
 * Date:2018年11月15日上午11:54:44
 * Copyright (c) 2018, Annie.Jiang@clps.com All Rights Reserved.
 *
 */
package com.scb.dev.sms.sm.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.scb.dev.sms.sm.dao.PermissionMapper;
import com.scb.dev.sms.sm.pojo.Permission;
import com.scb.dev.sms.sm.service.IPermissionService;

import lombok.Getter;
import lombok.Setter;

/**
 * ClassName: PermissionServiceImpl <br/>
 * Description: 
 * date: 2018年11月15日 上午11:54:44 <br/>
 *
 * @author Annie.Jiang
 * @version V1.0
 * @since JDK 1.8
 */
@Service
public class PermissionServiceImpl implements IPermissionService {

	 Permission permission;
	 PermissionMapper mapper;
	
	/**
	 * 
	 * @see com.scb.dev.sms.sm.service.IPermissionService#addPermission(com.scb.dev.sms.sm.pojo.Permission)
	 */
	@Override
	public int addPermission(Permission per) throws Exception {
		int result=mapper.insert(per);
		return result;
	}

	/**
	 * @see com.scb.dev.sms.sm.service.IPermissionService#showAllPermission(int)
	 */
	@Override
	public List<Permission> showAllPermissions() throws Exception {
		List<Permission> result=mapper.selsctAllPermission();
		return result;
	}

	/**
	 * @see com.scb.dev.sms.sm.service.IPermissionService#changePermission(int)
	 */
	@Override
	public int changePermission(Permission per) throws Exception {
		int result=mapper.updateByPrimaryKey(per);
		return result;
	}

	/**
	 
	 * @see com.scb.dev.sms.sm.service.IPermissionService#deletePermission(int)
	 */
	@Override
	public int deletePermission(int perId) throws Exception {
		int result=mapper.deleteByPrimaryKey(perId);
		return result;
	}

	/**
	 * @see com.scb.dev.sms.sm.service.IPermissionService#showPermission(int)
	 */
	@Override
	public Permission showPermission(int perId) throws Exception {
		Permission result=mapper.selectByPrimaryKey(perId);
		return result;
	}

	

}
