/**
 * Project Name:scb_sms
 * File Name:PositionService.java
 * Package Name:com.scb.dev.sms.sm.service.impl
 * Date:2018年11月15日下午2:50:52
 * Copyright (c) 2018,Grace.Lee@clps.com All Rights Reserved.
 *
 */
package com.scb.dev.sms.sm.service.impl;

import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.scb.dev.sms.common.CommonData;
import com.scb.dev.sms.sm.dao.PositionMapper;
import com.scb.dev.sms.sm.pojo.Menu;
import com.scb.dev.sms.sm.pojo.Position;
import com.scb.dev.sms.sm.service.IPositionService;
import com.scb.dev.sms.util.pagination.Page;

/**
 * ClassName: PositionService <br/>
 * Description: 
 * date: 2018年11月15日 下午2:50:52 <br/>
 *
 * @author Grace.Lee
 * @version V1.0
 * @since JDK 1.8
 */
@Service
public class PositionServiceImpl implements IPositionService {

	private Logger logger=Logger.getLogger(this.getClass());

	@Autowired
	private PositionMapper mapper;
	Position position;
	/**
	 * 查询所有职位信息
	 * @see com.scb.dev.sms.sm.service.IPositionService#queryAllPosititon()
	 */
	@Test
	public List<Position> queryAllPosititon() {
		List<Position> lp=mapper.selectAllPositionInfo();
		if(lp.size()>0) {
			  logger.info(CommonData.SUCCESS);
			  return lp;
		}else {
			logger.info(CommonData.FAILURES);
			return null;
		}
	  
		
	}

	/**
	 * 通过职位ID查询对应职位信息
	 * @see com.scb.dev.sms.sm.service.IPositionService#queryPositionById()
	 */
	@Override
	public Position queryPositionById(Integer positionId) {
		
		 position=mapper.selectByPrimaryKey(positionId);
		if(position!=null) {
			logger.info(CommonData.SUCCESS);
			return position;
		}else {
			logger.info(CommonData.FAILURES);
			return null;
		}
		
	}

	/**
	 * 
	 *  修改职位信息
	 * @see com.scb.dev.sms.sm.service.IPositionService#modifyPositionInfo(com.scb.dev.sms.sm.pojo.Position)
	 */
	@Override
	public String modifyPositionInfo(Position position) {
	
		this.position=position;
		Position p=mapper.selectByPrimaryKey(position.getPositionId());
		if(p!=null) {
			this.position.setPositionChangedTime(new Date());
			if(mapper.updateByPrimaryKey(this.position)>0) {
				return CommonData.UPDATE_SUCCESS;
			}else
				return CommonData.UPDATE_FAILURE;
		}else {
			return CommonData.QUERY_FAILURE;
		}
		
		
	}

	/**
	 * 
	 * 删除职位信息
	 * @see com.scb.dev.sms.sm.service.IPositionService#removePosition(java.lang.Integer)
	 */
	@Override 
	public String removePosition(Position pos) {
		
		 position=pos;
		if(mapper.selectByPrimaryKey(position.getPositionId())!=null) {
			int result=mapper.deleteByPrimaryKey(position.getPositionId());
			if(result>0) {
				return CommonData.DELETE_SUCCESS;
			}else
				return CommonData.DELETE_FAILURE;
		}else {
			return CommonData.QUERY_FAILURE;
		}
		
		
	}

	/**
	 * 
	 * 通过职位名称查询角色拥有的菜单列
	 * @see com.scb.dev.sms.sm.service.IPositionService#qeuryMenuByPosition(java.lang.String)
	 */
	@Override
	public List<Menu> qeuryMenuByPosition(Integer positionId) {
		
		List<Menu> list=mapper.selectMenuByPosition(positionId);
		if(list.size()>0) {
			
			logger.info(CommonData.QUERY_SUCCESS);
		}else
			logger.info(CommonData.QUERY_FAILURE);
		
		return list;
		
		
	}

	/**
	 * 
	 * 分页查询职位信息
	 * @see com.scb.dev.sms.sm.service.IPositionService#findByPage(com.scb.dev.sms.util.pagination.Page)
	 */
	@Override
	public List<Position> findByPage(Page page) {
		List<Position> positions=mapper.selectByPage(page);
		if(positions.size()>0) {
			logger.info(CommonData.SAVE_SUCCESS);
			return positions;
		}else {
			logger.info(CommonData.SAVE_FAILURE);
			return null;
		}
		
	}

	@Override
	public String insertPosition(Position position) {
		this.position=position;
		Position p=mapper.selectByPrimaryKey(position.getPositionId());
		if(p==null) {
			this.position.setPositionCreatedTime(new Date());
			if(mapper.insert(this.position)>0) {
				return CommonData.SAVE_SUCCESS;
			}else
				return CommonData.SAVE_FAILURE;
		}
		
		return null;
	}
	
	
	

	
}
