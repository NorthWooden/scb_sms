/**
 * Project Name:scb_sms
 * File Name:IMenu.java
 * Package Name:com.scb.dev.sms.sm.service
 * Date:2018年11月8日上午9:21:54
 * Copyright (c) 2018, gael.zhu@qq.com All Rights Reserved.
 *
 */
package com.scb.dev.sms.sm.service;

import java.util.List;

import com.scb.dev.sms.sm.pojo.Menu;
import com.scb.dev.sms.sm.vo.MenuVO;

/**
 * ClassName: IMenu <br/>
 * Description: Menu Service. <br/><br/>
 * date: 2018年11月8日 上午9:21:54 <br/>
 *
 * @author gael.zhu
 * @version V1.0
 * @since JDK 1.8
 */
public interface IMenuService {
	
	/**
	 * 
		 * @desciption 获取完整菜单(已处理的). <br/>     
		 * @return MenuTree 成型的森林:List<Node> 
	 */
	public List<MenuVO> getAllMenu();
	
	/**
	 * 
		 * @desciption 添加新菜单 <br/>     
		 * @param menu Menu
		 * @return String:详见CommonOperation
	 */
	public String insertMenu(Menu menu);
	
	/**
	 * 
		 * @desciption 更新菜单. <br/>     
		 * @param menu Menu
		 * @return String:详见CommonOperation
	 */
	public String updateMenu(Menu menu);
	
	/**
	 * 
		 * @desciption 根据ID删除指定菜单(包括子菜单) <br/>     
		 * @param menuId int
		 * @return String:详见CommonOperation
	 */
	public String deleteMenuById(int menuId);
	
	/**
	 * 
		 * @desciption 根据ID获取菜单 <br/>     
		 * @param menuId int
		 * @return String:详见CommonOperation
	 */
	public MenuVO getMenuById(Integer menuId);
	
	/**
	 * 
		 * @desciption 根据级别获取菜单 <br/>     
		 * @param menuLevel String:菜单级别
		 * @return String:详见CommonOperation
	 */
	public List<MenuVO> getMenuByLevel(String menuLevel);
}
