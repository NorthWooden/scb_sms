/**
 * Project Name:scb_sms
 * File Name:MailVO.java
 * Package Name:com.scb.dev.sms.sm.vo
 * Date:2018年11月9日上午10:24:18
 * Copyright (c) 2018, Annie.Jiang@clps.com All Rights Reserved.
 *
 */
package com.scb.dev.sms.sm.vo;

import java.io.Serializable;

/**
 * ClassName: MailVO <br/>
 * date: 2018年11月9日 上午10:24:18 <br/>
 * @author Annie.Jiang
 * @version V1.0
 * @since JDK 1.8
 */
public class MailVO implements Serializable{

	private static final long serialVersionUID = -3041435848984138063L;
	
	private String smtpHost;
	private String subject;
	private String content;
	private String imgFile;
	private String file;
	private String from;
	private String to;
	private String copyto;
	private String blindto;
	
	private String username;
	private String password;
	
	/**
	 *构造函数
	 */
	
	public MailVO() {
		super();
	}
	/**
	 * smtp.
	 * @return  the smtp
	 */
	public String getSmtpHost() {
		return smtpHost;
	}
	/**
	 * smtp.
	 * @param   smtp    the smtp to set
	 */
	public void setSmtpHost(String smtpHost) {
		this.smtpHost = smtpHost;
	}
	/**
	 * subject.
	 * @return  the subject
	 */
	public String getSubject() {
		return subject;
	}
	/**
	 * subject.
	 * @param   subject    the subject to set
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}
	/**
	 * mailBody.
	 * @return  the mailBody
	 */
	public String getContent() {
		return content;
	}
	/**
	 * mailBody.
	 * @param   mailBody    the mailBody to set
	 */
	public void setContent(String body) {
		this.content = body;
	}
	/**
	 * imgFile.
	 * @return  the imgFile
	 */
	public String getImgFile() {
		return imgFile;
	}
	/**
	 * imgFile.
	 * @param   imgFile    the imgFile to set
	 */
	public void setImgFile(String imgFile) {
		this.imgFile = imgFile;
	}
	/**
	 * file.
	 * @return  the file
	 */
	public String getFile() {
		return file;
	}
	/**
	 * file.
	 * @param   file    the file to set
	 */
	public void setFile(String file) {
		this.file = file;
	}
	/**
	 * from.
	 * @return  the from
	 */
	public String getFrom() {
		return from;
	}
	/**
	 * from.
	 * @param   from    the from to set
	 */
	public void setFrom(String from) {
		this.from = from;
	}
	/**
	 * to.
	 * @return  the to
	 */
	public String getTo() {
		return to;
	}
	/**
	 * to.
	 * @param   to    the to to set
	 */
	public void setTo(String to) {
		this.to = to;
	}
	/**
	 * copyto.
	 * @return  the copyto
	 */
	public String getCopyto() {
		return copyto;
	}
	/**
	 * copyto.
	 * @param   copyto    the copyto to set
	 */
	public void setCopyto(String copyto) {
		this.copyto = copyto;
	}
	/**
	 * blindto.
	 * @return  the blindto
	 */
	public String getBlindto() {
		return blindto;
	}
	/**
	 * blindto.
	 * @param   blindto    the blindto to set
	 */
	public void setBlindto(String blindto) {
		this.blindto = blindto;
	}
	
	
	/**
	 * username.
	 * @return  the username
	 */
	public String getUsername() {
		return username;
	}
	/**
	 * username.
	 * @param   username    the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}
	/**
	 * password.
	 * @return  the password
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * password.
	 * @param   password    the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
}
