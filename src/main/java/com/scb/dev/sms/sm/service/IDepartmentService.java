/**
 * Project Name:scb_sms
 * File Name:IDepartmentService.java
 * Package Name:com.scb.dev.sms.sm.service
 * Date:2018年11月15日上午10:36:14
 * Copyright (c) 2018, clpsglobal All Rights Reserved.
 *
 */
package com.scb.dev.sms.sm.service;

import java.util.List;

import com.scb.dev.sms.sm.pojo.Department;

/**
 * ClassName: IDepartmentService 
 * Description: service of department
 * date: 2018年11月15日 上午10:36:14 
 *
 * @author zack.guo
 * @version V1.0
 * @since JDK 1.8
 */
public interface IDepartmentService {
	/**
	 * 
	 * @description:查找所有部门，以list存储
	 * @return List<Department>
	 */
	public List<Department> findAllDepartment();
	/**
	 * 
	 * @description:以树的形式返回
	 * @return List<Department>
	 */
	public List<Department> findAllDepartmentByTree();
	/**
	 * 
	 * @description:按照部门名称查找部门信息
	 * @param departmentName String 部门名称
	 * @return Department
	 */
	public Department findOneDepartmentByName(String departmentName);
	/**
	 * 
	 * @description:添加新的部门，并设置部门领导
	 * @param department Department 使用对象保存信息
	 * @return String
	 * @throws Exception 
	 */
	public String addNewDepartment(Department department);
	/**
	 * 
	 * @description:逻辑删除部门，提供单独删除和迭代删除两种方法
	 * @param departmentId Integer 部门id
	 * @param single boolean 选择删除类型 true：单独删除 false：迭代删除
	 * @return String
	 */
	public String deleteDepartment(Integer departmentId,boolean single);
	/**
	 * 
	 * @description:修改部门信息
	 * @param Department department
	 * @return String
	 */
	public String updateDepartment(Department department);
	/**
	 * 
	 * @description:按照名字模糊查询
	 * @param String departmentName
	 * @return List<Department>
	 */
	public List<Department> findDepartmentByPartName(String departmentName);
	/**
	 * 
	 * @description:按照id查询
	 * @param Integer departmentId
	 * @return Department
	 */
	public Department findDepartmentById(Integer departmentId);
	/**
	 * 
	 * @description
	 * @param
	 * @return
	 */
	//public String resetDepartment(Integer departmentId);
}
