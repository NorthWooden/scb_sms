/**
 * Project Name:scb_sms
 * File Name:IPositionService.java
 * Package Name:com.scb.dev.sms.sm.service
 * Date:2018年11月15日下午2:36:09
 * Copyright (c) 2018,Grace.Lee@clps.com All Rights Reserved.
 *
 */
package com.scb.dev.sms.sm.service;

import java.util.List;

import com.scb.dev.sms.sm.pojo.Menu;
import com.scb.dev.sms.sm.pojo.Position;
import com.scb.dev.sms.util.pagination.Page;

/**
 * ClassName: IPositionService 
 * Description: 
 * date: 2018年11月15日 下午2:36:09 
 *
 * @author Grace.Lee
 * @version V1.0
 * @since JDK 1.8
 */

public interface IPositionService {

	/**
	 * 
	 * queryAllPosititon：查询所有职位信息 
	 * @return
	 */
	public List<Position> queryAllPosititon();
	
	/**
	 * 
	 * queryPositionById： 查询指定职位信息
	 * @param positionId  指定职位信息的ID
	 * @return
	 */
	public Position  queryPositionById(Integer positionId);
	
	/**
	 * 
	 * modifyPositionInfo:修改职位信息
	 * @param position  需要修改的职位信息
	 */
	public  String modifyPositionInfo(Position position) ;
	
	/**
	 * 
	 * removePosition：删除职位信息
	 * @param pos  需要删除的职位
	 */
	public  String removePosition(Position pos);

	/**
	 * 
	 * qeuryMenuByPosition 通过职位名称查询拥有的菜单
	 * @param positionName 职位名称
	 * @return
	 */
	public List<Menu> qeuryMenuByPosition(Integer positionId);
	
	/**
	 * 
	 * findByPage 通过页面查找职位信息
	 * @param page
	 * @return
	 */
	 public List<Position> findByPage(Page page);
	 
	 /**
	  * 
	  * insertPosition 新增position
	  * @param position
	  * @return
	  */
	 public String insertPosition(Position position );
}
