/**
* Project Name:scb_sms
 * File Name:Menu.java
 * Package Name:com.scb.dev.sms.sm.dao
 * Date:2018年11月8日上午11:51:48
 * Copyright (c) 2018, gael.zhu@qq.com All Rights Reserved.
 *
 */
package com.scb.dev.sms.sm.dao;

import java.util.List;

import com.scb.dev.sms.sm.pojo.Account;
import com.scb.dev.sms.sm.vo.AccountVO;

/**
 * ClassName: AccountMapper <br/>
 * Description: AccountMapper. <br/>
 * <br/>
 * date: 2018年11月8日 上午11:51:48 <br/>
 *
 * @author gael.zhu
 * @version V1.0
 * @since JDK 1.8
 */
public interface AccountMapper {

	/**
	 * 
	 * @desciption 根据ID删除用户. <br/>
	 * @param accountId String:TokenId
	 * @return int :（1表示成功，0表示失败）
	 */
	int deleteByPrimaryKey(String accountId);

	/**
	 * 
	 * @desciption 插入用户. <br/>
	 * @param account Account:用户信息
	 * @return int :（1表示成功，0表示失败）
	 */
	int insertSelective(Account account);

	/**
	 * 
	 * @desciption 根据Id查询用户. <br/>
	 * @param accountId String:用户TokenId
	 * @return AccountVO :用户显示信息
	 */
	AccountVO selectByPrimaryKey(String accountId);

	/**
	 * 
	 * @desciption 更新用户信息. <br/>
	 * @param account Account :用户信息
	 * @return int :（1表示成功，0表示失败）
	 */
	int updateByPrimaryKeySelective(Account account);

	/**
	 * 
		 * @desciption 获取所有用户. <br/>     
		 * @return List<AccountVO>:所有用户
	 */
	List<AccountVO> selectAllAccount();
}