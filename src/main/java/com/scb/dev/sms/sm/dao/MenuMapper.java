/**
 * Project Name:scb_sms
 * File Name:MenuMapper.java
 * Package Name:com.scb.dev.sms.sm.dao
 * Date:2018年11月8日上午11:51:48
 * Copyright (c) 2018, gael.zhu@qq.com All Rights Reserved.
 *
 */
package com.scb.dev.sms.sm.dao;

import java.util.List;

import com.scb.dev.sms.sm.pojo.Menu;
import com.scb.dev.sms.sm.vo.MenuVO;

/**
 * ClassName: MenuMapper <br/>
 * Description: MenuMapper. <br/>
 * <br/>
 * date: 2018年11月8日 上午11:51:48 <br/>
 *
 * @author gael.zhu
 * @version V1.0
 * @since JDK 1.8
 */
public interface MenuMapper {

	/**
	 * 
	 * @desciption 根据ID删除指定菜单（逻辑删除）. <br/>
	 * @param menuId Integer:需要删除的菜单Id
	 * @return int :（1表示成功，0表示失败）
	 */
	int deleteByPrimaryKey(Integer menuId);

	/**
	 * 
	 * @desciption 添加菜单. <br/>
	 * @param record Menu:添加菜单的内容
	 * @return int :（1表示成功，0表示失败）
	 */
	int insertSelective(Menu record);

	/**
	 * 
	 * @desciption 根据菜单Id查询菜单. <br/>
	 * @param menuId Integer:菜单Id
	 * @return menuVO :符合要求的菜单（名字正确且可视为1）
	 */
	MenuVO selectById(Integer menuId);

	/**
	 * 
	 * @desciption 查询所有菜单. <br/>
	 * @return List<menuVO> :符合要求的所有菜单（可视为1）
	 */
	List<MenuVO> selectAllMenu();

	/**
	 * 
	 * @desciption 根据ID修改菜单信息. <br/>
	 * @param record Menu:已经修改的菜单
	 * @return int :（1表示成功，0表示失败）
	 */
	int updateByPrimaryKeySelective(Menu record);

	
	/**
	 * 
	 * @desciption 根据级别查询菜单. <br/>
	 * @param menuLevel String:需要查询的Menu的Level
	 * @return List<menuVO> :符合要求的所有菜单（可视为1）
	 */
	List<MenuVO> selectMenuByLevel(String menuLevel);
	
	/**
	 * 
	 * @desciption 查询特定菜单下的所有菜单. <br/>
	 * @param menuLevel String:需要查询的Menu的Pid
	 * @return List<menuVO> :符合要求的所有菜单（可视为1）
	 */
	List<MenuVO> selectMenuByPid(Integer menuPid);
}