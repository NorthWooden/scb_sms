package com.scb.dev.sms.sm.controller;


import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.scb.dev.sms.common.CommonData;
import com.scb.dev.sms.sm.pojo.Employee;
import com.scb.dev.sms.sm.service.IEmployeeService;

/** 
 * ClassName: EmployeeController <br/>
 * date: 2018年11月21日 上午9:08:09 <br/>
 * @author Vanhom_Peng
 * @version V1.0
 * @since JDK 1.8
 */
@Controller
@RequestMapping(value="/sm")
public class EmployeeController {
	
	@Resource
	private IEmployeeService employeeService;
	Logger logger=Logger.getLogger(EmployeeController.class);
/**
 * 
 * showAllEmployees:(显示所有的职员信息). <br/>
 * @author Vanhom_Peng
 * @param model
 * @return
 * @throws Exception
 * @since JDK 1.8
 */
	@RequestMapping(value="/employees",method=RequestMethod.GET)
	public String showAllEmployees(Model model) throws Exception{
		List<Employee> emps=employeeService.queryAllEmployee();
		model.addAttribute("emps", emps);
		return "employee_list";
	}
	/**
	 * 
	 * selectEmployee:(根据职员ID查询职员信息). <br/>
	 * @author Vanhom_Peng
	 * @param model
	 * @param empId
	 * @return
	 * @throws Exception
	 * @since JDK 1.8
	 */
	@RequestMapping(value="/employee",method=RequestMethod.POST)
	public String selectEmployee(Model model,String employeeId) {
		Employee employee=employeeService.queryEmployeeById(employeeId);		
		if(null!=employee) {
			 logger.info(CommonData.QUERY_SUCCESS);
			 List<Employee> employees=new ArrayList<>();
			 employees.add(employee);
			 model.addAttribute("emps", employees);
			 return "employee_list";
		}else {
			 logger.info(CommonData.QUERY_FAILURE);
			 return "employee_list";
		 }
	}
	
/**
 * 
 * addEmployee:(添加职员). <br/>
 * @author Vanhom_Peng
 * @return
 * @since JDK 1.8
 */
	@RequestMapping(value="/employee/add",method=RequestMethod.GET)
	public String addEmployee() {
		return "employee_add";
	}

	@RequestMapping(value="/employee/add",method=RequestMethod.POST)
	public String employeeAdd(Employee emp) throws Exception {
		employeeService.addNewEmployee(emp);
		return "redirect:../employees";
	}
	
	/**
	 * 
	 * deleteEmployee:(删除职员). <br/>
	 * @author Vanhom_Peng
	 * @param employeeId
	 * @return
	 * @throws Exception
	 * @since JDK 1.8
	 */
	@RequestMapping(value="/employee/delete/{employeeId}",method=RequestMethod.GET)
	public String deleteEmployee(@PathVariable("employeeId")String employeeId){
		employeeService.removeEmployee(employeeService.queryEmployeeById(employeeId));
		return "redirect:../../employees";
	}

	/**
	 * 
	 * updateEmployee:(修改职员信息). <br/>
	 * @author Vanhom_Peng
	 * @param employeeId
	 * @param request
	 * @return
	 * @since JDK 1.8
	 */
	@RequestMapping(value="/employee/edit/{employeeId}",method=RequestMethod.GET)
	public String updateEmployee(@PathVariable("employeeId")String employeeId,HttpServletRequest request) {
		Employee employee=employeeService.queryEmployeeById(employeeId);
		request.setAttribute("employee",employee);
		return "employee_update";
	}
	@RequestMapping(value="/employee/edit",method=RequestMethod.POST)
	public String employeeUpdate(Employee employee) {
		employeeService.modifyEmployeeInfo(employee);
		return "redirect: ../employees";
	}
	
	
}
