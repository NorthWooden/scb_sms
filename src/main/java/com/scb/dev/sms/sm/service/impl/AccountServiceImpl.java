/**
 * Project Name:scb_sms
 * File Name:AccountServiceImpl.java
 * Package Name:com.scb.dev.sms.sm.service.impl
 * Date:2018年11月22日上午10:07:59
 * Copyright (c) 2018, gael.zhu@qq.com All Rights Reserved.
 *
 */
package com.scb.dev.sms.sm.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.scb.dev.sms.common.CommonData;
import com.scb.dev.sms.log.dao.AccountLogMapper;
import com.scb.dev.sms.sm.dao.AccountMapper;
import com.scb.dev.sms.sm.pojo.Account;
import com.scb.dev.sms.sm.service.IAccountService;
import com.scb.dev.sms.sm.vo.AccountVO;
import com.scb.dev.sms.util.factory.ToolFactory;

/**
 * ClassName: AccountServiceImpl <br/>
 * Description: Account Service Impl. <br/><br/>
 * date: 2018年11月22日 上午10:07:59 <br/>
 *
 * @author gael.zhu
 * @version V1.0
 * @since JDK 1.8
 */
@Service
public class AccountServiceImpl implements IAccountService {

	@Autowired
	private AccountMapper accountMapper;
	private int flag=0;
	@Autowired 
	private AccountVO accountVo;
	@Autowired
	private List<AccountVO> accountVoGroup;
	
	/**
	 * @see com.scb.dev.sms.sm.service.IAccountService#registerAccount(com.scb.dev.sms.sm.pojo.Account)
	 */
	@Override
	public String registerAccount(Account account) {
		if(null==account)
			return null;
		else {
			account.setAccountChangerId("6666");
			account.setAccountId(ToolFactory.getInstanceOfTokenIdTool().TokenIdProdution(CommonData.INSERTACCOUNT));
			account.setAccountChangedTime(ToolFactory.getInstanceOfDateTool().getTime());
			account.setAccountPwd(ToolFactory.getInstanceOfMD5Tool().getkeyBeanofStr(account.getAccountPwd()));
		}
		flag=this.accountMapper.insertSelective(account);
		if(flag>=1) {
			return CommonData.SAVE_SUCCESS;
		}
		else
			return CommonData.SAVE_FAILURE;
	}

	/**
	 * @see com.scb.dev.sms.sm.service.IAccountService#deleteAccount(java.lang.String)
	 */
	@Override
	public String deleteAccount(String accountId) {
		flag=this.accountMapper.deleteByPrimaryKey(accountId);
		if(flag>=1) {
			return CommonData.DELETE_SUCCESS;
		}
		else
			return CommonData.DELETE_FAILURE;
	}

	/**
	 * @see com.scb.dev.sms.sm.service.IAccountService#updateAccount(com.scb.dev.sms.sm.pojo.Account)
	 */
	@Override
	public String updateAccount(Account account) {
		if(null==account)
			return null;
		else {
			account.setAccountChangedTime(ToolFactory.getInstanceOfDateTool().getTime());
			account.setAccountPwd(ToolFactory.getInstanceOfMD5Tool().getkeyBeanofStr(account.getAccountPwd()));
			flag=this.accountMapper.updateByPrimaryKeySelective(account);
			if(flag>=1) {
				return CommonData.UPDATE_SUCCESS;
			}
			else
				return CommonData.UPDATE_FAILURE;
		}
	}

	/**
	 * @see com.scb.dev.sms.sm.service.IAccountService#selectAccountById(java.lang.String)
	 */
	@Override
	public AccountVO selectAccountById(String accountId) {
		this.accountVo=this.accountMapper.selectByPrimaryKey(accountId);
		if(null!=this.accountVo) 
			return this.accountVo;
		else
			return null;
	}

	/**
	 * @see com.scb.dev.sms.sm.service.IAccountService#selectAllAccount()
	 */
	@Override
	public List<AccountVO> selectAllAccount() {
		this.accountVoGroup=this.accountMapper.selectAllAccount();
		if(null==this.accountVoGroup)
			return null;
		else
			return this.accountVoGroup;
	}

	/**
	 * @see com.scb.dev.sms.sm.service.IAccountService#Login(com.scb.dev.sms.sm.pojo.Account)
	 */
	@Override
	public int login(Account account) {
		this.accountVoGroup=this.accountMapper.selectAllAccount();
		for(AccountVO accountVo:this.accountVoGroup) {
			if((accountVo.getAccountName().equals(account.getAccountName()))
					&&accountVo.getAccountPwd().equals(ToolFactory.getInstanceOfMD5Tool().getkeyBeanofStr(account.getAccountPwd()))) {
				return 1;
			}
		}
		return 0;
	}

}
