package com.scb.dev.sms.filter;
 
import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletResponse;

/**
 * ClassName: EncodingFilter <br/>
 * Description:To solve the code <br/>
 * date: 2018年11月13日 下午1:48:24 <br/>
 *
 * @author Cristal.Xu
 * @version V1.0
 * @since JDK 1.8
 */
@WebFilter(urlPatterns="/*")
public class EncodingFilter implements Filter {
   private FilterConfig fConfig;
     /** 
      * Creates a new instance of EncodingFilter. 
      */
    public EncodingFilter() { 
    	
    }
/** 
 * @see javax.servlet.Filter#destroy()
 */
	public void destroy() {
		 
	}

	 /** 
	  * @see javax.servlet.Filter#doFilter(javax.servlet.ServletRequest, javax.servlet.ServletResponse, javax.servlet.FilterChain)
	  */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		 
		request.setCharacterEncoding("UTF-8");
		((HttpServletResponse) response).setHeader("content-type","text/html;charset=UTF-8");
		response.setCharacterEncoding("UTF-8"); 
		chain.doFilter(request, response);
	}

	/** 
	 * @see javax.servlet.Filter#init(javax.servlet.FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException { 
		this.fConfig=fConfig;
	}

}
