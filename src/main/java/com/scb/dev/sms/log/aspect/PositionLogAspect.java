/**
 * Project Name:scb_sms
 * File Name:PositionLogAspect.java
 * Package Name:com.scb.dev.sms.log.aspect
 * Date:2018年12月3日下午1:24:22
 * Copyright (c) 2018, clpsglobal All Rights Reserved.
 *
 */
package com.scb.dev.sms.log.aspect;

import java.util.Date;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import com.scb.dev.sms.common.CommonData;
import com.scb.dev.sms.log.dao.PositionLogMapper;
import com.scb.dev.sms.log.pojo.PositionLog;

/**
 * ClassName: PositionLogAspect <br/>
 * Description: TODO ADD REASON(可选). <br/><br/>
 * date: 2018年12月3日 下午1:24:22 <br/>
 *
 * @author Grace.Lee
 * @version V1.0
 * @since JDK 1.8
 */
@Component
@Aspect
public class PositionLogAspect {

	@Resource
	private PositionLogMapper positionLogMapper;
	private PositionLog positionLog;
	@Pointcut("execution(* com.scb.dev.sms.sm.controller.PositionController.show*(..))")
	private void positionShowPoint() {};
	@Pointcut("execution(* com.scb.dev.sms.sm.controller.PositionController.delete*(..))")
	private void positionDeletePoint() {};
	@Pointcut("execution(* com.scb.dev.sms.sm.controller.PositionController.update*(..))")
	private void positionUpdatePoint() {};
	@Pointcut("execution(* com.scb.dev.sms.sm.controller.PositionController.add*(..))")
	private void positionAddPoint() {};
	
	
	@After(value="positionShowPoint()")
	public void doShowAfter(JoinPoint joinPoint) {
		
		HttpServletRequest request=((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getRequest();
		HttpSession session=request.getSession();
		String content="";
		if(request.getAttribute("log")!=null) {
			content=CommonData.QUERY_SUCCESS;
		}else {
			content=CommonData.QUERY_FAILURE;
		}
		String operator=(String) session.getAttribute("operator");
		positionLog=new PositionLog("show", new Date(),operator , content);
		positionLogMapper.insertSelective(positionLog);
		
	} 
	@After(value="positionDeletePoint()")
	public void doDeleteAfter(JoinPoint joinPoint) {
	
		HttpServletRequest request=((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getRequest();
		HttpSession session=request.getSession();
		String content=(String)request.getAttribute("log");
		String operator=(String) session.getAttribute("operator");
		positionLog=new PositionLog("delete",new Date(),operator,content);
		positionLogMapper.insertSelective(positionLog);
		
	}
	
	@After(value="positionUpdatePoint()")
	public void doUpdateAfter(JoinPoint joinPoint) {
		
		HttpServletRequest request=((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getRequest();
		HttpSession session=request.getSession();
		String content=(String)request.getAttribute("log");
		String operator=(String) session.getAttribute("operator");
		positionLog=new PositionLog("update",new Date(),operator,content);
		positionLogMapper.insertSelective(positionLog);
		
		
	}
	@After(value="positionAddPoint()")
	public void doAddAfter(JoinPoint joinPoint) {
		
		HttpServletRequest request=((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getRequest();
		HttpSession session=request.getSession();
		String content=(String) request.getAttribute("log");
		String operator=(String) session.getAttribute("operator");
		positionLog=new PositionLog("add",new Date(),operator,content);
		positionLogMapper.insertSelective(positionLog);
	}
}
	

