/**
 * Project Name:scb_sms
 * File Name:DepartmentLogAspect.java
 * Package Name:com.scb.dev.sms.log.aspect
 * Date:2018年12月1日下午10:28:46
 * Copyright (c) 2018, clpsglobal All Rights Reserved.
 *
 */
package com.scb.dev.sms.log.aspect;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.scb.dev.sms.common.CommonData;
import com.scb.dev.sms.log.dao.DepartmentLogMapper;
import com.scb.dev.sms.log.pojo.DepartmentLog;

/**
 * ClassName: DepartmentLogAspect <br/>
 * Description: log切面
 * date: 2018年12月1日 下午10:28:46 <br/>
 *
 * @author zack.guo
 * @version V1.0
 * @since JDK 1.8
 */
@Component
@Aspect
public class DepartmentLogAspect {
	@Autowired
	public DepartmentLogMapper depatmentLogMapper;
	public Logger logger = Logger.getLogger(this.getClass());
	DepartmentLog departmentLog;

	@Pointcut("execution(* com.scb.dev.sms.sm.controller.DepartmentController.find*(..))")
	private void departmentFindPoint() {};
	
	@Pointcut("execution(* com.scb.dev.sms.sm.controller.DepartmentController.add*(..))")
	private void departmentAddPoint() {};
	
	@Pointcut("execution(* com.scb.dev.sms.sm.controller.DepartmentController.update*(..))")
	private void departmentUpdatePoint() {};
	
	@AfterReturning(value="departmentFindPoint()",returning="rvt")
	public void doFindReturn(Object rvt) {
		logger.debug("----doReturn()开始----");
		HttpServletRequest request=((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getRequest();
		HttpSession session=request.getSession();
		String mark=null;
		Object object=session.getAttribute("log");
		if(object==null) {
			mark=CommonData.QUERY_FAILURE;
		}else {
			mark=CommonData.QUERY_SUCCESS;
		}
		String name=(String)session.getAttribute("operator");
		departmentLog=new DepartmentLog(null,new Date(),mark,"select",name);
		depatmentLogMapper.insertSelective(departmentLog);
	}
	
	@AfterReturning(value="departmentAddPoint()",returning="rvt")
	public void doAddReturn(Object rvt) {
		logger.debug("----doReturn()开始----");
		HttpServletRequest request=((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getRequest();
		HttpSession session=request.getSession();
		String mark=(String) session.getAttribute("log");
		String name=(String)session.getAttribute("operator");
		departmentLog=new DepartmentLog(null,new Date(),mark,"insert",name);
		depatmentLogMapper.insertSelective(departmentLog);
	}
	
	@AfterReturning(value="departmentUpdatePoint()",returning="rvt")
	public void doUpdateReturn(Object rvt) {
		logger.debug("----doReturn()开始----");
		HttpServletRequest request=((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getRequest();
		HttpSession session=request.getSession();
		String mark=(String) session.getAttribute("log");
		String name=(String)session.getAttribute("operator");
		departmentLog=new DepartmentLog(null,new Date(),mark,"update",name);
		depatmentLogMapper.insertSelective(departmentLog);
	}
}
