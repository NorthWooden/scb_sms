package com.scb.dev.sms.log.dao;

import java.util.Date;

import com.scb.dev.sms.log.pojo.MenuLog;

public interface MenuLogMapper {

	/**
	 * 
		 * @desciption  插入日志 <br/>     
		 * @param record MenuLog:日志内容
		 * @return int :（1表示成功，0表示失败）
	 */
    int insertSelective(MenuLog record);

    /**
     * 
    	 * @desciption 查询固定时间段内的日志. <br/>     
    	 * @param startTime Date:初始时间
    	 * @param endTime Date:结束时间
    	 * @return int :（1表示成功，0表示失败）
     */
    int selectByTime(Date startTime,Date endTime);
    
}