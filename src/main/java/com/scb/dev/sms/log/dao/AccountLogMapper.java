package com.scb.dev.sms.log.dao;

import java.util.Date;
import java.util.List;

import com.scb.dev.sms.log.pojo.AccountLog;

public interface AccountLogMapper {

	/**
	 * 
		 * @desciption 插入日志. <br/>     
		 * @param record AccountLog:日志内容
		 * @return int :（1表示成功，0表示失败）
	 */
    int insertSelective(AccountLog record);
    
    /**
     * 
    	 * @desciption 查询一段时间内的日志. <br/>     
    	 * @param startTime:开始时间
    	 * @param endTime:结束时间
    	 * @return List<AccountLog>:所有符合要求的日志
     */
    List<AccountLog> selectAccountLogByTime(Date startTime,Date endTime);
}