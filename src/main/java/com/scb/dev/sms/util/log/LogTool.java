/**
 * Project Name:scb_sms
 * File Name:LogTool.java
 * Package Name:com.scb.dev.sms.util.log
 * Date:2018年11月23日上午9:19:24
 * Copyright (c) 2018, gael.zhu@qq.com All Rights Reserved.
 *
 */
package com.scb.dev.sms.util.log;

import org.springframework.beans.factory.annotation.Autowired;

import com.scb.dev.sms.common.CommonData;
import com.scb.dev.sms.log.pojo.AccountLog;
import com.scb.dev.sms.sm.pojo.Account;
import com.scb.dev.sms.util.factory.ToolFactory;

/**
 * ClassName: LogTool <br/>
 * Description: LogTool <br/><br/>
 * date: 2018年11月23日 上午9:19:24 <br/>
 *
 * @author gael.zhu
 * @version V1.0
 * @since JDK 1.8
 */
public class LogTool {
	
	@Autowired
	private AccountLog accountLog=new AccountLog();
	public AccountLog logProduction(Account account) {
		this.accountLog.setAccountLogOperationTime(ToolFactory.getInstanceOfDateTool().getTime());
		this.accountLog.setAccountLogOperator(account.getAccountName());
		this.accountLog.setAccountLogOperation(CommonData.INSERTACCOUNT);
		this.accountLog.setAccountUpdatePwd(account.getAccountPwd());
		this.accountLog.setAccountLogOperationContent(account.getAccountName()+"  "+
		                                              CommonData.INSERTACCOUNT+"   "+ToolFactory.getInstanceOfDateTool().getTime());
		return this.accountLog;
	}
}
