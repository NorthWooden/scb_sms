/**
 * Project Name:scb_sms
 * File Name:ReceiveMail.java
 * Package Name:com.scb.dev.sms.util.mail
 * Date:2018年11月12日上午9:42:21
 * Copyright (c) 2018, Annie.Jiang@clps.com All Rights Reserved.
 *
 */
package com.scb.dev.sms.util.mail;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeUtility;

/**
 * ClassName: ReceiveMail <br/>
 * Description: 使用pop3协议接收邮件的工具类
 * date: 2018年11月12日 上午9:42:21 <br/>
 *
 * @author Annie.Jiang
 * @version V1.0
 * @since JDK 1.8
 */
public class ReceiveMail {

	public static void receive() throws Exception{
		
		Properties props=new Properties();
		props.getProperty(MailConfig.protocolP);  //协议
		props.getProperty(MailConfig.hostP);//pop3服务器
		
		//创建Session实例对象
		Session session=Session.getInstance(props);
		Store store=session.getStore(MailConfig.protocolP);
		store.connect(MailConfig.hostP, MailConfig.username, MailConfig.password);
		
		//获得收件箱
		Folder folder=store.getFolder("inbox");
		/**
		 * Folder.READ_ONLY:只读权限
		 * Folde.READ_WRITE:可读可写（可修改邮件的状态）
		 */
		folder.open(Folder.READ_WRITE);
		
		//
		System.out.println("未读邮件数："+folder.getUnreadMessageCount());
		
		//
		System.out.println("删除邮件数："+folder.getDeletedMessageCount());
		System.out.println("新邮件："+folder.getNewMessageCount());
		
		//收件箱的邮件总数
		System.out.println("邮件总数："+folder.getMessageCount());
		
		//得到收件箱中的所有邮件，并解析
		Message[] messages=folder.getMessages();
		parseMessage(messages);
		
		//释放资源
		folder.close(true);
		store.close();
	}

	/**
	 * parseMessage:解析邮件. <br/>
	 *@param messages
	 * @throws MessagingException 
	 * @throws IOException 
	 */
	public static void parseMessage(Message[] messages) throws MessagingException, IOException {
		if(messages==null||messages.length<1) {
			throw new MessagingException("未找到要解析的邮件！");
		}
		
		//解析所有邮件
		for (int i = 0; i < messages.length; i++) {
			MimeMessage msg=(MimeMessage) messages[i];
			System.out.println("----------解析第"+msg.getMessageNumber()+"封邮件--------");
			System.out.println("主题："+getSubject(msg));
			System.out.println("发件人："+getFrom(msg));
			System.out.println("收件人："+getReceiveAddress(msg,null));
			System.out.println("发送时间："+getSentDate(msg,null));
			System.out.println("是否已读："+isSeen(msg));
			boolean isContainAffix=isContainAffix(msg);
			System.out.println("是否包含附件："+isContainAffix);
			if(isContainAffix) {
				saveAffix(msg,null);//保存附件
			}
			StringBuffer content=new StringBuffer(30);
			getMailTextContent(msg,content);
			System.out.println("邮件正文："+(content.length()>100?content.substring(0,100)+"...":content));
			System.out.println("----------第"+msg.getMessageNumber()+"封邮件解析结束--------");
			System.out.println();
		}
	}


	

	/**
	 * getSubject:获取邮件主题. <br/>
	 *@param msg 邮件内容
	 *@return 
	 * @throws MessagingException 
	 * @throws UnsupportedEncodingException 
	 */
	public static String getSubject(MimeMessage msg) throws UnsupportedEncodingException, MessagingException {
		
		return MimeUtility.decodeText(msg.getSubject());
	}
	
	/**
	 * getFrom:获取邮件发件人. <br/>
	 *@param msg
	 *@return 姓名<Email地址>
	 * @throws MessagingException 
	 * @throws UnsupportedEncodingException 
	 */
	@SuppressWarnings("unused")
	private static String getFrom(MimeMessage msg) throws MessagingException, UnsupportedEncodingException {
		
		String from="";
		Address[] froms=msg.getFrom();
		if(froms.length<1) {
			throw new MessagingException("没有发件人！");
		}
		InternetAddress address=(InternetAddress) froms[0];
		String person=address.getPersonal()+" ";
		if(person!=null) {
			person=MimeUtility.decodeText(person)+" ";
		}else {
			person=" ";
		}
		from=person+"<"+address.getAddress()+">";
		return from;
	}
	
	/**
	 * getReceiveAddress:根据收件人类型，获取邮件的收件人、抄送和密送地址，如果收件人类型为空，则获得所有的收件人. <br/>
	 * @param msg 邮件内容
	 * @param type 收件人类型
	 * @return
	 * @throws MessagingException 
	 *
	 */
	public static String getReceiveAddress(MimeMessage msg, Message.RecipientType type) throws MessagingException {

		StringBuffer receiveAddress=new StringBuffer();
		Address[] address=null;
		if(type==null) {
			address=msg.getAllRecipients();
		}else {
			address=msg.getRecipients(type);
		}
		
		if(address==null||address.length<1) {
			throw new MessagingException("没有收件人！");
		}
		for (Address address2 : address) {
			InternetAddress internetAddress=(InternetAddress)address2;
			receiveAddress.append(internetAddress.toUnicodeString()).append(",");
		}
		receiveAddress.deleteCharAt(receiveAddress.length()-1);
		return receiveAddress.toString();
	}
	
	/**
	 * 
	 * getSentDate:获取邮件发送时间. <br/>
	 *@param msg
	 *@return yyyy年mm月dd日 星期x HH:mm
	 *@throws MessagingException
	 */
	public static String getSentDate(MimeMessage msg,String pattern) throws MessagingException {
		Date receiveDate=msg.getSentDate();
		if(receiveDate==null) {
			return "";
		}
		
		if(pattern==null||"".equals(pattern)) {
			pattern="yyyy年MM月dd日 E HH:mm ";
		}
		
		return new SimpleDateFormat(pattern).format(receiveDate);
	}
	
	/**
	 * isContainAffix:判断是否包含附件. <br/>
	 *@param part
	 *@return 存在返回true 不存在返回false
	 * @throws MessagingException 
	 * @throws IOException 
	 */
	public static boolean isContainAffix(Part part) throws MessagingException, IOException {

		boolean flag=false;
		if(part.isMimeType("multipart/*")) {
			MimeMultipart multipart=(MimeMultipart) part.getContent();
			int partCount=multipart.getCount();
			for (int i = 0; i < partCount; i++) {
				BodyPart bodyPart=multipart.getBodyPart(i);
				String disp=bodyPart.getDescription();
				if(disp!=null&&(disp.equalsIgnoreCase(Part.ATTACHMENT)||disp.equalsIgnoreCase(Part.INLINE))) {
					flag=true;
				}else if(bodyPart.isMimeType("multipart/*")) {
					flag=isContainAffix(bodyPart);
				}else {
					String contentType=bodyPart.getContentType();
					if(contentType.indexOf("application")!=-1) {
						flag=true;
					}
					if(contentType.indexOf("name")!=-1) {
						flag=true;
					}
				}
				if(flag) break;
			}
		}else if(part.isMimeType("message/rfc822")) {
			flag=isContainAffix((Part)part.getContent());
		}
		return flag;
	}
	
	/**
	 * isSeen:判断邮件是否已读. <br/>
	 *@param msg
	 *@return 已读则返回true,否则返回false
	 * @throws MessagingException 
	 */
	public static boolean isSeen(MimeMessage msg) throws MessagingException {

		return msg.getFlags().contains(Flags.Flag.SEEN);
	}
	
	/**
	 * getMailTextContent:获取邮件文本内容. <br/>
	 *@param part 邮件体
	 *@param content 存储邮件文本内容的字符串
	 * @throws MessagingException 
	 * @throws IOException 
	 */
	public static void getMailTextContent(Part part, StringBuffer content) throws MessagingException, IOException {
		
		boolean isContainTextAttach=part.getContentType().indexOf("name")>0;
		if(part.isMimeType("text/*")&&!isContainTextAttach) {
			content.append(part.getContent().toString());
		}else if(part.isMimeType("message/rfc822")) {
			getMailTextContent((Part)part.getContent(),content);
		}else if(part.isMimeType("multipart/*")) {
			Multipart multipart=(Multipart) part.getContent();
			int partCount=multipart.getCount();
			for (int i = 0; i < partCount; i++) {
				BodyPart bodyPart=multipart.getBodyPart(i);
				getMailTextContent(bodyPart,content);
			}
		}
	}
	
	/**
	 * saveAffix:保存附件. <br/>
	 *@param part 邮件中多个组合体中的其中一个组合体
	 *@param destDir 附近保存目录
	 * @throws IOException 
	 * @throws MessagingException 
	 */
	public static void saveAffix(Part part, String destDir) throws MessagingException, IOException {
		
		if(part.isMimeType("multipart/*")) {
			Multipart multipart=(Multipart) part.getContent();  //复杂体邮件
			int partCount=multipart.getCount();
			for (int i = 0; i < partCount; i++) {
				BodyPart bodyPart=multipart.getBodyPart(i);
				String disp=bodyPart.getDescription();
				if(disp!=null&&(disp.equalsIgnoreCase(Part.ATTACHMENT)||disp.equalsIgnoreCase(Part.INLINE))) {
					InputStream is=bodyPart.getInputStream();
					saveFile(is,destDir,decodeText(bodyPart.getFileName()));
				}else if(bodyPart.isMimeType("multipart/*")) {
					saveAffix(bodyPart,destDir);
				}else {
					String contentType=bodyPart.getContentType();
					if(contentType.indexOf("name")!=-1||contentType.indexOf("application")!=-1) {
						saveFile(bodyPart.getInputStream(),destDir,decodeText(bodyPart.getFileName()));
					}
				}
			}
		}else if(part.isMimeType("message/rfc822")) {
			saveAffix((Part) part.getContent(),destDir);
		}
	}
	

	/**
	 * 
	 * saveFile:读取输入流中的数据保存至指定目录. <br/>
	 *@param is 输入流
	 *@param fileName 文件名
	 *@param destDir 文件存储目录
	 * @throws IOException 
	 */
	public static void saveFile(InputStream is,String destDir,String fileName) throws IOException {
		BufferedInputStream bis=new BufferedInputStream(is);
		BufferedOutputStream bos=new BufferedOutputStream(
									new FileOutputStream(new File(destDir+fileName)));
		int len=-1;
		while((len=bis.read())!=-1) {
			bos.write(len);
			bos.flush();
		}
		bos.close();
		bis.close();
	}
	
	/**
	 * decodeText:文本解码. <br/>
	 *@param encodeText 解码MimeUtility.encodeText(String text)方法解码后的文本
	 *@return 
	 * @throws UnsupportedEncodingException 
	 */
	public static String decodeText(String encodeText) throws UnsupportedEncodingException {
		
		if(encodeText==null||"".equals(encodeText)) {
			return "";
		}else {
			return MimeUtility.decodeText(encodeText);
		}
		
	}
	
}
