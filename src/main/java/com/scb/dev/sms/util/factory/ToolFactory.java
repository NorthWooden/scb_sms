/**
----------------- * Project Name:clps.sms
 * File Name:ToolFactory.java
 * Package Name:com.clps.dev.sms.util.factory
 * Date:2018年10月31日下午2:18:26
 * Copyright (c) 2018, gael.zhu@qq.com All Rights Reserved.
 *
 */
package com.scb.dev.sms.util.factory;

import com.scb.dev.sms.util.date.DateTool;
import com.scb.dev.sms.util.date.TimeTool;
import com.scb.dev.sms.util.date.TokenIdTool;
import com.scb.dev.sms.util.log.LogTool;
import com.scb.dev.sms.util.method.MethodSelectTool;
import com.scb.dev.sms.util.method.RandomNumber;
import com.scb.dev.sms.util.security.MD5Tool;
import com.scb.dev.sms.util.tree.MenuTreeUtil;

/**
 * ClassName: ToolFactory <br/>
 * Description: Tool Factory <br/><br/>
 * date: 2018年10月31日 下午2:18:26 <br/>
 *
 * @author gael.zhu
 * @version V1.0
 * @since JDK 1.8
 */
public class ToolFactory {
	
	/**
	 * 
		 * @desciption DateTool. <br/>     
		 * @return DateTool
	 */
	public static DateTool getInstanceOfDateTool() {
		return new DateTool();
	}
	
	/**
	 * 
		 * @desciption TimeTool. <br/>     
		 * @return TimeTool
	 */
	public static TimeTool getInstanceOfTimeTool() {
		return new TimeTool();
	}
	
	/**
	 * 
		 * @desciption RandomNumber. <br/>     
		 * @return RandomNumber
	 */
	public static RandomNumber getInstanceOfRandomNumber() {
		return new RandomNumber();
	}
	
	/**
	 * 
		 * @desciption MD5Tool. <br/>     
		 * @return MD5Tool
	 */
	public static MD5Tool getInstanceOfMD5Tool() {
		return new MD5Tool();
	}
	
	/**
	 * 
		 * @desciption TokenIdTool. <br/>     
		 * @return TokenIdTool
	 */
	public static TokenIdTool getInstanceOfTokenIdTool() {
		return new TokenIdTool();
	}

	/**
	 * 
		 * @desciption MethodSelectTool. <br/>     
		 * @return MethodSelectTool
	 */
	public static MethodSelectTool getInstanceOfMethodSelectTool() {
		return new MethodSelectTool();
	}
	
	/**
	 * 
		 * @desciption MenuTreeUtil. <br/>     
		 * @return MenuTreeUtil
	 */
	public static MenuTreeUtil getInstanceOfMenuTreeUtil() {
		return new MenuTreeUtil();
	}
	
	/**
	 * 
		 * @desciption LogTool. <br/>     
		 * @return LogToo
	 */
	public static LogTool getInstanceOfLogTool() {
		return new LogTool();
	}
}
