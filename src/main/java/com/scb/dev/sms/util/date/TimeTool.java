/**
 * Project Name:clps.sms
 * File Name:Time.java
 * Package Name:com.clps.dev.sms.util.date
 * Date:2018年10月31日下午2:20:48
 * Copyright (c) 2018, gael.zhu@qq.com All Rights Reserved.
 *
 */
package com.scb.dev.sms.util.date;

import java.util.Calendar;

/**
 * ClassName: Time <br/>
 * Description: Time Tool. <br/><br/>
 * date: 2018年10月31日 下午2:20:48 <br/>
 *
 * @author gael.zhu
 * @version V1.0
 * @since JDK 1.8
 */
public class TimeTool {
	public String getTimeZone(){
	    Calendar cal = Calendar.getInstance();
	    int offset = cal.get(Calendar.ZONE_OFFSET);
	    cal.add(Calendar.MILLISECOND, -offset);
	    Long timeStampUTC = cal.getTimeInMillis();
	    Long timeStamp = System.currentTimeMillis();
	    Long timeZone = (timeStamp - timeStampUTC) / (1000 * 3600);
	    return String.valueOf(timeZone);
	}
}
