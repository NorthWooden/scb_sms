/**
 * Project Name:clps.sms
 * File Name:MethodSelectTool.java
 * Package Name:com.clps.dev.sms.util.others
 * Date:2018年10月31日下午11:23:50
 * Copyright (c) 2018, gael.zhu@qq.com All Rights Reserved.
 *
 */
package com.scb.dev.sms.util.method;

import com.scb.dev.sms.common.CommonData;

/**
 * ClassName: MethodSelectTool <br/>
 * Description: Select Method. <br/><br/>
 * date: 2018年10月31日 下午11:23:50 <br/>
 *
 * @author gael.zhu
 * @version V1.0
 * @since JDK 1.8
 */
public class MethodSelectTool {
	/**
	 * 
		 * @desciption 获取方法编码 <br/>     
		 * @param method name
		 * @return method number
	 */
	public String AccountMethodSelect(String methodName) {
		if(methodName.equals("addAccount"))
			return CommonData.ADDACCOUNT;
		return null;
	}
}
