/**
 * Project Name:clps.sms
 * File Name:TokenIdTool.java
 * Package Name:com.clps.dev.sms.util.date
 * Date:2018年10月31日下午5:34:21
 * Copyright (c) 2018, gael.zhu@qq.com All Rights Reserved.
 *
 */
package com.scb.dev.sms.util.date;

import java.util.Date;

import com.scb.dev.sms.common.CommonData;
import com.scb.dev.sms.util.factory.ToolFactory;

/**
 * ClassName: TokenIdTool <br/>
 * Description: TokenId. <br/><br/>
 *              1位为时区
 *              2-8位为0-9随机数字
 *              9-16位为特定动作代码数字
 *              17-24位为月份日期小时分钟
 *              25-26/27位为随机固5定数字+0-9随机数字
 * date: 2018年10月31日 下午5:34:21 <br/>
 *
 * @author gael.zhu
 * @version V1.0
 * @since JDK 1.8
 */
public class TokenIdTool {
	
	/**
	 * 
		 * @desciption 生成TokenId <br/>     
		 * @param method name String
		 * @return tokenId String
	 */
	public String TokenIdProdution(String methodName){
		String preCode="";
		
		//生成时区的八位
		preCode=preCode+ToolFactory.getInstanceOfTimeTool().getTimeZone();
        preCode=ToolFactory.getInstanceOfRandomNumber().UpdatedRandom(8, preCode);
        //生成方法的八位
        preCode=preCode+ToolFactory.getInstanceOfMethodSelectTool().AccountMethodSelect(methodName);
        //生成时间的八位
		preCode=preCode+ToolFactory.getInstanceOfDateTool().DateToString(new Date(), CommonData.MMDDHHMM);
		//生成随机数的八位
		preCode=preCode+CommonData.TOKENIDRANDOM[(int)(Math.random()*3)];
        preCode=ToolFactory.getInstanceOfRandomNumber().UpdatedRandom(32, preCode);
        //MD5加密
		preCode=ToolFactory.getInstanceOfMD5Tool().getkeyBeanofStr(preCode);
		return preCode;
	}

}
