/**
 * Project Name:clps.sms
 * File Name:RandomNumber.java
 * Package Name:com.clps.dev.sms.util.others
 * Date:2018年10月31日下午7:56:22
 * Copyright (c) 2018, gael.zhu@qq.com All Rights Reserved.
 *
 */
package com.scb.dev.sms.util.method;

/**
 * ClassName: RandomNumber <br/>
 * Description: random Number. <br/><br/>
 * date: 2018年10月31日 下午7:56:22 <br/>
 *
 * @author gael.zhu
 * @version V1.0
 * @since JDK 1.8
 */
public class RandomNumber {
	/**
	 * 
		 * @desciption 生成0-9随机数字补全到你要的位数. <br/>     
		 * @param length int     需要位数
		 * @param text String    需要补全的字符串
		 * @return text String
	 */
	public String UpdatedRandom(int length,String text) {
		while(text.length()<length) {
			text=text+(int)(Math.random()*10);
		}
		return text;
	}
}
