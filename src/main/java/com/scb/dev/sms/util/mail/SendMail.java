/**
 * Project Name:scb_sms
 * File Name:Mail.java
 * Package Name:com.scb.dev.sms.util.mail
 * Date:2018年11月6日下午4:54:34
 * Copyright (c) 2018, Annie.Jiang@clps.com All Rights Reserved.
 *
 */
package com.scb.dev.sms.util.mail;

import java.io.FileInputStream;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;

/**
 * ClassName: Mail <br/>
 * date: 2018年11月6日 下午4:54:34 <br/>
 * @author Annie.Jiang
 * @version V1.0
 * @since JDK 1.8
 */
public class SendMail {
	
	//邮箱传输协议
	private static final String PROTOCOL=MailConfig.protocol;
	//服务主机名
	private static final String HOST=MailConfig.host;
	//第三方授权登陆的用户名和密码
	private static final String USERNAME=MailConfig.username;
	private static final String PASSWORD=MailConfig.password;
	
//	private static final String DEFAULTENCODING=MailConfig.defaultEncoding;
	//用户名和密码的校验
	private static final String AUTH=MailConfig.auth;
	
//	private static final String TIMEOUT=MailConfig.timeout;
	
	//邮件正文格式编码
	private static final String CONTENTENCODING=MailConfig.contentEncoding;

	
	private MimeMessage mimeMessage;  //Mime邮件对象
	private Session session;  //邮件会话对象
	private Properties properties;  //系统属性
//	private boolean needAuth=false;  //stmp是否需要认证
	
	//stmp认证的用户名和密码
//	private String username;
//	private String password;
	
	private Multipart multipart;  //Multipart对象 邮件内容 标题 附件等内容添加到这里面，然后生成MineMessage对象
	
	
	/**
	 * 构造方法
	 * @param stmp
	 */
	public SendMail(String smtp) {
		setSmtpHost(smtp);
		createMineMessage();
	}
	
	/**
	 * 
	 * createMineMessage:创建MineMessage邮件对象. <br/>
	 *@return
	 */
	public boolean createMineMessage() {
		//获取邮件会话对象
		session=Session.getDefaultInstance(properties,null);
		//创建Mine邮件对象
		mimeMessage=new MimeMessage(session);
		multipart=new MimeMultipart();
		return true;
	}
	
	/**
	 * 
	 * setStmpHost:设置邮件发送服务器. <br/>
	 *@param hostName
	 */
	public void setSmtpHost(String hostName) {
		if(properties==null) {
			properties=System.getProperties();  //获取系统属性对象
		}
		properties.put(HOST, hostName);  //设置smtp主机
	}
	
	/**
	 * 
	 * setNeedAuth:设置smtp是否需要认证. <br/>
	 *@param need
	 */
	public void setNeedAuth(boolean need) {
		if(properties==null) {
			properties=System.getProperties();
		}
		if(need) {
			properties.put(AUTH, "true");
		}else {
			properties.put(AUTH, "false");
		}
	}
	
	/**
	 * 
	 * setNamePassword:发件人的用户名和密码  163的用户名就是邮箱的前缀. <br/>
	 *@param username
	 *@param password
	 */
//	public void setNamePassword(String username,String password) {
//		this.username=username;
//		this.password=password;
//	}
	
	/**
	 * 
	 * setSubject:邮件主题. <br/>
	 *@param subject
	 *@return
	 */
	public boolean setSubject(String subject) {
		try {
			mimeMessage.setSubject(subject);
			return true;
		} catch (MessagingException e) {
			
			e.printStackTrace();
//			Logger.error(e.getMessage(),e);
			return false;
		}
	}
	
	/**
	 * 
	 * setBody:邮件正文. <br/>
	 *@param mailBody
	 *@return
	 */
	public boolean setBody(String mailBody) {
		BodyPart bodyPart=new MimeBodyPart();
		try {
			bodyPart.setContent(""+mailBody,CONTENTENCODING);
			multipart.addBodyPart(bodyPart);
			return true;
		} catch (MessagingException e) {
			
			e.printStackTrace();
			return false;
		}
	}
	
	/**
	 * 
	 * setBodyWithImg:邮件正文（带图片）. <br/>
	 *@param mailBody
	 *@param imgFile
	 *@return
	 */
	public boolean setBodyWithImg(String mailBody,String imgFile) {
		BodyPart content=new MimeBodyPart();
		BodyPart img=new MimeBodyPart();
		try {
			multipart.addBodyPart(content);
			multipart.addBodyPart(img);
			
			ByteArrayDataSource byteArrayDataSource=new ByteArrayDataSource(new FileInputStream(imgFile),"application/octet-stram");
			DataHandler imgDataHandler=new DataHandler(byteArrayDataSource);
			img.setDataHandler(imgDataHandler);
			
			//图片文件名
			String imgFileName=imgFile.substring(imgFile.lastIndexOf("/")+1);
			
			String headerValue="<"+imgFileName+">";
			img.setHeader("Content-ID", headerValue);
			//为图片设置文件名，有的邮箱会把html内嵌的图片也当成附件
			img.setFileName(imgFileName);
			//在html代码中想要显示刚才的图片名 src里不能直接写Content-ID的值，要用cid:这种方式
//			mailBody+="<img src='cid:"+imgFileName+"' alt='picture' width='100px' heigth='100px' />";
			content.setContent(""+mailBody,CONTENTENCODING);
			return true;
		} catch (Exception e) {
			
			e.printStackTrace();
			return false;
		}
	}
	
	/**
	 * 
	 * addFileAffix:邮件添加附件. <br/>
	 *@param file
	 *@return
	 */
	public boolean addFileAffix(String file) {
		String[] fileArray=file.split(",");
		
		for (int i = 0; i < fileArray.length; i++) {
			FileDataSource fileDataSource=new FileDataSource(fileArray[i]);
			try {
				BodyPart bodyPart=new MimeBodyPart();
				bodyPart.setDataHandler(new DataHandler(fileDataSource));
				bodyPart.setFileName(fileDataSource.getName());
				multipart.addBodyPart(bodyPart);
			} catch (MessagingException e) {
				
				e.printStackTrace();
				return false;
			}
		}
		return true;
	}
	
	/**
	 * 
	 * setFrom:发件人邮箱. <br/>
	 *@param from
	 *@return
	 */
	public boolean setFrom(String from) {
		try {
			mimeMessage.setFrom(new InternetAddress(from));
			return true;
		}catch (MessagingException e) {
			
			e.printStackTrace();
			return false;
		}
	}
	
	/**
	 * 
	 * setTo:收件人邮箱. <br/>
	 *@param to
	 *@return
	 */
	public boolean setTo(String to) {
		if(to==null) 
			return false;
			try {
				//电子邮件可以有三种类型的收件人，分别为收件人to,抄送cc(carbon copy)和密送bcc(blind carbon copy)
				mimeMessage.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
				return true;
			} catch (MessagingException e) {
				
				e.printStackTrace();
				return false;
			}
	}
	
	/**
	 * 
	 * setCopyTo:抄送人邮箱，字符串中用逗号隔开. <br/>
	 *@param copyto
	 *@return
	 */
	public boolean setCopyTo(String copyto) {
		if(copyto==null) {
			return false;
		}
		try {
			mimeMessage.setRecipients(Message.RecipientType.CC,(Address[]) InternetAddress.parse(copyto));
			return true;
		} catch (MessagingException e) {
			
			e.printStackTrace();
			return false;
		}
	}
	
	/**
	 * 
	 * setBlindTo:密送人邮箱. <br/>
	 *@param blindto
	 *@return
	 */
	public boolean setBlindTo(String blindto) {
		if(blindto==null) {
			return false;
		}
		try {
			mimeMessage.setRecipients(Message.RecipientType.BCC, (Address[]) InternetAddress.parse(blindto));
			return true;
		} catch (MessagingException e) {
			
			e.printStackTrace();
			return false;
		}
	}
	
	/**
	 * 
	 * sendOut:发送. <br/>
	 *@param copyto 抄送人
	 *@param blindto 密送人
	 *@return
	 */
	public boolean sendOut(String copyto,String blindto) {
		try {
			//multipart放入message
			mimeMessage.setContent(multipart);
			mimeMessage.saveChanges();
			Session mailSession=Session.getInstance(properties, null);
			Transport transport=mailSession.getTransport(PROTOCOL);
			transport.connect(properties.getProperty(HOST), USERNAME, PASSWORD);
			transport.sendMessage(mimeMessage, mimeMessage.getRecipients(Message.RecipientType.TO));
			if(copyto!=null) {
				transport.sendMessage(mimeMessage, mimeMessage.getRecipients(Message.RecipientType.CC));
			}
			if(blindto!=null) {
				transport.sendMessage(mimeMessage, mimeMessage.getRecipients(Message.RecipientType.BCC));
			}
			System.out.println("邮件发送成功");
			transport.close();
			return true;
		} catch (MessagingException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	/**
	 * 将上述方法选择性组合，完成发送
	 * send:普通的一对一发送. <br/>
	 *@param smtp
	 *@param from
	 *@param to
	 *@param subject
	 *@param content
	 *@param username
	 *@param password
	 *@return
	 */
//	public static boolean send(String smtp,String from,String to,String subject,String content,String username,String password) {
	public static boolean send(String smtp,String from,String to,String subject,String content) {
		SendMail mail=new SendMail(smtp);
		mail.setNeedAuth(true);//需要认证
		if(!mail.setSubject(subject))
			return false;
		if(!mail.setBody(content))
			return false;
		if(!mail.setFrom(from))
			return false;
		if(!mail.setTo(to)) 
			return false;
//		mail.setNamePassword(username, password);
		if(!mail.sendOut(null,null))
			return false;
		return true;
	}
	
	/**
	 * 
	 * sendAndCcWithFile:带附件，抄送的邮件. <br/>
	 *@param smtp
	 *@param from
	 *@param to
	 *@param subject
	 *@param content
	 *@param username
	 *@param password
	 *@param copyto
	 *@param filename
	 */
	public static boolean sendAndCcWithFile(String smtp,String from,String to,String subject,String content,String copyto,String filename) {
		SendMail mail=new SendMail(smtp);
		mail.setNeedAuth(true);//需要认证
		if(!mail.setSubject(subject)) {
			return false;
		}
		if(!mail.setBody(content)) {
			return false;
		}
		if(!mail.addFileAffix(filename)) {
			return false;
		}
		if(!mail.setFrom(from)) {
			return false;
		}
		if(!mail.setTo(to)) {
			return false;
		}
		if(!mail.setCopyTo(copyto)) {
			return false;
		}
//		mail.setNamePassword(username, password);
		if(!mail.sendOut(copyto,null)) {
			return false;
		}
		return true;
	}
	
}
