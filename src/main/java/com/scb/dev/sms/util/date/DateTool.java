/**
 * Project Name:clps.sms
 * File Name:DateTool.java
 * Package Name:com.clps.dev.sms.util
 * Date:2018年10月31日下午1:51:48
 * Copyright (c) 2018, gael.zhu@qq.com All Rights Reserved.
 *
 */
package com.scb.dev.sms.util.date;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.scb.dev.sms.common.CommonData;
/**
 * 
 * ClassName: DateTool 
 * Description: convert string to Date 
 * 			    convert date to String	
 * 
 * @author gael.zhu
 * @version V1.0
 * @since JDK 1.8	
 */
public class DateTool {
	
	
	private SimpleDateFormat simpleDateFormat = null;   
	private Date date=new Date();

	public Date getTime() {
		try {
			return StringToDate(DateToString(this.date,CommonData.YYYY_MM_DD_HH_MM_24),CommonData.YYYY_MM_DD_HH_MM_24);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	/**
	 *
	 * @param string ,pattern
	 * @return date
	 * @throws Exception
	 * convert String value to Date format
	 * 
	 */
	public Date StringToDate(String string,String pattern) throws Exception {
		this.simpleDateFormat = new SimpleDateFormat(pattern);
		Date date = simpleDateFormat.parse(string);
		return date;
	}
	
	/**
	 * @param date ,pattern
	 * @return string in the form of yyyy-MM-dd HH:mm:ss.
	 * convert Date value to String format
	 * 
	 */
	public String DateToString(Date date,String pattern) {
		this.simpleDateFormat = new SimpleDateFormat(pattern);
		String string = simpleDateFormat.format(date);
		return string;
	}
}
