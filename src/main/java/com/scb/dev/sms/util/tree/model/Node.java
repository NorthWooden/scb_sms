/**
 * Project Name:scb_sms
 * File Name:Node.java
 * Package Name:com.scb.dev.sms.util.tree.model
 * Date:2018年11月7日上午8:56:59
 * Copyright (c) 2018, gael.zhu@qq.com All Rights Reserved.
 *
 */
package com.scb.dev.sms.util.tree.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.scb.dev.sms.sm.vo.MenuVO;

/**
 * ClassName: Node <br/>
 * Description: Tree model. <br/>
 * <br/>
 * date: 2018年11月7日 上午8:56:59 <br/>
 *
 * @author gael.zhu
 * @version V1.0
 * @since JDK 1.8
 */
public class Node implements Serializable {

	private static final long serialVersionUID = 1456897049394658725L;
	private MenuVO menuvo;                //菜单信息
	private List<Node> children;          //子菜单

	/**
	 * Creates a new instance of Node.
	 *
	 */

	public Node() {
		super();
		this.menuvo=new MenuVO();
		this.children = new ArrayList<Node>();
	}

	/**
	 * menuvo.
	 *
	 * @return  the menuvo
	 */
	public final MenuVO getMenuvo() {
		return menuvo;
	}

	/**
	 * menuvo.
	 *
	 * @param   menuvo    the menuvo to set
	 */
	public final void setMenuvo(MenuVO menuvo) {
		this.menuvo = menuvo;
	}

	/**
	 * children.
	 *
	 * @return  the children
	 */
	public final List<Node> getChildren() {
		return children;
	}

	/**
	 * children.
	 *
	 * @param   children    the children to set
	 */
	public final void setChildren(List<Node> children) {
		this.children = children;
	}
	
}
