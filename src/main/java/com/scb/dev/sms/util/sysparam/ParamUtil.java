/**
 * Project Name:Test
 * File Name:ResourceUtil.java
 * Package Name:com.clps.test
 * Date:2018年11月8日下午2:08:24
 * Copyright (c) 2018, erwin.wang@clpsglobal.com All Rights Reserved.
 *
 */
package com.scb.dev.sms.util.sysparam;

import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

/**
 * ClassName: ResourceUtil <br/>
 * Description:  <br/><br/>
 * date: 2018年11月8日 下午2:08:24 <br/>
 *
 * @author Cristal.Xu
 * @version V1.0
 * @since JDK 1.8
 */
public class ParamUtil {
	public static final ResourceBundle bundle=java.util.ResourceBundle.getBundle("resource");
	public static final Map<String, String> map = new HashMap<String, String>();
	/** 
	* @return 获得session信息
	*/
	public String getSessionInfo() {
		return bundle.getString("sessionInfoName");
		
	}
	
	/** 
	* @return 获得上传Ip地址
	*/
	public  String getIpAddr() {
		return bundle.getString("getIpAddr");
		
	}
	
	/** 
	* @return 获得上传文件的名字
	*/
	public  String getUploadFileName() {
		return bundle.getString("uploadfileName");
	}
	/** 
	* @return 获得上传文件的路径
	*/
	public   String getUploadDirectory() {
		return bundle.getString("uploadDerictory");
	}
	
	/** 
	* @return 获得上传头像名称
	*/
	 
	public  String getUploadphotoName() {
		return bundle.getString("uploadfileName");
	}
	/** 
	* @return 获得上传表单域的名称
	*/
	public  String getUploadFieldName() {
	return bundle.getString("uploadFieldName");
	}


	/** 
	* @return 获得上传logo路径
	*/
	public   String getuploadlogoName(){
	return bundle.getString("uploadlogoName");
	}

	/** 
	* @return 获得上传图片路径
	*/
	public   String getuploadScrollPicName(){
	return bundle.getString("uploadScrollPicName");
	}


	/** 
	* @return  获得上传路径
	*/
	public   String getuploadProductName(){
	return bundle.getString("uploadProductName");
	}

	/** 
	* @return 获得上传文件的最大大小限制
	*/
	public  long getUploadFileMaxSize() {
	return Long.valueOf(bundle.getString("uploadFileMaxSize"));
	}


	/** 
	* @return 获得允许上传文件的扩展名
	*/
	public   String getUploadFileExts() {
	return bundle.getString("uploadFileExts");
	}


	/** 
	* @return 文件上传文件的扩展名
	*/
	public  String getUploadFile() {
	return bundle.getString("uploadFile");
	}

	/** 
	* @return 图片上传文件的扩展名
	*/
	public  String getUploalFilePhoto() {
	return bundle.getString("uploalFilePhoto");
	}
 

	/** 
	* @return 获得邮箱信息
	*/
	public   String getEmail() {
	return bundle.getString("email");
	}

	/** 
	* @return 获得日志
	*/
	public   String getlog(String log) {
	return bundle.getString(log);
	}

	
	 
}
