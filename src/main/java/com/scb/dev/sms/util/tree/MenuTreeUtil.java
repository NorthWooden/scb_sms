/**
 * Project Name:scb_sms
 * File Name:MenuTreeUtil.java
 * Package Name:com.scb.dev.sms.util.tree
 * Date:2018年11月7日上午8:56:04
 * Copyright (c) 2018, gael.zhu@qq.com All Rights Reserved.
 *
 */
package com.scb.dev.sms.util.tree;

import java.util.ArrayList;
import java.util.List;

import com.scb.dev.sms.sm.vo.MenuVO;
import com.scb.dev.sms.util.tree.model.Node;

/**
 * ClassName: MenuTreeUtil <br/>
 * Description: Tree Factory. <br/><br/>
 * date: 2018年11月7日 上午8:56:04 <br/>
 *
 * @author gael.zhu
 * @version V1.0
 * @since JDK 1.8
 */
public class MenuTreeUtil {
	 
    private Node root = new Node();                               //新节点
    private List<Node> tree = new ArrayList<Node>();              //菜单树
    private List<Node> removeList = new ArrayList<Node>();        //移除树
 
    /**
     * 
    	 * @desciption to make a righr tree. <br/>     
    	 * @param   leaf should be assigned :Node
    	 * @param   tree:List<Node>
    	 * @return  tree :List<Node>
     */
    public List<Node> findChild(List<Node> tree,Node leaf){
    	if(leaf.getMenuvo().getMenuPid()==leaf.getMenuvo().getMenuId()) {
    		return tree;
    	}
    	else {
    		for(Node pLeaf:tree) {
    			if(leaf.getMenuvo().getMenuPid()==pLeaf.getMenuvo().getMenuId()) {
    				pLeaf.getChildren().add(leaf);
    				this.removeList.add(leaf);
    				break;
    			}
    		}
    	}
        return tree;
    }
    
    /**
     * 
    	 * @desciption get menu and deal with it. <br/>     
    	 * @param the menu :List<MenuVO>
    	 * @return the forest  :List<Node>
     */
    public List<Node> menuList(List<MenuVO> menu){
        for(MenuVO menuVo:menu) {
        	this.root=new Node();
        	this.root.setMenuvo(menuVo);
        	this.tree.add(root);
        }
        for(Node leaf:this.tree) {
        	this.tree=findChild(this.tree,leaf);
        }
        this.tree.removeAll(removeList);
        return this.tree;      
    }
}
