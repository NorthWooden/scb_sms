/**
 * Project Name:scb_sms
 * File Name:MailConfig.java
 * Package Name:com.scb.dev.sms.util.mail
 * Date:2018年11月8日下午3:50:15
 * Copyright (c) 2018, Annie.Jiang@clps.com All Rights Reserved.
 *
 */
package com.scb.dev.sms.util.mail;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * ClassName: MailConfig <br/>
 * date: 2018年11月8日 下午3:50:15 <br/>
 *
 * @author Annie.Jiang
 * @version V1.0
 * @since JDK 1.8
 */
public class MailConfig {

	private static final String PROPERTIES_DEFAULT="mailConfig.properties";
	public static String protocol;  //smtp的协议
	public static String protocolP;  //POP3的协议
	public static String host;      //smtp的服务器
	public static String hostP;     //POP3的服务器
	public static String username;
	public static String password;
	public static String defaultEncoding;
	public static String auth;
	public static String timeout;
	public static String contentEncoding;
	
	public static Properties properties;
	
	static {
		init();
	}
	
	/**
	 * 
	 * init:.初始变量 <br/>
	 *
	 */
	private static void init() {
		properties=new Properties();
		InputStream in=null;
		try {
			 in=MailConfig.class.getClassLoader().getResourceAsStream(PROPERTIES_DEFAULT);
			properties.load(in);
//			inputStream.close();
			protocol=properties.getProperty("mail.transport.protocol");
			protocolP=properties.getProperty("mail.store.protocol");
			host=properties.getProperty("mail.smtp.host");
			hostP=properties.getProperty("mail.pop3.host");
			username=properties.getProperty("mail.username");
			password=properties.getProperty("mail.password");
			defaultEncoding=properties.getProperty("mail.defaultEncoding");
			auth=properties.getProperty("mail.smtp.auth");
			timeout=properties.getProperty("mail.smtp.timeout");
			contentEncoding=properties.getProperty("mail.contentEncoding");
		} catch (IOException e) {
			e.printStackTrace();
		}finally {
			try {
				in.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
	}
}
