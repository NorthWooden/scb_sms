/**
 * Project Name:scb_sms
 * File Name:ValidationTool.java
 * Package Name:com.scb.dev.sms.util.validation
 * Date:2018年11月1日下午1:14:11
 * Copyright (c) 2018, gael.zhu@qq.com All Rights Reserved.
 *
 */
package com.scb.dev.sms.util.validation;

import java.util.regex.Pattern;

import com.scb.dev.sms.common.CommonData;

/**
 * ClassName: ValidationTool <br/>
 * Description: Do Validation Operation(email,number). <br/>
 * <br/>
 * date: 2018年11月1日 下午1:14:11 <br/>
 *
 * @author gael.zhu
 * @version V1.0
 * @since JDK 1.8
 */
public class ValidationTool {
	
	private Pattern pattern = Pattern.compile(CommonData.EMAIL_WHETHERLEAGAL);   //正则表达式
	/**
	 * 
		 * @desciption Email判断. <br/>     
		 * @param email String
		 * @return result String
	 */
	public String EmailCheckTool(String email) {
		if (!email.contains(CommonData.EMAIL_SIGN)) {
			return CommonData.EMAIL_NOEMAIL;
		} else {
			String[] splitEmail = email.split(CommonData.EMAIL_SIGN);
			if(!splitEmail[splitEmail.length-1].equals(CommonData.CLPS)) {
				return CommonData.EMAIL_NOEMAIL;
			}
			else if(!WhetherLegal(splitEmail[0])){
				return CommonData.EMAIL_NOEMAIL;
			}
			else
				return CommonData.EMAIL_AEMAIL;
		}
	}

	/**
	 * 
		 * @desciption 判断Email前半段是否合法 <br/>     
		 * @param emailText String
		 * @return 是否合法 boolean
	 */
	private boolean WhetherLegal(String emailText) {
		if((emailText.length()<CommonData.EMAIL_MIN)||(emailText.length()>CommonData.EMAIL_MAX))
			return false;
		else if(this.pattern.matcher(emailText).matches()) {
			return true;
		}
		else
			return false;
	}
	
}
