package com.scb.dev.sms.sm.service.impl;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Date;

import javax.annotation.Resource;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.scb.dev.sms.common.CommonData;
import com.scb.dev.sms.sm.pojo.Employee;
import com.scb.dev.sms.sm.service.IEmployeeService;
import com.scb.dev.sms.util.factory.ToolFactory;
import com.scb.dev.sms.util.pagination.Page;
/**
 * ClassName: EmployeeServiceImplTest <br/>
 * date: 2018年11月20日 上午11:40:09 <br/>
 * @author Vanhom.Peng
 * @version V1.0
 * @since JDK 1.8
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:/applicationContext.xml"})
public class EmployeeServiceImplTest {
	@Resource
    private IEmployeeService employeeService;
    @Before
    public void setUp() throws Exception {
    }
    @After
    public void tearDown() throws Exception {
    }
    /** 
	 *  测试查询所有职员的信息
	 * @see com.scb.dev.sms.sm.service.IEmployeeService#queryAllEmployee()
	 */
    @Test
	public void testqueryAllEmployee() {
		assertNotNull(employeeService.queryAllEmployee());
	}
    /** 
	 *  测试根据职员ID查询职员信息
	 * @see com.scb.dev.sms.sm.service.IEmployeeService#queryEmployeeById()
	 */
    @Test
	public void testqueryEmployeeById() throws Exception{
		assertNotNull(employeeService.queryEmployeeById("3d46445e5b87420dad6c9b96e64236fb"));
	}
    /** 
	 *  测试根据部门ID查询职员信息
	 * @see com.scb.dev.sms.sm.service.IEmployeeService#queryEmployeeByDepartmentId()
	 */
    @Test
	public void testqueryEmployeeByDepartmentId() throws Exception{
		assertNotNull(employeeService.queryEmployeeByDepartmentId(2));
	}
    /** 
	 *  测试根据职位ID查询职员的信息
	 * @see com.scb.dev.sms.sm.service.IEmployeeService#queryEmployeeByPositionId()
	 */
    @Test
	public void testqueryEmployeeByPositionId() throws Exception{
		assertNotNull(employeeService.queryEmployeeByPositionId(1));
	}
	/**
	 * 
	 * testaddEmployee 测试添加职员信息
	 */
	@Test
	public void testaddEmployee() {
		Employee emp=new Employee();
		emp.setEmployeeName("小明");
		emp.setEmployeeGender("男");
		emp.setEmployeeAddress("广州市");
		emp.setEmployeeChangedTime(new Date());
		emp.setEmployeeChangerId("admin");
		emp.setEmployeeCreatedTime(new Date());
		emp.setEmployeeCreatorId("admin");
		emp.setEmployeeDepartmentId(11);
		emp.setEmployeeEmail("Ming.Xiao@clpsglobal.com");
		emp.setEmployeeEnglishname("zhangsan");
		emp.setEmployeePhoneNumber("15201013698");
		emp.setEmployeePositionId(1001);
		emp.setEmployeeState("在职");
		emp.setEmployeeTel("8648789");
		try {
			emp.setEmployeeBorn(ToolFactory.getInstanceOfDateTool().StringToDate("1995-09-06", "yyyy-MM-dd"));
			emp.setEmployeeInductionTime(ToolFactory.getInstanceOfDateTool().StringToDate("2011-09-06", "yyyy-MM-dd"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		String mark=employeeService.addNewEmployee(emp);
		assertEquals(CommonData.SAVE_SUCCESS, mark);
		assertNotEquals(CommonData.SAVE_FAILURE, mark);
		
	}
	/**
	 * 
	 * testUpdateEmployeeInfo 测试修改职员信息
	 */
	@Test
	public void testUpdateEmployeeInfo() {
		Employee emp=new Employee();
		emp.setEmployeeId("a9c0d7d328ec42a5a7472773ddfb62e6");
		emp.setEmployeeName("张三");
		emp.setEmployeeGender("男");
		emp.setEmployeeAddress("深圳市");
		emp.setEmployeeChangedTime(new Date());
		emp.setEmployeeChangerId("admin");
		emp.setEmployeeCreatedTime(new Date());
		emp.setEmployeeCreatorId("admin");
		emp.setEmployeeDepartmentId(11);
		emp.setEmployeeEmail("San.Zhang@clpsglobal.com");
		emp.setEmployeeEnglishname("zhangsan");
		emp.setEmployeePhoneNumber("15070123698");
		emp.setEmployeePositionId(1001);
		emp.setEmployeeState("在职");
		emp.setEmployeeTel("8645789");
		try {
			emp.setEmployeeBorn(ToolFactory.getInstanceOfDateTool().StringToDate("1995-09-06", "yyyy-MM-dd"));
			emp.setEmployeeInductionTime(ToolFactory.getInstanceOfDateTool().StringToDate("2018-09-06", "yyyy-MM-dd"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		assertEquals(CommonData.UPDATE_SUCCESS, employeeService.modifyEmployeeInfo(emp));
		
	}
	/**
	 * 
	 * testDeleteEmployee 测试删除职员信息
	 */
	@Test
	public void testDeleteEmployee() {
		Employee emp=new Employee();
		emp.setEmployeeId("a09d39d0137a4517914bf66cc80d71e1");
		Employee e=employeeService.queryEmployeeById(emp.getEmployeeId());
		if(e!=null) {
			assertEquals(CommonData.DELETE_SUCCESS, employeeService.removeEmployee(e));
		}else {
			assertEquals(CommonData.QUERY_FAILURE, e);
		}
		
	}
	@Test
	public void testSelectByPage() {
		Page page=new Page("0","5");
		assertNotNull(employeeService.findByPage(page));
		
	}
	
    
}