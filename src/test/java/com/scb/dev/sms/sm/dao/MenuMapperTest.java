/**
 * Project Name:scb_sms
 * File Name:MenuMapperTest.java
 * Package Name:com.scb.dev.sms.sm.dao
 * Date:2018年11月14日下午2:21:02
 * Copyright (c) 2018, gael.zhu@qq.com All Rights Reserved.
 *
 */
package com.scb.dev.sms.sm.dao;

import static org.junit.Assert.assertEquals;

import java.util.Date;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.scb.dev.sms.sm.pojo.Menu;

/**
 * ClassName: MenuMapperTest <br/>
 * Description: MenuDao test. <br/><br/>
 * date: 2018年11月14日 下午2:21:02 <br/>
 * 
 * @author gael.zhu
 * @version V1.0
 * @since JDK 1.8
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:/applicationContext.xml"})
public class MenuMapperTest {

	@Autowired
	private MenuMapper menuMapper;
	@Autowired
	private Menu menu;
	/**  
	 * @param
	 * @return
	 */
	@Before
	public void setUp() throws Exception {
		this.menu.setMenuChangedTime(new Date());
		this.menu.setMenuCreatedTime(new Date());
	}

	/**   
	 * @param
	 * @return
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link com.scb.dev.sms.sm.dao.MenuMapper#deleteByPrimaryKey(java.lang.Integer)}.
	 */
	@Test
	public void testDeleteByPrimaryKey() {
		assertEquals(1,this.menuMapper.deleteByPrimaryKey(1));
	}

	/**
	 * Test method for {@link com.scb.dev.sms.sm.dao.MenuMapper#insertSelective(com.scb.dev.sms.sm.pojo.Menu)}.
	 */
	@Test
	public void testInsertSelective() {
		this.menu.setMenuName("添加部门");
		this.menu.setMenuPid(2);
		this.menu.setMenuDescripition("部门管理操作");
		this.menu.setMenuLevel("1");
		this.menu.setMenuOrder("1");
		this.menu.setMenuCreatedTime(new Date());
		this.menu.setMenuChangedTime(new Date());
		this.menu.setMenuCreatorId("1512480230");
		this.menu.setMenuChangerId("1512480230");
		this.menu.setMenuUrl("www.qq.com");
		
		assertEquals(1,this.menuMapper.insertSelective(menu));
		this.menu.setMenuId(1);
		assertEquals(1,this.menuMapper.insertSelective(menu));
	}

	/**
	 * Test method for {@link com.scb.dev.sms.sm.dao.MenuMapper#selectByPrimaryKey(java.lang.Integer)}.
	 */
	@Test
	public void testSelectById() {
		assertEquals("用户信息",this.menuMapper.selectById(1).getMenuName());
		assertEquals("部分管理",this.menuMapper.selectById(1).getMenuName());
	}

	/**
	 * Test method for {@link com.scb.dev.sms.sm.dao.MenuMapper#selectByPrimaryKey(java.lang.Integer)}.
	 */
	@Test
	public void testSelectAllMenu() {
		assertEquals(3,this.menuMapper.selectAllMenu().size());
	}
	
	/**
	 * Test method for {@link com.scb.dev.sms.sm.dao.MenuMapper#updateByPrimaryKeySelective(com.scb.dev.sms.sm.pojo.Menu)}.
	 */
	@Test
	public void testUpdateByPrimaryKeySelective() {
		this.menu.setMenuId(1);
		assertEquals(1,this.menuMapper.updateByPrimaryKeySelective(menu));
	}


}
