/**
 * Project Name:scb_sms
 * File Name:PositionServiceImplTest.java
 * Package Name:com.scb.dev.sms.sm.service.impl
 * Date:2018年11月15日下午3:25:36
 * Copyright (c) 2018,Grace.Lee@clps.com All Rights Reserved.
 *
 */
package com.scb.dev.sms.sm.service.impl;

import static org.junit.Assert.*;

import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.scb.dev.sms.common.CommonData;
import com.scb.dev.sms.sm.pojo.Menu;
import com.scb.dev.sms.sm.pojo.Position;
import com.scb.dev.sms.util.pagination.Page;


/**
 * ClassName: PositionServiceImplTest <br/>
 * Description: 
 * date: 2018年11月15日 下午3:25:36 <br/>
 *
 * @author Grace.Lee
 * @version V1.0
 * @since JDK 1.8
 */
@ContextConfiguration({"classpath:/applicationContext.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class PositionServiceImplTest {

//	private Logger logger=Logger.getLogger(this.getClass());
	
	@Autowired
	private PositionServiceImpl positionServiceimpl;
	
	/**
	 * setUp
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}


	
	/**
	 * 
	 * queryAllPositioin 测试查询所有职位信息
	 */
	@Test
	public void testqueryAllPositioin() {
		
		assertNotNull(positionServiceimpl.queryAllPosititon());
	}
	
	
	/**
	 * 
	 * qeuryMenuByPosition 测试通过职位名称查询拥有的菜单
	 */
	@Test
	public void testqeuryMenuByPosition() {
		Position position=new Position();
		position.setPositionName("系统管理员");
		List<Menu> list=positionServiceimpl.qeuryMenuByPosition(position.getPositionId());
		assertNotNull(list);
		
	}
	
	/**
	 * 
	 * testUpdatePositionInfo 测试修改职位信息
	 */
	@Test
	public void testUpdatePositionInfo() {
		Position position=new Position();
		position.setPositionId(10030);
		position.setPositionName("人力资源助理");
		position.setPositionChangerId("admin");
		position.setPositionChangedTime(new Date());
		assertEquals(CommonData.UPDATE_SUCCESS, positionServiceimpl.modifyPositionInfo(position));
	}
	
	/**
	 * 
	 * testDeletePosition 测试删除职位信息
	 */
	@Test
	public void testDeletePosition() {
		Position position=new Position();
		position.setPositionId(10012);
		position.setPositionName("业务员");
		Position pos=positionServiceimpl.queryPositionById(position.getPositionId());
		if(pos!=null) {
			assertEquals(CommonData.DELETE_SUCCESS, positionServiceimpl.removePosition(position));
		}else {
			assertEquals(CommonData.QUERY_FAILURE, pos);
		}
	}
	
	@Test
	public void testSelectByPage() {
		Page page=new Page("1","5");
		assertNotNull(positionServiceimpl.findByPage(page));
		
	}

}
