/**
 * Project Name:scb_sms
 * File Name:AccountServiceImplTest.java
 * Package Name:com.scb.dev.sms.sm.service.impl
 * Date:2018年11月22日上午10:52:45
 * Copyright (c) 2018, gael.zhu@qq.com All Rights Reserved.
 *
 */
package com.scb.dev.sms.sm.service.impl;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.scb.dev.sms.sm.pojo.Account;
import com.scb.dev.sms.sm.service.IAccountService;

/**
 * ClassName: AccountServiceImplTest <br/>
 * Description: Account Service Test. <br/><br/>
 * date: 2018年11月22日 上午10:52:45 <br/>
 *
 * @author gael.zhu
 * @version V1.0
 * @since JDK 1.8
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:/applicationContext.xml"})
public class AccountServiceImplTest {

	@Autowired
	private IAccountService accountService;
	@Autowired
	private Account account;
	@Before
	public void setUp() throws Exception {
		this.account.setAccountName("154876564");
		this.account.setAccountPwd("luoke456j");
		this.account.setAccountChangerId("56156156");
	}

	/**
	 * Test method for {@link com.scb.dev.sms.sm.service.impl.AccountServiceImpl#registerAccount(com.scb.dev.sms.sm.pojo.Account)}.
	 */
	@Test
	public void testRegisterAccount() {
		assertEquals("001-001", this.accountService.registerAccount(this.account));
	}

	/**
	 * Test method for {@link com.scb.dev.sms.sm.service.impl.AccountServiceImpl#deleteAccount(java.lang.String)}.
	 */
	@Test
	public void testDeleteAccount() {
		assertEquals("003-001", this.accountService.deleteAccount("508E3C94F8F113DA10BFE90FBE6B22E4"));
	}

	/**
	 * Test method for {@link com.scb.dev.sms.sm.service.impl.AccountServiceImpl#updateAccount(com.scb.dev.sms.sm.pojo.Account)}.
	 */
	@Test
	public void testUpdateAccount() {
		this.account.setAccountId("508E3C94F8F113DA10BFE90FBE6B22E4");
		assertEquals("002-001", this.accountService.updateAccount(this.account));
	}

	/**
	 * Test method for {@link com.scb.dev.sms.sm.service.impl.AccountServiceImpl#selectAccountById(java.lang.String)}.
	 */
	@Test
	public void testSelectAccountById() {
		assertEquals("154876564", this.accountService.selectAccountById("508E3C94F8F113DA10BFE90FBE6B22E4").getAccountName());
	}

	/**
	 * Test method for {@link com.scb.dev.sms.sm.service.impl.AccountServiceImpl#selectAllAccount()}.
	 */
	@Test
	public void testSelectAllAccount() {
		assertEquals(2, this.accountService.selectAllAccount().size());
	}

}
