/**
 * Project Name:scb_sms
 * File Name:MenuServiceImplTest.java
 * Package Name:com.scb.dev.sms.sm.service.impl
 * Date:2018年11月16日上午9:10:11
 * Copyright (c) 2018, gael.zhu@qq.com All Rights Reserved.
 *
 */
package com.scb.dev.sms.sm.service.impl;

import static org.junit.Assert.*;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.scb.dev.sms.sm.pojo.Menu;
import com.scb.dev.sms.sm.service.IMenuService;

/**
 * ClassName: MenuServiceImplTest <br/>
 * Description: Menu Service Test. <br/><br/>
 * date: 2018年11月16日 上午9:10:11 <br/>
 *
 * @author gael.zhu
 * @version V1.0
 * @since JDK 1.8
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:/applicationContext.xml"})
public class MenuServiceImplTest {

	@Autowired
	private IMenuService menuService;
	@Autowired
	private Menu menu;
	/**    
	 * @param
	 * @return
	 */
	@Before
	public void setUp() throws Exception {
		this.menu.setMenuChangedTime(new Date());
		this.menu.setMenuCreatedTime(new Date());
	}

	/**
	 * Test method for {@link com.scb.dev.sms.sm.service.impl.MenuServiceImpl#getAllMenu()}.
	 */
	@Test
	public void testGetAllMenu() {
		System.out.println(this.menuService.getAllMenu());
	}

	/**
	 * Test method for {@link com.scb.dev.sms.sm.service.impl.MenuServiceImpl#insertMenu(com.scb.dev.sms.sm.pojo.Menu)}.
	 */
	@Test
	public void testInsertMenu() {
		this.menu.setMenuName("提出反馈");
		this.menu.setMenuPid(1);
		this.menu.setMenuDescripition("提出反馈操作");
		this.menu.setMenuLevel("1");
		this.menu.setMenuOrder("3");
		this.menu.setMenuCreatedTime(new Date());
		this.menu.setMenuChangedTime(new Date());
		this.menu.setMenuCreatorId("1512480230");
		this.menu.setMenuChangerId("1512480230");
		this.menu.setMenuUrl("www.Alibaba.com");
		assertEquals("001-001", this.menuService.insertMenu(this.menu));
	}

	/**
	 * Test method for {@link com.scb.dev.sms.sm.service.impl.MenuServiceImpl#updateMenu(com.scb.dev.sms.sm.pojo.Menu)}.
	 */
	@Test
	public void testUpdateMenu() {
		this.menu.setMenuId(1);
		assertEquals("002-001", this.menuService.updateMenu(this.menu));
	}

	/**
	 * Test method for {@link com.scb.dev.sms.sm.service.impl.MenuServiceImpl#deleteMenuById(int)}.
	 */
	@Test
	public void testDeleteMenuById() {
		assertEquals("003-001", this.menuService.deleteMenuById(1));
	}

	/**
	 * Test method for {@link com.scb.dev.sms.sm.service.impl.MenuServiceImpl#deleteMenuById(int)}.
	 */
	@Test
	public void testgetMenuByLevel() {
		System.out.println(this.menuService.getMenuByLevel("1"));
	}
}
