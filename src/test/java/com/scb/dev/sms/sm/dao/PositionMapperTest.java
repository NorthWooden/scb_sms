/**
 * Project Name:scb_sms
 * File Name:PositionMapperTest.java
 * Package Name:com.scb.dev.sms.sm.mapper
 * Date:2018年11月14日下午2:39:33
 * Copyright (c) 2018,Grace.Lee@clps.com All Rights Reserved.
 *
 */
package com.scb.dev.sms.sm.dao;

import static org.junit.Assert.*;

import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import com.scb.dev.sms.sm.pojo.Position;



/**
 * ClassName: PositionMapperTest <br/>
 * Description: 
 * date: 2018年11月14日 下午2:39:33 <br/>
 *
 * @author Grace.Lee
 * @version V1.0
 * @since JDK 1.8
 */
@ContextConfiguration(locations = { "classpath:applicationContext.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
public class PositionMapperTest {
	
	@Autowired
	private PositionMapper mapper;
	
	
	/**
	 * setUp
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * tearDown
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * 
	 * testInsert 测试Insert接口
	 * @throws Exception
	 */
	@Test
	public void testInsert() throws Exception {

		Position pos=new Position();
		pos.setPositionId(10010);
		pos.setPositionName("项目经理");
		pos.setPositionCreatorId("admin");
		pos.setPositionCreatedTime(new Date());	
		assertEquals(1,mapper.insert(pos));
		

	}
	/**
	 * 
	 * testInsertSelective 
	 * @throws Exception
	 */
	@Test
	public void testInsertSelective() throws Exception{
		
		Position pos=new Position();
		pos.setPositionId(10023);
		pos.setPositionName("前端开发工程师");
		pos.setPositionCreatorId("admin");
		pos.setPositionCreatedTime(new Date());
		assertEquals(1,mapper.insertSelective(pos));
		
	}
	
	/**
	 * 
	 * testSelectByPrimaryKey 
	 * @throws Exception
	 */
	@Test
	public void testSelectByPrimaryKey() throws Exception{

		
		assertNotNull(mapper.selectByPrimaryKey(10010));

	}
	
	/**
	 * 
	 * testDeleteByPrimaryKey
	 * @throws Exception
	 */
	@Test
	public void testDeleteByPrimaryKey() throws Exception{
		
		assertEquals(1,mapper.deleteByPrimaryKey(10010));
	}

	/**
	 * 
	 * testUpdateByPrimaryKey
	 * @throws Exception
	 */
	@Test
	public void testUpdateByPrimaryKey() throws Exception{
		
		Position pos=new Position();
		pos.setPositionId(10030);
		pos.setPositionName("人力资源主管");
		pos.setPositionChangerId("admin");
		pos.setPositionChangedTime(new Date());
		int l=mapper.updateByPrimaryKey(pos);
		assertEquals(1,l);
		
		
	}
	
	/**
	 * 
	 * testSelectAllPositionInfo  
	 * @throws Exception
	 */
	
	@Test
	public void testSelectAllPositionInfo() throws Exception{
		List<Position> listPosition=mapper.selectAllPositionInfo();
		
		
		assertNotNull(listPosition);
	}
}
