/**
 * Project Name:scb_sms
 * File Name:AccountMapperTest.java
 * Package Name:com.scb.dev.sms.sm.dao
 * Date:2018年11月22日上午9:47:09
 * Copyright (c) 2018, gael.zhu@qq.com All Rights Reserved.
 *
 */
package com.scb.dev.sms.sm.dao;

import static org.junit.Assert.*;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.scb.dev.sms.sm.pojo.Account;

/**
 * ClassName: AccountMapperTest <br/>
 * Description: Account Test. <br/><br/>
 * date: 2018年11月22日 上午9:47:09 <br/>
 *
 * @author gael.zhu
 * @version V1.0
 * @since JDK 1.8
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:/applicationContext.xml"})
public class AccountMapperTest {

	@Autowired
	private AccountMapper accountMapper;
	@Autowired
	private Account account;
	@Before
	public void setUp() throws Exception {
		this.account.setAccountChangedTime(new Date());
		this.account.setAccountChangerId("12345678123456781234567812345678");
		this.account.setAccountId("12345678123456781234567812345678");
		this.account.setAccountName("1512480230");
		this.account.setAccountPwd("luodawda");
	}

	/**
	 * Test method for {@link com.scb.dev.sms.sm.dao.AccountMapper#deleteByPrimaryKey(java.lang.String)}.
	 */
	@Test
	public void testDeleteByPrimaryKey() {
		assertEquals(1, this.accountMapper.deleteByPrimaryKey(account.getAccountId()));
	}

	/**
	 * Test method for {@link com.scb.dev.sms.sm.dao.AccountMapper#insertSelective(com.scb.dev.sms.sm.pojo.Account)}.
	 */
	@Test
	public void testInsertSelective() {
		assertEquals(1, this.accountMapper.insertSelective(account));
	}

	/**
	 * Test method for {@link com.scb.dev.sms.sm.dao.AccountMapper#selectByPrimaryKey(java.lang.String)}.
	 */
	@Test
	public void testSelectByPrimaryKey() {
		this.account.setAccountInvisible(0);
		assertEquals(account, this.accountMapper.selectByPrimaryKey(account.getAccountId()));
	}

	/**
	 * Test method for {@link com.scb.dev.sms.sm.dao.AccountMapper#updateByPrimaryKeySelective(com.scb.dev.sms.sm.pojo.Account)}.
	 */
	@Test
	public void testUpdateByPrimaryKeySelective() {
		this.account.setAccountName("mybatis");
		this.account.setAccountId(account.getAccountId());
		assertEquals(1, this.accountMapper.updateByPrimaryKeySelective(account));
	}

}
