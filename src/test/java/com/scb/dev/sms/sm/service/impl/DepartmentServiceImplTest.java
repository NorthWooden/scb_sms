package com.scb.dev.sms.sm.service.impl;

import static org.junit.Assert.*;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.scb.dev.sms.common.CommonData;
import com.scb.dev.sms.sm.pojo.Department;
import com.scb.dev.sms.sm.service.IDepartmentService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:/applicationContext.xml"})
public class DepartmentServiceImplTest {

	Logger logger=LogManager.getFormatterLogger(this.getClass());
	Department department=null;
	@Resource
	IDepartmentService departmentService;
	@Before
	public void setUp() throws Exception {
		department=new Department();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testFindAllDepartment() {
		List<Department> dlist=departmentService.findAllDepartment();
		for (Department department : dlist) {
			logger.debug(department.getDepartmentName());
		}
		assertEquals(2, dlist.size());
	}
	
	@Test
	public void testAddNewDepartment() {
		department.setDepartmentName("NEW");
		department.setDepartmentLeader("3");
		department.setDepartmentPid(0);
		department.setDepartmentCreatorId("1");
		department.setDepartmentCreatedTime(new Date());
		String mark=departmentService.addNewDepartment(department);
		assertEquals(CommonData.SAVE_SUCCESS, mark);
		assertNotEquals(CommonData.SAVE_FAILURE, mark);
	}
	
	@Test
	public void testDeleteDepartment() {
		department.setDepartmentName("NEO1");
		department.setDepartmentLeader("3");
		department.setDepartmentPid(0);
		department.setDepartmentCreatorId("1");
		department.setDepartmentCreatedTime(new Date());
		departmentService.addNewDepartment(department);
		Integer pid=departmentService.findOneDepartmentByName("NEO1").getDepartmentId();
		department=new Department();
		department.setDepartmentName("NEO2");
		department.setDepartmentLeader("3");
		department.setDepartmentPid(pid);
		department.setDepartmentCreatorId("1");
		department.setDepartmentCreatedTime(new Date());
		departmentService.addNewDepartment(department);
		department=new Department();
		department.setDepartmentName("NEO3");
		department.setDepartmentLeader("3");
		department.setDepartmentPid(pid);
		department.setDepartmentCreatorId("1");
		department.setDepartmentCreatedTime(new Date());
		departmentService.addNewDepartment(department);
		departmentService.deleteDepartment(departmentService.findOneDepartmentByName("NEO3").getDepartmentId(), true);
		assertEquals(5, departmentService.findAllDepartment().size());
		departmentService.deleteDepartment(pid, false);
		assertEquals(3, departmentService.findAllDepartment().size());
	}
	
	@Test
	public void testUpdateDepartment() {
		department.setDepartmentId(1);
		department.setDepartmentName("SSSSSS");
		department.setDepartmentLeader("55");
		String mark=departmentService.updateDepartment(department);
		assertNotEquals(CommonData.UPDATE_FAILURE, mark);
		assertEquals(CommonData.UPDATE_SUCCESS, mark);
		department.setDepartmentName("RB");
		department.setDepartmentLeader("1");
		departmentService.updateDepartment(department);
	}
	
	@Test
	public void testSelectById() {
		assertEquals("RB", departmentService.findDepartmentById(1).getDepartmentName());
	}
	
	@Test
	public void testFindDepartmentByPartName() {
		assertEquals(1, departmentService.findDepartmentByPartName("R").size());
		assertNotEquals(0, departmentService.findDepartmentByPartName("R").size());
	}

}
