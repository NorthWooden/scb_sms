/**
 * Project Name:scb_sms
 * File Name:PermissionServiceImplTest.java
 * Package Name:com.scb.dev.sms.sm.service.impl
 * Date:2018年11月15日下午6:02:05
 * Copyright (c) 2018, Annie.Jiang@clps.com All Rights Reserved.
 *
 */
package com.scb.dev.sms.sm.service.impl;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.scb.dev.sms.sm.pojo.Permission;
import com.scb.dev.sms.sm.service.IPermissionService;

/**
 * ClassName: PermissionServiceImplTest <br/>
 * Description: 
 * date: 2018年11月15日 下午6:02:05 <br/>
 *
 * @author Annie.Jiang
 * @version V1.0
 * @since JDK 1.8
 */

@ContextConfiguration(locations= {"classpath:/applicationContext.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class PermissionServiceImplTest {

	@Autowired
	private Permission per;
	private IPermissionService perService;
	
	
	/**
	 * setUp:. <br/>
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		per=new Permission();
	}

	/**
	 * tearDown:. <br/>
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link com.scb.dev.sms.sm.service.impl.PermissionServiceImpl#addPermission(com.scb.dev.sms.sm.pojo.Permission)}.
	 * @throws Exception 
	 */
	@Test
	public void testAddPermission() throws Exception {
		per.setPermissionId(1002);
		per.setPermissionName("添加职员");
		per.setPermissionPositionId("CEO");
		per.setPermissionMenuId("操作");
		
		assertEquals(1,perService.addPermission(per));
		assertNotEquals(1,perService.addPermission(per));
	}


	/**
	 * Test method for {@link com.scb.dev.sms.sm.service.impl.PermissionServiceImpl#changePermission(com.scb.dev.sms.sm.pojo.Permission)}.
	 * @throws Exception 
	 */
	@Test
	public void testChangePermission() throws Exception {
		per.setPermissionId(1002);
		per.setPermissionName("添加用户");
		per.setPermissionPositionId("总裁");
		per.setPermissionMenuId("操作");
		
		assertEquals(1,perService.changePermission(per));
		assertNotEquals(1,perService.changePermission(per));
	}

	/**
	 * Test method for {@link com.scb.dev.sms.sm.service.impl.PermissionServiceImpl#deletePermission(int)}.
	 * @throws Exception 
	 */
	@Test
	public void testDeletePermission() throws Exception {
		assertEquals(1,perService.deletePermission(1002));
	}
	
	@Test
	public void testShowPermission() throws Exception{
		per.setPermissionId(1002);
		per.setPermissionName("添加用户");
		per.setPermissionPositionId("CEO");
		per.setPermissionMenuId("操作");
		
//		assertEqulas(per,perService.showPermission(1002));
	}

}
