package com.scb.dev.sms.sm.dao;

import static org.junit.Assert.*;

import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.scb.dev.sms.sm.pojo.Department;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:/applicationContext.xml"})

public class DepartmentMapperTest {
	@Autowired
	DepartmentMapper departmentDAO;
	Department department=null;
	Logger logger = LogManager.getFormatterLogger(this.getClass());
	
	@Before
	public void setup() {
		department=new Department();
	}
	
	@Test
	public void testSelectByPrimaryKey() {
		assertEquals("RB",departmentDAO.selectByPrimaryKey(1).getDepartmentName());
		assertNotEquals("SSS", departmentDAO.selectByPrimaryKey(1).getDepartmentName());
	}
	
	@Test
	public void testSelectAll() {
		List<Department>dlist=departmentDAO.selectAll();
		for (Department department : dlist) {
			logger.debug(department.getDepartmentId()+" "+department.getDepartmentName());
		}
		assertEquals(3, departmentDAO.selectAll().size());
		assertNotEquals(1, departmentDAO.selectAll().size());
	}
	
	@Test
	public void testDeleteByPrimaryKey() {
		assertEquals(1, departmentDAO.deleteByPrimaryKey(1));
		department.setDepartmentId(1);
		department.setDepartmentIsvisible(0);
		departmentDAO.updateByPrimaryKeySelective(department);
	}

	@Test
	public void testInsertSelective() {
		department.setDepartmentName("SCC");
		department.setDepartmentLeader("3");
		department.setDepartmentPid(0);
		department.setDepartmentDescription("kong");
		department.setDepartmentCreatorId("1");
		department.setDepartmentCreatedTime(new Date());
		assertEquals(1, departmentDAO.insertSelective(department));
		departmentDAO.deleteReal(departmentDAO.selectByInfo(department).get(0).getDepartmentId());
	}

	@Test
	public void testUpdateByPrimaryKeySelective() {
		department.setDepartmentId(1);
		department.setDepartmentName("razer");
		assertEquals(1, departmentDAO.updateByPrimaryKeySelective(department));
		assertEquals("razer", departmentDAO.selectByPrimaryKey(1).getDepartmentName());
		assertNotEquals("RB", departmentDAO.selectByPrimaryKey(1).getDepartmentName());
		department.setDepartmentName("RB");
		assertEquals(1, departmentDAO.updateByPrimaryKeySelective(department));
	}
	
	@Test
	public void testSelectByInfo() {
		department.setDepartmentLeader("4fd5f6f11bdb4988990e05a30d2b693e");
		assertEquals(2, departmentDAO.selectByInfo(department).size());
	}
	
	@Test
	public void testSelectByPartName() {
		assertEquals(1, departmentDAO.selectByPartName("B").size());
	}
}
