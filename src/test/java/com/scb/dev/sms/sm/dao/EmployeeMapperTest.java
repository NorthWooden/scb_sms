/**
 * Project Name: scb.sms
 * File Name: EmployeeMapperTest
 * Package Name: com.scb.dev.sms.dao.sm
 * Date: 2018/11/19 4:01 PM
 * Copyright (c) 2018, Vanhom.Peng All Rights Reserved.
 */
package com.scb.dev.sms.sm.dao;
import com.scb.dev.sms.sm.dao.EmployeeMapper;
import com.scb.dev.sms.sm.pojo.Employee;
import com.scb.dev.sms.util.factory.ToolFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
/**
 * 
 * ClassName: EmployeeMapperTest <br/>
 * Description: 职员DAO测试类
 * date: 2018年11月19日 下午4:01:17 <br/>
 *
 * @author Vanhom.Peng
 * @version V1.0
 * @since JDK 1.8
 */
@ContextConfiguration(locations = { "classpath:applicationContext.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
public class EmployeeMapperTest {
    @Autowired
    private EmployeeMapper employeeMapper;

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }
    /** 
	 * testSelectAll:测试SelectAll()方法. <br/>
	 * @throws Exception
	 */
	@Test
	public void testSelectAllEmployeeInfo() throws Exception{
		List<Employee> listEmployee=employeeMapper.selectAllEmployeeInfo();	
		assertNotNull(listEmployee);
	}
	/**
	 * 
	 * testInsert 测试Insert接口
	 * @throws Exception
	 */
	@Test
	public void testInsert() throws Exception {
		Employee emp=new Employee();
		emp.setEmployeeId(UUID.randomUUID().toString().replace("-",""));
		emp.setEmployeeName("张三");
		emp.setEmployeeGender("男");
		emp.setEmployeeAddress("天津市");
		emp.setEmployeeBorn(ToolFactory.getInstanceOfDateTool().StringToDate("1996-11-05", "yyyy-MM-dd"));
		emp.setEmployeeChangedTime(new Date());
		emp.setEmployeeChangerId("admin");
		emp.setEmployeeCreatedTime(new Date());
		emp.setEmployeeCreatorId("admin");
		emp.setEmployeeDepartmentId(11);
		emp.setEmployeeEmail("San.Zhang@clpsglobal.com");
		emp.setEmployeeEnglishname("zhangsan");
		emp.setEmployeeInductionTime(ToolFactory.getInstanceOfDateTool().StringToDate("2018-10-05", "yyyy-MM-dd"));
		emp.setEmployeePhoneNumber("15070123698");
		emp.setEmployeePositionId(1001);
		emp.setEmployeeState("在职");
		emp.setEmployeeTel("8645789");
		assertEquals(1,employeeMapper.insert(emp));		
	}
	/**
	 * testInsertSelective 
	 * @throws Exception
	 */
	@Test
	public void testInsertSelective() throws Exception{
		Employee emp=new Employee();
		emp.setEmployeeId(UUID.randomUUID().toString().replace("-",""));
		emp.setEmployeeName("李四");
		emp.setEmployeeGender("男");
		emp.setEmployeeAddress("北京市");
		emp.setEmployeeBorn(ToolFactory.getInstanceOfDateTool().StringToDate("1995-09-06", "yyyy-MM-dd"));
		emp.setEmployeeCreatedTime(new Date());
		emp.setEmployeeCreatorId("admin");
		emp.setEmployeeDepartmentId(12);
		emp.setEmployeeEmail("Si.Li@clpsglobal.com");
		emp.setEmployeeEnglishname("lisi");
		emp.setEmployeeInductionTime(ToolFactory.getInstanceOfDateTool().StringToDate("2008-09-06", "yyyy-MM-dd"));
		emp.setEmployeePhoneNumber("15075465455");
		emp.setEmployeePositionId(1003);
		emp.setEmployeeState("在职");
		assertEquals(1,employeeMapper.insertSelective(emp));	
	}
	/**
	 * testSelectByPrimaryKey 
	 * @throws Exception
	 */
	@Test
	public void testSelectByPrimaryKey() throws Exception{
		assertNotNull(employeeMapper.selectByPrimaryKey("3d46445e5b87420dad6c9b96e64236fb"));
	}
	/**
	 * 
	 * testselectByDepartmentId
	 * @author Vanhom_Peng
	 * @throws Exception
	 * @since JDK 1.8
	 */
	@Test
	public void testselectByDepartmentId() throws Exception{
		assertNotNull(employeeMapper.selectByDepartmentId(2));
	}
	/**
	 * 
	 * testselectByPositionId
	 * @author Vanhom_Peng
	 * @throws Exception
	 * @since JDK 1.8
	 */
	@Test
	public void testselectByPositionId() throws Exception{
		assertNotNull(employeeMapper.selectByPositionId(1));
	}
	/**
	 * testDeleteByPrimaryKey
	 * @throws Exception
	 */
	@Test
	public void testDeleteByPrimaryKey() throws Exception{	
		assertEquals(1,employeeMapper.deleteByPrimaryKey("ee176db971f1414598f427aebbbd1265"));
	}
	/**
	 * testUpdateByPrimaryKey
	 * @throws Exception
	 */
	@Test
	public void testUpdateByPrimaryKey() throws Exception{		
		Employee emp=new Employee();
		emp.setEmployeeId("790ba3b2a797480084eb983bb62df499");
		emp.setEmployeeName("王五");
		emp.setEmployeeGender("男");
		emp.setEmployeeAddress("上海市");
		emp.setEmployeeBorn(ToolFactory.getInstanceOfDateTool().StringToDate("1980-11-05", "yyyy-MM-dd"));
		emp.setEmployeeChangedTime(new Date());
		emp.setEmployeeChangerId("admin");
		emp.setEmployeeCreatedTime(new Date());
		emp.setEmployeeCreatorId("admin");
		emp.setEmployeeDepartmentId(13);
		emp.setEmployeeEmail("Wu.Wang@clpsglobal.com");
		emp.setEmployeeEnglishname("wangwu");
		emp.setEmployeeInductionTime(ToolFactory.getInstanceOfDateTool().StringToDate("2009-08-06", "yyyy-MM-dd"));
		emp.setEmployeePhoneNumber("15070127896");
		emp.setEmployeePositionId(1003);
		emp.setEmployeeState("在职");
		emp.setEmployeeTel("86457456");
		assertEquals(1,employeeMapper.updateByPrimaryKey(emp));	
	}

}
