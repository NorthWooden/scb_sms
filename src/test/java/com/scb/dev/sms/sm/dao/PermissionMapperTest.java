/**
 * Project Name:scb_sms
 * File Name:PermissionMapperTest.java
 * Package Name:com.scb.dev.sms.sm.dao
 * Date:2018年11月14日下午2:40:21
 * Copyright (c) 2018, Annie.Jiang@clps.com All Rights Reserved.
 *
 */
package com.scb.dev.sms.sm.dao;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.scb.dev.sms.sm.pojo.Permission;

/**
 * ClassName: PermissionMapperTest <br/>
 * Description: PermissionMapperTest的测试类
 * date: 2018年11月14日 下午2:40:21 <br/>
 *
 * @author Annie.Jiang
 * @version V1.0
 * @since JDK 1.8
 */

@ContextConfiguration(locations= {"classpath:/applicationContext.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class PermissionMapperTest {

	@Autowired
	
	private PermissionMapper mapper;
	private Permission per;
	
	/**
	 * setUp:. <br/>
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		 per=new Permission();
	}

	/**
	 * tearDown:. <br/>
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		
	}

	/**
	 * 
	 * testInsert:测试insert()方法. <br/>
	 * @throws Exception
	 */
	@Test
	public void testInsert() throws Exception{
		
		per.setPermissionId(1001);
		per.setPermissionName("添加账户");
		per.setPermissionPositionId("管理员");
		per.setPermissionMenuId("操作");
		
		assertEquals(1,mapper.insert(per));
		assertNotEquals(1,mapper.insert(per));
	}
	
	
	/**
	 * 
	 * testSelectByPrimaryKey:测试selectByPrimaryKey()方法. <br/>
	 * @throws Exception
	 */
	@Test
	public void testSelectByPrimaryKey() throws Exception{
		
		per.setPermissionId(1001);
		per.setPermissionName("添加账户");
		per.setPermissionPositionId("管理员");
		per.setPermissionMenuId("操作");
		
		assertEquals(per,mapper.selectByPrimaryKey(1001));
		assertNotEquals(per,mapper.selectByPrimaryKey(1001));
	}
	
	/**
	 * 
	 * testUpdateByPrimaryKey:测试testUpdateByPrimaryKey()方法. <br/>
	 * @throws Exception
	 */
	@Test
	public void testUpdateByPrimaryKey() throws Exception{
		
		per.setPermissionId(1001);
		per.setPermissionName("删除账户");
		per.setPermissionPositionId("管理员");
		per.setPermissionMenuId("操作");
		
		assertEquals(1,mapper.updateByPrimaryKey(per));
	}
	
	/**
	 * 
	 * testDelete:测试deleteByPrimaryKey()方法. <br/>
	 * @throws Exception
	 */
	@Test
	public void testDelete() throws Exception{

		assertEquals(1,mapper.deleteByPrimaryKey(1001));
		assertNotEquals(1,mapper.deleteByPrimaryKey(1001));
		
	}

}
