/**
 * Project Name:scb_sms
 * File Name:MainTest.java
 * Package Name:com.scb.dev.sms.util.mail
 * Date:2018年11月9日下午1:31:33
 * Copyright (c) 2018, Annie.Jiang@clps.com All Rights Reserved.
 *
 */
package com.scb.dev.sms.util.mail;

import com.scb.dev.sms.sm.vo.MailVO;

/**
 * ClassName: MainTest <br/>
 * date: 2018年11月9日 下午1:31:33 <br/>
 *
 * @author Annie.Jiang
 * @version V1.0
 * @since JDK 1.8
 */

public class SendMailTest {

	/**
	 * main:SendMail的测试主方法. <br/>
	 *
	 */
	public static void main(String[] args) {
	
		//这是我自己的邮箱，你可以更改为你的
		//首先要开启SMTP允许第三方登陆，一般在网页版邮箱的主页设置里
		
		
		MailVO mail=new MailVO();
		mail.setSmtpHost("smtp.163.com");
		mail.setTo("jiangliu961212@163.com");
		mail.setFrom("jiangliu961212@163.com");
		mail.setSubject("welcome my code!");
		mail.setContent("Happy Every Day!!!(^~^)");
		
//		mail.setUsername("jiangliu961212");
//		mail.setPassword("qazplm2580");
		
		
		//普通发送
		SendMail.send(mail.getSmtpHost(), mail.getFrom(), mail.getTo(), mail.getSubject(), mail.getContent());
		
//		SendMail.send(mail.getSmtpHost(), mail.getFrom(), mail.getTo(), mail.getSubject(), mail.getContent(),mail.getUsername(),mail.getPassword());
	}

}
