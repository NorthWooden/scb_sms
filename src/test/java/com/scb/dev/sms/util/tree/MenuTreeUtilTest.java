/**
 * Project Name:scb_sms
 * File Name:MenuTreeUtilTest.java
 * Package Name:com.scb.dev.sms.util.tree
 * Date:2018年11月15日下午2:15:16
 * Copyright (c) 2018, gael.zhu@qq.com All Rights Reserved.
 *
 */
package com.scb.dev.sms.util.tree;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.scb.dev.sms.sm.dao.MenuMapper;
import com.scb.dev.sms.util.factory.ToolFactory;
import com.scb.dev.sms.util.tree.model.Node;

/**
 * ClassName: MenuTreeUtilTest <br/>
 * Description: Tree Test. <br/><br/>
 * date: 2018年11月15日 下午2:15:16 <br/>
 *
 * @author gael.zhu
 * @version V1.0
 * @since JDK 1.8
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:/applicationContext.xml"})
public class MenuTreeUtilTest {
	
	@Autowired
	private MenuMapper menuMapper;
	private List<Node> menu=null;
	/**
	 * @param
	 * @return
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**  
	 * @param
	 * @return
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link com.scb.dev.sms.util.tree.MenuTreeUtil#menuList(java.util.List)}.
	 */
	@Test
	public void testMenuList() {
		this.menu=ToolFactory.getInstanceOfMenuTreeUtil().menuList(this.menuMapper.selectAllMenu());
		for(Node leaf:this.menu) {
			printTree(leaf);
		}
	}
	public void printTree(Node leaf) {
		System.out.println(leaf);
		for(Node nLeaf:leaf.getChildren()) {
			printTree(nLeaf);
		}
	}
}
