/**
 * Project Name:scb_sms
 * File Name:ReceiveMailTest.java
 * Package Name:com.scb.dev.sms.util.mail
 * Date:2018年11月16日上午10:17:27
 * Copyright (c) 2018, Annie.Jiang@clps.com All Rights Reserved.
 *
 */
package com.scb.dev.sms.util.mail;

/**
 * ClassName: ReceiveMailTest <br/>
 * Description: pop3协议存储邮件的测试方法
 * date: 2018年11月16日 上午10:17:27 <br/>
 *
 * @author Annie.Jiang
 * @version V1.0
 * @since JDK 1.8
 */
public class ReceiveMailTest {

	/**
	 * main:测试方法. <br/>
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		
			ReceiveMail.receive();
		
	}

}
